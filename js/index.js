/*弹出对话框*/
/*2014-07-24 张传奕
*修改了登录框和注册框的下拉上拉方式
*/
function ModalDialog(name, divid) {
    this.name = name; //名称
    this.div = divid; //要放入窗体中的元素名称
    this.show = function ()//显示窗体
    {
	//alert(obj.name + "_divshow");
        document.getElementById(obj.name + "_divshow").style.left = (document.body.clientWidth - 634) / 2 + "px";
        document.getElementById(obj.name + "_mask").style.width = document.body.clientWidth + "px";
        document.getElementById(obj.name + "_mask").style.height = window.screen.availHeight + "px";
        document.getElementById(obj.name + "_divshow").style.visibility = "visible";
        document.getElementById(obj.name + "_mask").style.visibility = "visible";
        document.getElementById(obj.name + "_divshow").style.opacity = 0.5;     // new line
        document.getElementById(obj.name + "_divshow").style.top = -800+"px";   // new line
        //$("#" + obj.name + "_divshow").hide();
        $("#" + obj.name + "_mask").fadeIn(600);
        $("#" + obj.name + "_divshow").animate({top:"100px",opacity:1.0},600);
    }

    this.close = function ()//关闭窗体
    {
        //document.all(obj.name + "_mask").style.width = 0 + "px";
        //document.all(obj.name + "_mask").style.height = 0 + "px";
        //document.all(obj.name + "_divshow").style.visibility = "hidden";
        //document.all(obj.name + "_mask").style.visibility = "hidden";
        $("#" + obj.name + "_divshow").animate({top:"-800px",opacity:0.0},600);
        $("#" + obj.name + "_mask").fadeOut(600);
    }

    this.login_toString = function () {

        //灰色半透明掩膜
        tmp = "<div id='" + this.name + "_mask' style='position:fixed; top:0; left:0; width:0; height:0; background:#333;opacity:0.5; filter:ALPHA(opacity=60); z-index:9; visibility:hidden'></div>";
        //背景
        tmp += "<div id='" + this.name + "_divshow' class='divshow_signup'>";
        //内容
        var inhtm = $("#logincontent").html();
        $("#logincontent").html("");
        tmp += "<div id='" + this.name + "_content'>" + inhtm + "</div></div>";
        document.write(tmp);
        //document.all(this.name + "_content").insertBefore(document.all(this.div));
    }
    this.signup_toString = function () {
        //灰色半透明掩膜
        tmp = "<div id='" + this.name + "_mask' style='position:fixed; top:0; left:0; width:0; height:0; background:#333;opacity:0.5; filter:ALPHA(opacity=60); z-index:9; visibility:hidden'></div>";
        //背景
        tmp += "<div id='" + this.name + "_divshow' class='divshow_signup'>";
        //内容
        var inhtm = $("#signupcontent").html();
        $("#signupcontent").html("");
        tmp += "<div id='" + this.name + "_content'>" + inhtm + "</div></div>";
        document.write(tmp);
        //document.all(this.name + "_content").insertBefore(document.all(this.div));
    }
    this.cpw_toString = function () {
        //灰色半透明掩膜
        tmp = "<div id='" + this.name + "_mask' style='position:fixed; top:0; left:0; width:0; height:0; background:#444;opacity:0.5; filter:ALPHA(opacity=60); z-index:9; visibility:hidden'></div>";
        //背景
        tmp += "<div id='" + this.name + "_divshow' class='divshow_cpw'>";
        //内容
        var inhtm = $("#CPWcontent").html();
        $("#CPWcontent").html("");
        tmp += "<div id='" + this.name + "_content'>" + inhtm + "</div></div>";
        document.write(tmp);
        //document.all(this.name + "_content").insertBefore(document.all(this.div));
    }
    var obj = this;
}
var md_login = new ModalDialog("md_login", "login-innner");
var md_reg = new ModalDialog("md_reg", "signup-innner");
var md_cpw = new ModalDialog("md_changepw", "changepw-inner");
//显示窗口
function uLoginShow() {
    md_login.show();
    return false;
}
//关闭窗口
function uLogindisplay() {
    md_login.close();
    return false;
}


//显示注册窗口
function uRegShow() {
    md_reg.show();
    return false;
}
//关闭注册窗口
function uRegdisplay() {
    md_reg.close();
    return false;
}
//显示修改密码窗口
function uCPWShow() {
    md_cpw.show();
    return false;
}
//关闭修改密码窗口
function uCPWdisplay() {
    md_cpw.close();
    return false;
}


var agreement_checked = false;
function agreement_toggle() {
    if (agreement_checked) {
        $("#signup-agree").attr("class", "signup-fragment-needagree")
        agreement_checked = false;
    } else {
        $("#signup-agree").attr("class", "signup-fragment-alreadyagree")
        agreement_checked = true;
    }
}

md_reg.signup_toString();
md_login.login_toString();
md_cpw.cpw_toString();

function guidebar_choose(obj) {
    var temp = obj.id;
    var barid = "guidebar-content-" + temp.substr(-1);

    $(".guide-change").attr('style', 'color:#a6aaae; background-color:#fff;font-size:16px;border-right:1px solid #e7e7e7;height:45px;');
    $("#guidebar-content-7").attr('style', 'color:#a6aaae; background-color:#fff;font-size:16px;height:45px;');
    $("#" + barid).attr('style', 'color:white; background-color:#69b7d4;-webkit-box-shadow: 1px 3px 3px;-moz-box-shadow: 1px 3px 3px; box-shadow: 0px 3px 3px -1px #2a6a81;font-size:17px;height:45px;');
	
	switch(obj.id)
	{
	// 最学术
	case 'guidebar-1':
		location.href = '/';
		break;
	// 最课程
	case 'guidebar-2':
		if (typeof(flag_login) != "undefined" && (!flag_login))
		{
			alert('请先登录！');
			location.href = '/';
		}
		else
		    location.href = '/course';
		break;
	default:
		alert('正在努力开发中！');
		break;
	}
}

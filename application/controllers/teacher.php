<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teacher extends CI_Controller {
	function index() {
	}
	
	function id($school_key,$teacher_id) {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->model('teacher_m');
		$result = $this->teacher_m->query_teacher_info($school_key,$teacher_id);
		if(count($result) == 0)
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
		    $this->load->library('session');
			$info = array("user_id" => $this->session->userdata['user_id'], "user_name" => $this->session->userdata['user_name'], "teacher_info" => $result[0]);
			$this->load->view('teacher_v',$info);
		}
	}
}

?>


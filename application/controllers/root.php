<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Root extends CI_Controller {
	public function index()
	{
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		if($user_id != false)
		{
			$this->load->model("user_m");
			$result = $this->user_m->query_user_by_id($user_id);
			$this->load->model("operate_count_m");
			$tag = $this->operate_count_m->user_status($user_id);
			$flag_authCode = ($tag != 0);
			$userdata = array("user_id" => $user_id, "name" => $result[0]->user_name, "flag_authCode" => $flag_authCode, "flag_login" => true);
			$this->load->model('record_m');
			$ip_address = $this->session->userdata('ip_address');
			$this->record_m->insert($user_id, $ip_address);
			switch($tag)
			{
				case 0:
					$this->load->view('index',$userdata);
					break;
				case 1:
					$this->load->view('index',$userdata);
					break;
				case 2:
					show_error('You have too many operations! If you think a technical problem has encontered, please contact feedback@zuixueshu.com.',500,'ERROR');
					break;
				default:
					show_error('You have too many operations! If you think a technical problem has encontered, please contact feedback@zuixueshu.com.',500,'ERROR');
					break;
			}
		}
		else
		{
			$ip_address = $this->session->userdata('ip_address');
			$this->load->model("operate_count_m");
			$tag = $this->operate_count_m->ip_status($ip_address);
			$flag_authCode = ($tag != 0);
			$userdata = array("flag_authCode" => $flag_authCode, "flag_login" => false);
			switch($tag)
			{
				case 0:
					$this->load->view('index',$userdata);
					break;
				case 1:
					$this->load->view('index',$userdata);
					break;
				case 2:
					show_error('You have too many operations! If you think a technical problem has encontered, please contact feedback@zuixueshu.com.',500,'ERROR');
					break;
				default:
					show_error('You have too many operations! If you think a technical problem has encontered, please contact feedback@zuixueshu.com.',500,'ERROR');
					break;
			}
		}
	}	
	
	
}
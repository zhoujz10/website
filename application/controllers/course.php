<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Course extends CI_Controller {
	function index() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_data = array('user_id' => $this->session->userdata('user_id'), 'user_name' => $this->session->userdata('user_name'));
		$this->load->view('course_index', $user_data);
	}
	
	function query() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
        
		if(!array_key_exists("keyword", $_GET) || !array_key_exists("page", $_GET) || !array_key_exists("max_record_number", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}

		$de = array('全部','建筑学院','城规系','建筑系','土木系','水利系','环境学院','机械系','精仪系','热能系','汽车系','工业工程系','信息学院','电机系','电子系','计算机系','自动化系','微纳电子系','航院','工物系','化工系','材料学院','数学系','物理系','化学系','生命学院','地球科学中心','交叉信息学院','周培源应','经管学院','公共管理','金融学院','人文学院','社科学院','中文系','外文系','法学院','新闻学院','马克思主义学院','人文学院','社科学院','体育部','电教中心','图书馆','艺教中心','美术学院','土水学院','建管系','建筑技术','核研院','教研院','网络中心','训练中心','电工电子中心','宣传部','学生部','武装部','研究生院','深研生院','校医院','医学院','生医系','软件学院');
		$te = array('全部','文化素质核心课','体育课','新生研讨课','双语课','专题研讨课','实验课','实践课');
		$ye = array('全部','2013','2012','2011','2010');
		$a = array();
		for($i = 1; $i <= 63; $i++)
		{
			if(array_key_exists('de' . $i, $_GET) && $_GET['de' . $i] == 'true')
				$a[] = $de[$i-1];
		}
		$b = array();
		for($i = 1; $i <= 8; $i++)
		{
			if(array_key_exists('te' . $i, $_GET) && $_GET['te' . $i] == 'true')
				$b[] = $te[$i-1];
		}
		$c = array();
		for($i = 1; $i <= 5; $i++)
		{
			if(array_key_exists('ye' . $i, $_GET) && $_GET['ye' . $i] == 'true')
				$c[] = $ye[$i-1];
		}
		$d = $_GET['keyword'];
		$max_record_number = intval($_GET['max_record_number']);
		$page = intval($_GET['page']);
		
		$this->load->model("course_m");
		$query_num = $this->course_m->query_num($a,$b,$c,$d);
		if($max_record_number > 0 && $page >= 1 && ($page <= ceil($query_num / $max_record_number) || $page == 1))
		{
			$query_course_list = $this->course_m->query_course($a,$b,$c,$d,$_GET['page'],$max_record_number);
			$rjson = array("status" => true, 'query_num' => $query_num, 'query_course_list' => $query_course_list, "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
			$rjson = array("status" => false, "failDesc" => "页码范围不正确！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
    }
    
    function course_schedule() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$this->load->model('care_m');
		$course_list = $this->care_m->querymycourse($user_id);
		if(count($course_list) == 0)
		{
			$rjson = array("status" => true, "course_number" => 0, "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
		    $len = count($course_list);
		    $this->load->model('course_m');
		    $current_week = $this->course_m->query_current_week('10003');
		    $course_schedule = array();
		    for ($i=0; $i<$len; $i++)
		    {
		        $course_time = $course_list[$i]->course_time;
		        $tag = 0;//用于标记这门课这周是否开设
		        $arr = preg_split('/,/',$course_time);
		        for ($j=0; $j<count($arr); $j++)
		        {
		            if (strstr($arr[$j],'全周'))
		            {
		                $tag = 1;
		            }
		            elseif (strstr($arr[$j],'前八周'))
		            {
		                if ($current_week <= 8)
		                    $tag = 1;
		            }
		            elseif (strstr($arr[$j],'后八周'))
		            {
		                if ($current_week > 8 && $current_week <= 16)
		                    $tag = 1;
		            }
		            elseif (strstr($arr[$j],'单周'))
		            {
		                if ($current_week % 2 == 1)
		                    $tag = 1;
		            }
		            elseif (strstr($arr[$j],'双周'))
		            {
		                if ($current_week % 2 == 0)
		                    $tag = 1;
		            }
		            else
		            {
		                $start_week = intval(preg_replace(array("/.+\(/","/-.+/"),array("",""),$arr[$j]));
		                $end_week = intval(preg_replace(array("/.+-/","/周\)/"),array("",""),$arr[$j]));
		                if ($current_week >= $start_week && $current_week <= $end_week)
		                    $tag = 1;
		            }
		            if ($tag == 1)
		            {
		                $day = intval(preg_replace("/-.+/","",$arr[$j]));
		                $section = intval(preg_replace(array("/\(.+/","/.+-/"),array("",""),$arr[$j]));
		                $course_schedule[] = array('course_key' => $course_list[$i]->primary_key, 'course_id' => $course_list[$i]->course_id, 'sub_id' => $course_list[$i]->sub_id, 'course_name' => $course_list[$i]->course_name, 'teacher_name' => $course_list[$i]->teacher_name, 'day' => $day, 'section' => $section);
		            }
		        }
		    }
		}
		$rjson = array("status" => true, "course_number" => count($course_schedule), "course_schedule" => $course_schedule, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
    }
	
    function id($course_id,$sub_id) {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');

		$this->load->model("course_m");
		$result = $this->course_m->search_primary_key_by_id($course_id,$sub_id);
		if(count($result) == 0)
		{
			header('Location:/');
			return;
		}
		else
		{
		    $course_info = $this->course_m->search_course_by_key($result[0]->primary_key)[0];
		    $course_name = $course_info->course_name;
		    $school_name = $course_info->school;
		    $this->course_m->add_course_click($result[0]->primary_key,10003);
			$this->load->model("care_m");
			$tag = $this->care_m->query_course_tag($user_id,$result[0]->primary_key);
			$info = array("user_id" => $user_id, "user_name" => $this->session->userdata['user_name'], "primary_key" => $result[0]->primary_key, "school_name" => $school_name, "course_name" => $course_name, "tag" => $tag);
			$this->load->view('course_v',$info);
		}
	}
	
	function change_concern_tag() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');

		if(!array_key_exists("primary_key", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model("course_m");
		$num = count($this->course_m->search_course_name_by_key($_GET["primary_key"]));
		if($num == 0)
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}

		$this->load->model("care_m");
		$this->care_m->change_course_care($user_id, $_GET["primary_key"]);
		$rjson = array("status" => true, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function like_discussion() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');

		if(!array_key_exists("course_primary_key", $_GET) || !array_key_exists("primary_key", $_GET) || !array_key_exists("flag", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model('discussion_m');
		if($_GET['flag'] == '1')
		{
			$result = $this->discussion_m->query_discussion($_GET['primary_key']);
		}
		else
		{
			$result = $this->discussion_m->query_discussion_reply($_GET['primary_key']);
		}
		if(count($result) == 0)
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$tag = $this->discussion_m->like($_GET['flag'], $user_id, $_GET['primary_key']);
		$rjson = array("status" => true, "tag" => $tag, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function like_study_experience() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');

		if(!array_key_exists("course_primary_key", $_GET) || !array_key_exists("primary_key", $_GET) || !array_key_exists("flag", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model('study_experience_m');
		if($_GET['flag'] == '1')
		{
			$result = $this->study_experience_m->query_study_experience($_GET['primary_key']);
		}
		else
		{
			$result = $this->study_experience_m->query_study_experience_reply($_GET['primary_key']);
		}
		if(count($result) == 0)
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$tag = $this->study_experience_m->like($_GET['flag'], $user_id, $_GET['primary_key']);
		$rjson = array("status" => true, "tag" => $tag, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function get_course_info() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		if(!array_key_exists("primary_key", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model('course_m');
		$result = $this->course_m->search_course_by_key($_GET['primary_key']);
		if(count($result) == 0)
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
		    $this->load->model('teacher_m');
		    $teacher_id = $this->teacher_m->query_teacher_id($result[0]->teacher_key);
		    $result[0]->teacher_id = $teacher_id;
			$rjson = array("status" => true, "course_info" => $result[0], "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
	}
	
	// 从某个id开始，请求接下来的几条，返回数据
	function get_discussion() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		if(!array_key_exists("course_primary_key", $_GET) || !array_key_exists("last_primary_key", $_GET) || !array_key_exists("max_discussion_number", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$course_primary_key = $_GET['course_primary_key'];
		$last_primary_key = $_GET['last_primary_key'];
		$max_discussion_number = intval($_GET['max_discussion_number']);
		if(intval($last_primary_key) >= 0 && $max_discussion_number > 0)
		{
			$this->load->model('discussion_m');
			$discussion_list = $this->discussion_m->get_discussion($course_primary_key, $last_primary_key, $max_discussion_number);
			$reply_list = $this->discussion_m->get_discussion_reply($course_primary_key, $discussion_list);
			$rjson = array("status" => true, "discussion_list" => $discussion_list, "reply_list" => $reply_list, "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
	}
	
	//获得学习经验列表
	function get_study_experience() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		if(!array_key_exists("course_primary_key", $_GET) || !array_key_exists("page", $_GET) || !array_key_exists("max_study_experience_number", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$course_primary_key = $_GET['course_primary_key'];
		$page = intval($_GET['page']);
		$max_study_experience_number = intval($_GET['max_study_experience_number']);
		$this->load->model("study_experience_m");
		$num = $this->study_experience_m->query_num($course_primary_key);
		if($max_study_experience_number > 0 && $page >= 1 && ($page <= ceil($num / $max_study_experience_number) || $page == 1))
		{
			$study_experience_list = $this->study_experience_m->query_study_experience_list($course_primary_key,$page,$max_study_experience_number);
			$rjson = array("status" => true, 'num' => $num, 'study_experience_list' => $study_experience_list, "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
			$rjson = array("status" => false, "failDesc" => "页码范围不正确！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
	}
	
	function submit_discussion() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		if(!array_key_exists("course_primary_key", $_POST) || !array_key_exists("content", $_POST))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		if($_POST["content"] == null)
		{
			$rjson = array("status" => false, "failDesc" => "新鲜事内容不能为空！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model("discussion_m");
		$tag = $this->discussion_m->insert_new_discussion($user_id, $this->session->userdata['user_name'], $_POST['course_primary_key'], $_POST['content']);
		if($tag == 0)
		{
			$rjson = array("status" => false, "failDesc" => "该课程不存在！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
			$rjson = array("status" => true, "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
	}
	
	function reply_discussion() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		if(!array_key_exists("tag", $_POST) || !array_key_exists("primary_key", $_POST) ||!array_key_exists("content", $_POST))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		if($_POST["content"] == null)
		{
			$rjson = array("status" => false, "failDesc" => "新鲜事内容不能为空！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}

		$this->load->model("discussion_m");
		if($_POST['tag'] == '1')
		{
			$insert_result = $this->discussion_m->insert_new_reply($user_id, $this->session->userdata['user_name'], $_POST['primary_key'], $_POST['content']);
			if($insert_result['tag'] == 0)
			{
				$rjson = array("status" => false, "failDesc" => "该新鲜事不存在！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
				return;
			}
		}
		else
		{
			$insert_result = $this->discussion_m->insert_new_sub_reply($user_id, $this->session->userdata['user_name'], $_POST['primary_key'], $_POST['content']);
			if($insert_result['tag'] == 0)
			{
				$rjson = array("status" => false, "failDesc" => "该新鲜事不存在！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
				return;
			}
		}
		
		$this->load->model("message_m");
		$this->load->model("course_m");
		if($_POST['tag'] == '1')
		{
			$result = $this->discussion_m->query_discussion($_POST['primary_key']);
			$course_name = $this->course_m->search_course_by_key($result[0]->course_key)[0]->course_name;
			$this->message_m->insert_new_discussion_reply_message($result[0]->course_key, $course_name, $user_id, $this->session->userdata['user_name'], $result[0]->primary_key, $result[0]->user_id, $result[0]->content);

		}
		else
		{
			$result_reply = $this->discussion_m->query_discussion_reply($_POST['primary_key']);
			$result = $this->discussion_m->query_discussion($result_reply[0]->discussion_key);
			$course_name = $this->course_m->search_course_by_key($result[0]->course_key)[0]->course_name;
			$this->message_m->insert_new_discussion_reply_message($result[0]->course_key, $course_name, $user_id, $this->session->userdata['user_name'], $result[0]->primary_key, $result_reply[0]->user_id, $result[0]->content);
			if($result_reply[0]->user_id != $result[0]->user_id)
				$this->message_m->insert_new_discussion_reply_message($result[0]->course_key, $course_name, $user_id, $this->session->userdata['user_name'], $result[0]->primary_key, $result[0]->user_id, $result[0]->content);
		}
		$rjson = array("status" => true, "reply_key" => $insert_result['reply_primary_key'], "time" => $insert_result['time'],"url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function get_course_id() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		if(!array_key_exists("course_primary_key", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model('course_m');
		$result = $this->course_m->search_course_by_key($_GET['course_primary_key']);
		if(count($result) == 0)
		{
			$rjson = array("status" => false, "failDesc" => "无满足查询条件的结果！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$rjson = array("status" => true, "course_id" => $result[0]->course_id, "sub_id" => $result[0]->sub_id, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function write_xuexijingyan() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		if(!array_key_exists("course_primary_key", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model("course_m");
		$course_info_array = $this->course_m->search_course_by_key($_GET["course_primary_key"]);
		if (count($course_info_array) == 0)
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		$course_info = $course_info_array[0];
		$rjson = array("status" => true, "url" => "/study_experience/write/" . $course_info->course_id . "/" . $course_info->sub_id);
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
}



?>


<?php

class User extends CI_Controller {
	function index() {
	}

	function id($view_id) {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');

		if($user_id == $view_id)
		{
			$userdata = array("user_id" => $user_id, "user_name" => $this->session->userdata('user_name'));
			$this->load->view('user_info',$userdata);
		}
		else
		{
		}
	}

/*	function save() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$data = array("name" => $_POST["nameinput"], "department" => $_POST["departmentinput"], "class" => $_POST["classinput"]);
		$this->user_m->update($user_id, $data);
		$u = "location:/user/" . $user_id;
		header($u);
	}*/

	function leavemessage($user_id,$view_id) {
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);
		
		$this->load->model("message_m");
		$num = $this->message_m->count_num();
		$id = $num + 1;
		$info = array("id" => $id, "user_id" => $user_id, "view_id" => $view_id, "user_name" => $result[0]->name, "content" => $_POST["content_input"], "time" => date('Y-m-d H:i:s',time()));
		$num = $this->message_m->insert($info);

		$u = "location:/user/" . $view_id;
		header($u);
	}

    function logout() {
        $this->load->library("session");
        $this->session->unset_userdata("user_id");
		header("location:/");
    }
	
	function get_user_info() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$this->load->model('user_m');
		$user_data = $this->user_m->query_user_by_id($user_id)[0];
		$rjson = array("status" => true, 'user_name' => $user_data->user_name, 'user_mail' => $user_data->user_mail, 'student_id' => $user_data->student_id, 'department' => $user_data->department, 'class' => $user_data->class, 'birthday' => $user_data->birthday);
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function get_my_course() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$this->load->model('care_m');
		$course_data = $this->care_m->querymycourse($user_id);
		$rjson = array('status' => true, 'course_data' => $course_data);
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function get_my_message() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$this->load->model('message_m');
		$message_data = $this->message_m->get_message($user_id);
		$rjson = array('status' => true, 'message_data' => $message_data);
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function save_user_info() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$this->load->model('user_m');
		$user_info = array();
		if(!array_key_exists('user_name', $_POST) || !array_key_exists('student_id', $_POST) || !$_POST['user_name'] || !$_POST['student_id'])
		{
			$rjson = array("status" => false, "failDesc" => "姓名或学号不能为空！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		$user_info['user_name'] = $_POST['user_name'];
		$user_info['student_id'] = $_POST['student_id'];
		if(array_key_exists('department', $_POST))
			$user_info['department'] = $_POST['department'];
		if(array_key_exists('class', $_POST))
			$user_info['class'] = $_POST['class'];
		if(array_key_exists('birthday', $_POST))
			$user_info['birthday'] = $_POST['birthday'];			
		$this->user_m->update($user_id,$user_info);
		$rjson = array("status" => true, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function change_user_password() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$this->load->model('user_m');
		$user_info = array();
		if(!array_key_exists('orig_password', $_POST) || !array_key_exists('new_password', $_POST) || !array_key_exists('new_password_repeat', $_POST)|| !$_POST['orig_password'] || !$_POST['new_password'] || !$_POST['new_password_repeat'])
		{
			$rjson = array("status" => false, "failDesc" => "密码不能为空！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		$orig_password = $this->user_m->query_user_by_id($user_id)[0]->password;
		if($_POST['orig_password'] != $orig_password)
		{
			$rjson = array("status" => false, "failDesc" => "原密码输入不正确！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		if($_POST['new_password'] != $_POST['new_password_repeat'])
		{
			$rjson = array("status" => false, "failDesc" => "两次新密码输入必须相同！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		$user_info['password'] = $_POST['new_password'];
		$this->user_m->update($user_id,$user_info);
		$rjson = array("status" => true, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function set_message_read() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$this->load->model('message_m');
		$num = count($_POST);
		if($num == 0)
		{
			$rjson = array("status" => false, "failDesc" => "您没有选中任何消息！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
			$message_id_array = array();
			for($i = 0; $i < $num; $i++)
			{
				if(!array_key_exists('del' . $i, $_POST) || !$_POST['del' . $i])
				{
					$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
					$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
					echo $rjson;
					return;
				}
				$message_id_array[] = $_POST['del' . $i];
			}
			$this->message_m->set_message_read($user_id, $message_id_array);
			$rjson = array("status" => true, "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
	}
	
	function cancel_course_care() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$this->load->model("care_m");
		$num = count($_POST);
		if($num == 0)
		{
			$rjson = array("status" => false, "failDesc" => "您没有选中任何课程！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
			$course_care_key_array = array();
			for($i = 0; $i < $num; $i++)
			{
				if(!array_key_exists('cancel' . $i, $_POST) || !$_POST['cancel' . $i])
				{
					$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
					$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
					echo $rjson;
					return;
				}
				$course_care_key_array[] = $_POST['cancel' . $i];
			}
			$this->care_m->cancel_course_care($user_id, $course_care_key_array);
			$rjson = array("status" => true, "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
	}

}

?>

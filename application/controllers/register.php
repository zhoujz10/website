<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller {

    /*function index() {
	    $this->load->library("session");
        if ($this->session->userdata('auth_code') == $_POST["authcd"])
        {
            $this->load->model("user_m");
		    $this->load->model("email_m");
		    $result = $this->user_m->querymail($_POST["mailinput"]);
		    if (!$result) {
			    $user_id = $this->user_m->count_num() + 1;
			    $info = array("user_id" => $user_id, "mail" => $_POST["mailinput"], "name" => $_POST["nameinput"], "password" => $_POST["passwordinput"]);
			    $this->user_m->insert($info);

			    //$this->load->library("session");
			    $this->session->set_userdata('user_id',$user_id);
			    $data = array("user_id" => $user_id, "name" => $_POST["nameinput"]);

/*			    $this->load->library('email');
			    $this->email->from('admin@zuixueshu.com', 'admin');
			    $this->email->to($_POST["mailinput"]); 
			    $this->email->subject('验证注册');
			    $this->email->message('Testing the email class.'); 
                $this->email->send();*/
     /*           $this->email_m->sendMessage('dengyuntian@zuixueshu.com', 'zhoujz10@163.com', '验证注册', '欢迎您，'.$_POST['nameinput'].'同学！');

			    $this->load->view("head1.php", $data);
			    $this->load->view("banner2.php", $data);
			    $this->load->view('searcher');
			    }
		    else {
			    echo "<html><head><meta charset = 'utf-8'><title>error occurs!</title></head><body><p>邮箱已存在！</p></body></html>";
            }
        }
        else {
            echo "<html><head><meta charset = 'utf-8'><title>error occurs!</title></head><body><p>验证码错误！</p></body></html>";
        }
	}*/
	
	function index() {
		$this->load->library("authcode");
        if (array_key_exists("mail", $_POST) && array_key_exists("studentId", $_POST) && array_key_exists("name", $_POST) && array_key_exists("password", $_POST))
        { 
			$this->load->library("session");
			$ip_address = $this->session->userdata('ip_address');
			$this->load->model("operate_count_m");
			$this->load->library("Operate_count");
			$tag = $this->operate_count_m->ip_status($ip_address);

			if ($tag == 2)
			{
				$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
				return;
			}
			
			$this->load->model("user_m");
			$result_mail = $this->user_m->query_user_by_mail($_POST["mail"]);
			if (count($result_mail) == !0)
			{
				$this->operate_count->register(0);
				$tag = $this->operate_count_m->ip_status($ip_address);
				if ($tag == 2)
					$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
				else
					$rjson = array("status" => false, "authCode" => ($tag != 0), "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "该邮箱已被注册！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
				return;
			}
			
			$result_student_id = $this->user_m->query_user_by_student_id($_POST["studentId"]);
			if (count($result_student_id) == !0)
			{
				$this->operate_count->register(0);
				$tag = $this->operate_count_m->ip_status($ip_address);
				if ($tag == 2)
					$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
				else
					$rjson = array("status" => false, "authCode" => ($tag != 0), "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "该学号已被注册！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
				return;
			}
			
            if ($tag == 1)
            {
				if ((!array_key_exists("authCode", $_POST)) || $_POST["authCode"] == "")
				{
					$this->operate_count->register(0);
					$tag = $this->operate_count_m->ip_status($ip_address);
					if ($tag == 2)
						$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
					else
						$rjson = array("status" => false, "test" => $this->session->userdata("auth_code"), "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "验证码不能为空！", "url" => "/");
                    $rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
                    echo $rjson;
                    return;
				}
                if (!($this->authcode->check($_POST["authCode"])))
                {
					$this->operate_count->register(0);
					$tag = $this->operate_count_m->ip_status($ip_address);
					if ($tag == 2)
						$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
					else
						$rjson = array("status" => false, "test" => $this->session->userdata("auth_code"), "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "验证码不正确！", "url" => "/");
                    $rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
                    echo $rjson;
                    return;
                }
            }
            //$result = $this->user_m->query_user_by_mail($_POST["mail"]);
            
			$user_id = $this->user_m->count_user() + 1;
			$this->operate_count->register(1);
			$info = array("user_id" => $user_id, "user_mail" => $_POST["mail"], "user_name" => $_POST["name"], "student_id" => $_POST["studentId"],"password" => $_POST["password"]);
			$this->user_m->insert($info);
			$this->session->set_userdata(array("user_id" => $user_id, "user_name" => $info['user_name'], "user_mail" => $info['user_mail']));
			$data = array("user_id" => $user_id, "name" => $_POST["name"]);
			$user_name = $_POST["name"];
			$rjson = array("status" => true, "userId" => $user_id, "user_name" => $user_name, "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
            
        }
        else
        {
                $rjson = array("status" => false, "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "有项目为空！", "url" => "/");
                $rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
                echo $rjson;
        }
    }

}


?>

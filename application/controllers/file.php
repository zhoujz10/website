<?php

class File extends CI_Controller {

	function index() {
	}

    function upload_mypic() {
		$this->load->library('Operate_count');
		$tag = $this->Operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		$config["upload_path"] = './userpic/';
		$config['overwrite'] = true;
		$config["allowed_types"] = 'gif|jpg|png';
		$config['max_size'] = '100';
		$config['max_width'] = '1024';
		$config['max_height'] = '1024';
		$config['file_name'] = "pic" . $user_id;;
		$this->load->library("upload", $config);
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail);
		if ($this->upload->do_upload("fs")) {
			header("Location:/user_info/info");
		}
		else {
			echo $this->upload->display_errors('<p>', '</p>');
		}
	}
	
    function do_upload() {
		$this->load->library('Operate_count');
		$tag = $this->operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		log_message('debug','1');
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		$user_name = $this->session->userdata('user_name');

		if(!array_key_exists("title", $_POST) || !array_key_exists("course_key", $_POST) || !array_key_exists("file_tag", $_POST) || !array_key_exists("file_description", $_POST))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		log_message('debug','2');
		if($_POST['title'] == "" && $_POST['title_tag'] == false) //文件标题为空，但是又没有选择使用原文件名，说明不是正常途径访问网站
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		log_message('debug','3');
		$course_key = $_POST["course_key"];
		$file_tag = $_POST["file_tag"];
		$description = $_POST["file_description"];
		if (array_key_exists("title_tag", $_POST))
		    $title_tag = true;
	    else
	        $title_tag = false;
		$title = $_POST["title"];
		log_message('debug','4');
		$this->load->model("course_m");
		$R = $this->course_m->search_course_by_key($course_key);
		if(count($R) == 0) //课程不存在
		{log_message('debug','5');
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
            $course_id = $R[0]->course_id;
            $sub_id = $R[0]->sub_id;
		}
		log_message('debug','6');
        $this->load->model("material_m");
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'txt|gif|jpg|png|jpeg|pdf|doc|docx|ppt|pptx|xls|xlsx|zip|rar';
        $config['max_size'] = '64000';
        $config['remove_spaces'] = 'TRUE';
        $config['file_name'] = $this->material_m->generate(10);
        
        $this->load->library('upload', $config);
        log_message('debug','7');
        if ( ! $this->upload->do_upload())
        {log_message('debug','8');
            $rjson = array("status" => false, "failDesc" => $this->upload->display_errors(), "url" => "/");
		    $rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		    echo $rjson;
		    return;
        }
        else
        {log_message('debug','9');
            $data = array('upload_data' => $this->upload->data());
            $this->material_m->upload_file($title,$title_tag,$user_id,$user_name,$course_key,$course_id,$sub_id,$file_tag,$description,$data['upload_data']);
            $rjson = array("status" => true, "url" => "/");
		    $rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		    echo $rjson;
		    return;
        }
    }
    
    function do_download()
    {
		$this->load->library('Operate_count');
		$tag = $this->Operate_count->operate_count_judge();
		switch ($tag)
		{
		    case 2:
    			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		case 3:
    			$rjson = array("failDesc" => "请先登录！", "url" => "/");
    			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
    			echo $rjson;
    			return;
    		default:
    		    break;
		}
		
		$this->load->library('session');
		$user_id = $this->session->userdata('user_id');
		
		if(!array_key_exists("primary_key", $_POST))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$primary_key = $_POST['primary_key'];
        $this->load->model("material_m");
        $R = $this->material_m->download_info($primary_key);
        if(len($R) == 0) //该资料不存在
        {
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
        }
        else
        {
            $url = "./uploads" . $R[0]->raw_name;
        }
        
        $this->load->helper('download');
        $file = file_get_contents($url); // 读文件内容
        force_download($R[0]->title, $file);

		$rjson = array("status" => true, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
    }

}

?>
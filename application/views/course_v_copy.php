﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <!--script type="text/javascript" src="/js/jquery-1.2.6.js"></script-->

        <script src="/js/raphael-min.js"></script>
        <script src="/js/slider.js"></script>
        <script src="/js/polar.js"></script>
        <script type="text/javascript" src="/js/comment.js"></script>  
        <script type="text/javascript" src="/js/albumshow.js"></script>  

        <link rel="stylesheet" type="text/css" href="/css/index.css">
        
        <!--分数占比图表样式专用-->
		<style type="text/css">
            #circle-graph {
                height:300px;
                width:300px;
            }
            #holder {
                height: 180px;
                margin: 0 0 0 0;
                width: 180px;
            }

            #weights {
                position:absolute;
                margin-left:200px;
                margin-top:80px;
                font-size:16px;
            }

            #copy {
                bottom: 0;
                font: 300 .7em "Helvetica Neue", Helvetica, "Arial Unicode MS", Arial, sans-serif;
                position: absolute;
                right: 1em;
                text-align: right;
            }
            #copy a {
                color: #fff;
            }
        </style>
        <!--滑块样式专用-->
        <style type="text/css">
            .defaultbar {  
                height: 10px;
                background-color: #c3c1c1;   
                position: relative;
                -moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px;
                cursor:pointer; 
            } .defaultbar .jquery-completed {  
                height: 10px;  
                background-color: #89d3ee;   
                position: absolute;
                -moz-border-radius-topleft:8px;-webkit-border-top-left-radius:8px;border-top-left-radius:8px;
                -moz-border-radius-bottomleft:8px;-webkit-border-bottom-left-radius:8px;border-bottom-left-radius:8px;
            } .defaultbar .jquery-jslider {  
                height: 15px;
                width: 50%;
                background-color: #69b7d4;   
                top: -3px; 
                -moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px;
                display: block;  
                cursor: pointer;  
                position: absolute;  
            } .defaultbar .jquery-jslider-hover {  
                background-color: #459dbd;  
            }            
        </style>

		<!--PHP变量赋值给js-->
        <script type = "text/javascript">
			<?php
				if (isset($user_name))
				{
					echo "var user_name = '" . $user_name . "';\n";
				}
				if (isset($user_id))
				{
					echo "var user_id = '" . $user_id ."';\n";
				}
				if (isset($primary_key))
				{
					echo "var course_primary_key = '" . $primary_key ."';\n";
				}
				if (isset($tag))
				{
					echo "var tag = '" . $tag ."';\n";
				}
			?>
		</script>
		
		<!--页面初始化-->
		<script type="text/javascript">
			$(document).ready(function(){
				//加载姓名部分
				$('#denglu').html(user_name);
				$('#zhuce').html("退出");
				
				//加载课程信息部分
				getCourseInfo(course_primary_key);
				$.fn.get_discussion('0');
				$.fn.get_study_experience('1');
				//setTimer();
				
			});
			function setTimer()
			{
			//	alert('12');
			//	setTimeout('setTimer()', 5000);
			}
			//载入用户信息及退出函数
			function uInfoShow()
			{
				location.href = '/user/' + user_id;
			}
			function uLogout()
			{
				$.get('/login/logout/'+ Math.random(),function(result){
					location.href = '/';
				});
			}
			//加载课程信息部分
			function getCourseInfo(course_primary_key)
			{
				$.get('/course/get_course_info/', {primary_key: course_primary_key} ,function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							if(tag == 0)
								$('#titlezone').html(result.course_info['course_name'] + '<a href="#addconcern" class="no-underline"><span class="add-concern" id="concern" onclick="change_concern_tag()">+ 关注</span></a>');
							else
								$('#titlezone').html(result.course_info['course_name'] + '<a href="#addconcern" class="no-underline"><span class="add-concern" id="concern" onclick="change_concern_tag()">取消关注</span></a>');
							$('#course_name_span').html(result.course_info['course_name']);
							$('#course_teacher').html('<span class="gray-text">主讲教师：</span>' + result.course_info['teacher_name']);
							$('#course_time').html('<span class="gray-text">时间：</span>' + result.course_info['course_time']);
							$('#course_cap_1').html('<span class="gray-text">本科生课容量：</span>' + result.course_info['student_capacity_1']);
							$('#course_cap_2').html('<span class="gray-text">研究生课容量：</span>' + result.course_info['student_capacity_2']);
							$('#course_credit').html('<span class="gray-text">学分：</span>' + result.course_info['credit']);
							$('#course_id').html('<span class="gray-text">课程号：</span>' + result.course_info['course_id']);
							$('#sub_id').html('<span class="gray-text">课序号：</span>' + result.course_info['sub_id']);
							$('#course_department').html('<span class="gray-text">开课院系：</span>' + result.course_info['department']);
							if(result.course_info['course_feature'] != null)
								$('#course_feature').html('<span class="gray-text">课程类型：</span>' + result.course_info['course_feature']);
							else
								$('#course_feature').html('<span class="gray-text">课程类型：</span>');
							if(result.course_info['course_description'] != null)
								$('#course_description').html('<span class="gray-text">课程简介：</span>' + result.course_info['course_description']);
							else
								$('#course_description').html('<span class="gray-text">课程简介：</span>');
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
				});
			}
			
			//加载新鲜事部分
			$.fn.get_discussion = function(last_primary_key, callback)
			{
				max_discussion_number = 20;
				$.get('/course/get_discussion/', {course_primary_key: course_primary_key, last_primary_key: last_primary_key, max_discussion_number: max_discussion_number} ,function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							var discusstemp = '';
							var i = 0;
							var len = result.discussion_list.length;
							var j = 0;
							var len_r = result.reply_list.length;
							var k = 0;
							if (len == 0)
							{
								if (last_primary_key == '0')
									alert('还没有新鲜事');
								else
									alert('没有更多新鲜事了');
							}
							for (i = 0; i < len; i++)
							{
								discusstemp += '<!--type1.状态-->\n';
                                discusstemp += '       <div class="xxs-content-status" id="discussion_content-' + result.discussion_list[i].primary_key + '">\n';
								$('#last_pk').val(result.discussion_list[i].primary_key);
                                discusstemp += '           <div class="row">\n';
                                discusstemp += '               <div class="col-sm-2">\n';
                                discusstemp += '               	<img class="img-head" src="/img/head1.jpg">\n';
                                discusstemp += '               </div>\n';
                                discusstemp += '               <div class="col-sm-10 padding-0 padding-r-20 margin-t-20">\n';
                                discusstemp += '                   <div>\n';
                                discusstemp += '                       <a href="#namelink" class="text-name-link">\n';
                                discusstemp += '                           ' + result.discussion_list[i].user_name + '：\n';
                                discusstemp += '                       </a>\n';
                                discusstemp += '                       <span>\n';
                                discusstemp += '                       	' + result.discussion_list[i].content + '\n';
                                discusstemp += '                   	</span>\n';
                                discusstemp += '                   </div>\n';
                                discusstemp += '                   <div class="margin-t-10 padding-lr-10 size-h-40">\n';
                                discusstemp += '                        <div class="text-hint">\n';
                                //discusstemp += '                            <div class="pull-left">\n';
                                //discusstemp += '                            	<div class="icon-public pull-left"></div>公开\n';
                                //discusstemp += '                            </div>\n';
                                discusstemp += '                            <div class="pull-left">\n';
                                discusstemp += '                                ' + result.discussion_list[i].time + '\n';
                                discusstemp += '                            </div>\n';
                                discusstemp += '                        </div>\n';
                                discusstemp += '                        <div class="pull-right">\n';
                                discusstemp += '                            <div class="pull-right size-w-90">\n';
                                discusstemp += '                                <a href="#like" name="like_discussion" id="like_discussion-' + result.discussion_list[i].primary_key + '"><div class="icon-like pull-left"></div>赞(' + result.discussion_list[i].like + ')</a>\n';
                                discusstemp += '                            </div>\n';
                                //discusstemp += '                            <div class="pull-right size-w-90">\n';
                                //discusstemp += '                                <a href="#share"><div class="icon-public pull-left"></div>分享(4)</a>\n';
                                //discusstemp += '                            </div>\n';
                                discusstemp += '                            <div class="pull-right size-w-90">\n';
                                discusstemp += '                                <a href="#reply" id="reply_number-' + result.discussion_list[i].primary_key + '"><div class="icon-reply pull-left"></div>回复(' + result.discussion_list[i].reply + ')</a>\n';
                                discusstemp += '                            </div>\n';
                                discusstemp += '                        </div>\n';
                                discusstemp += '                    </div>\n';
							
								discusstemp += '<!--显示评论列表-->\n';
                                discusstemp += '                    <div id="comment-list-' + result.discussion_list[i].primary_key + '">\n';
								k = 0;
								var last_pk_temp = '0';
								
								discusstemp += '                    <div id="comment-' + result.discussion_list[i].primary_key + '-0"></div>\n';
								for (j = 0; j < len_r; j++)
								{
									if (result.reply_list[j].discussion_key != result.discussion_list[i].primary_key)
										continue;
                                    discusstemp += '                    <div id="comment-' + result.discussion_list[i].primary_key + '-' + result.reply_list[j].primary_key + '">\n';
                                    discusstemp += '                        <div  style="display:inline-block">\n';
                                    discusstemp += '                             <img class="img-head-small" src="/img/head1.jpg">\n';
                                    discusstemp += '                        </div>\n';
                                    discusstemp += '                        <div class="pinglun-content">\n';
                                    discusstemp += '                            <div class="pinglun-bubble">\n';
                                    discusstemp += '                                <s class="pinglun-bubble-backg"></s>\n';
                                    discusstemp += '                                <s class="pinglun-bubble-shelter"></s>  \n';
                                    discusstemp += '                                <div style="display:inline" class="orange-text">' + result.reply_list[j].user_name + '：</div>\n';
                                    discusstemp += '                                <div style="display:inline">\n';
                                    discusstemp += '                                    ' + result.reply_list[j].content + '\n';
                                    discusstemp += '                                </div>\n';
                                    discusstemp += '                            </div>\n';
                                    discusstemp += '                            <div class="margin-t-5">\n';
                                    discusstemp += '                                <div class="text-hint pingluln-foot">\n';
                                    discusstemp += '                                    ' + result.reply_list[j].time + '\n';
                                    discusstemp += '                                </div>\n';
                                    discusstemp += '                                <div class="pingluln-foot" style="margin-left:5px;">\n';
                                    discusstemp += '                                    <a href="#reply" class="text-hint" onclick="javascript:pinglun_reply(\'reply-' + result.discussion_list[i].primary_key + '\',\'' + result.reply_list[j].user_name + '\', \'' + result.reply_list[j].primary_key + '\')">回复</a>\n';
                                    discusstemp += '                                </div>\n';
                                    discusstemp += '                                <div class="pingluln-foot" style="margin-left:5px;">\n';
                                    discusstemp += '                                    <a href="#like" name = "like_discussion_reply" id="like_discussion_reply-' + result.reply_list[j].primary_key + '" class="text-hint">赞(' + result.reply_list[j].like + ')</a>\n';
                                    discusstemp += '                                </div>   \n';
                                    discusstemp += '                            </div>      \n';
                                    discusstemp += '                        </div>\n';
                                    discusstemp += '                    </div>\n';
									if (parseInt(result.discussion_list[i].reply) > 2 && k == 0)
                                    {
										discusstemp += '                    <div id="xianshipl-' + result.discussion_list[i].primary_key + '">\n';
										discusstemp += '                        <a style="cursor:pointer;" onclick="javascript:slideShowReply_down(\'comment-list-more-' + result.discussion_list[i].primary_key + '\');">显示其它评论</a>\n';
										discusstemp += '                    </div>\n';
                                        discusstemp += '                <div id="comment-list-more-' + result.discussion_list[i].primary_key + '" style="display:none;">\n';
                                    }
                                                            
									if (parseInt(result.discussion_list[i].reply) > 2 && k == parseInt(result.discussion_list[i].reply) - 2)
                                    {
										discusstemp += '    </div>\n';
									}
									last_pk_temp = result.reply_list[j].primary_key;
									k = k + 1;
								}
                                if (parseInt(result.discussion_list[i].reply) > 2)
                                {                
                                    discusstemp += '                    <div id="shouqipl-' + result.discussion_list[i].primary_key + '" style="display:none;">\n';
                                    discusstemp += '                        <a style="cursor:pointer;"onclick="javascript:slideShowReply_up(\'comment-list-more-' + result.discussion_list[i].primary_key + '\');">收起其它评论</a>\n';
                                    discusstemp += '                    </div>\n';
								}
                                discusstemp += '                </div>\n';
                                discusstemp += '                <!--评论列表结束-->    \n';
								discusstemp += '                    <input type="hidden" id="last_reply_pk-' + result.discussion_list[i].primary_key + '" value="' + last_pk_temp + '" />\n';
                                discusstemp += '            </div>\n';
                                discusstemp += '        </div>\n';
                                discusstemp += '        <div id="commentinputdiv-' + result.discussion_list[i].primary_key + '" class="comment-inputdiv">\n';
                                discusstemp += '                <textarea  id="reply-' + result.discussion_list[i].primary_key + '"  rows="1" class="text-click comment-input" onblur="javascript:changeBegin(this)" onclick="javascript:changeEnd(this)" placeholder="评论："  ></textarea>\n';
                                discusstemp += '                <div id="pinglun-tijiao-' + result.discussion_list[i].primary_key + '" class="pinglun-tijiao" style="display:none">\n';
                                discusstemp += '                    <input type="button" class="pull-right" style="margin-right:7px;" name="pinglun_button" id="pinglun-button' + result.discussion_list[i].primary_key + '" value="提交">\n';
								discusstemp += '                    <input type="hidden" id="reply_pk-' + result.discussion_list[i].primary_key + '" value="' + result.discussion_list[i].primary_key + '" />\n';
								discusstemp += '                    <input type="hidden" id="tag-' + result.discussion_list[i].primary_key + '" value="1" />\n';
                                discusstemp += '                </div>\n';
                                discusstemp += '            </div>\n';
                                discusstemp += '        </div>\n';
							}
							if (last_primary_key == '0')
								$('#xinxianshi-body').html(discusstemp);
							else
								$(discusstemp).insertAfter('#discussion_content-' + last_primary_key);
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
					if(callback)
						callback();
				});
			}
			
			//加载学习经验部分
			$.fn.get_study_experience = function(page, callback)
			{
				max_study_experience_number = 20;
				$.get('/course/get_study_experience/', {course_primary_key: course_primary_key, page: page, max_study_experience_number: max_study_experience_number} ,function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							var study_experiencetemp = '';
							var i = 0;
							var len = result.study_experience_list.length;
							if(len != 0)
							{
								for(i = 0; i < len; i++)
								{
									if(i % 2 == 0 || i == 0)
										study_experiencetemp += '<div class="rzh-content-gray">\n';
									else
										study_experiencetemp += '<div class="rzh-content-white">\n';
									study_experiencetemp += '	<div class="rzh-name-size">\n';
									study_experiencetemp += '		<a href="/study_experience/id/' + result.study_experience_list[i].primary_key + '" target="_blank" class="rzh-name-link-red">\n';
									study_experiencetemp += '			' + result.study_experience_list[i].title + '\n';
									study_experiencetemp += '		</a>\n';
									study_experiencetemp += '	</div>\n';
									study_experiencetemp += '	<div class="rzh-author-size">\n';
									study_experiencetemp += '		作者：' + result.study_experience_list[i].user_name + '\n';
									study_experiencetemp += '	</div>\n';
									study_experiencetemp += '	<div class="rzh-text" style="height:40px;overflow:hidden;">\n';
									study_experiencetemp += '		' + result.study_experience_list[i].content_preview + '\n';
									study_experiencetemp += '	</div>\n';
									study_experiencetemp += '	<div class="rzh-foot">\n';
									study_experiencetemp += '		<div class="pull-left">\n';
									//study_experiencetemp += '			<div class="pull-left">\n';
									//study_experiencetemp += '				<div class="icon-public pull-left"></div>公开\n';
									//study_experiencetemp += '			</div>\n';
									study_experiencetemp += '			<div class="pull-left">\n';
									study_experiencetemp += '				' + result.study_experience_list[i].time + '\n';
									study_experiencetemp += '			</div>\n';
									study_experiencetemp += '		</div>\n';
									study_experiencetemp += '		<div class="pull-right">\n';
									study_experiencetemp += '			<div class="pull-right size-w-90">\n';
                                    study_experiencetemp += '				<a href="#like" name="like_study_experience" id="like_study_experience-' + result.study_experience_list[i].primary_key + '"><div class="icon-like pull-left"></div>赞(' + result.study_experience_list[i].like + ')</a>\n';
                                    study_experiencetemp += '			</div>\n';
									study_experiencetemp += '			<div class="pull-right size-w-90">\n';
									study_experiencetemp += '				<a href="#reply"><div class="icon-reply pull-left"></div>评论(' + result.study_experience_list[i].reply + ')</a>\n';
									study_experiencetemp += '			</div>\n';
									study_experiencetemp += '			<div class="pull-right size-w-90">\n';
									study_experiencetemp += '				<a href="#reply"><div class="icon-public pull-left"></div>阅读(' + result.study_experience_list[i].click + ')</a>\n';
									study_experiencetemp += '			</div>\n';
									study_experiencetemp += '		</div>\n';
									study_experiencetemp += '	</div>\n';
									study_experiencetemp += '</div>\n';
								}	
									
								$('#study_experience-body').html(study_experiencetemp);
							}
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
					if(callback)
						callback();
				});
			}
		</script>
		
		<!--更改关注按钮-->
		<script type="text/javascript">
			function change_concern_tag() {
				$.get('/course/change_concern_tag/', {primary_key: course_primary_key}, function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							tag = 1 - tag;
							if(tag == 0)
								$('#concern').html("+ 关注");
							else
								$('#concern').html("取消关注");
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
				});
			}
		</script>
		
		<!--加载更多按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#icon-getmore').click(function(){
					var pk = $('#last_pk').val();
					$.fn.get_discussion(pk);
				});
			});
		</script>
		
		<!--注册登录按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#login_link').click(function(){
					uInfoShow();
				});
				$('#signup_link').click(function(){
					uLogout();
				});
			});
		</script>
		
		<!--提交新鲜事按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#fazhaugntai-button').click(function(){
					$.post('/course/submit_discussion/', {course_primary_key: course_primary_key, content: $('#fazhaugntai-text').val()} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								$('#fazhaugntai-text').val('');
								zhuangtai_shouhui();
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			});
		</script>
		
		<!--写经验按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#xiejingyan-button').click(function(){
					var course_id = $('#course_id').html().replace(/[^\d]/g,"");
					var sub_id = $('#sub_id').html().replace(/[^\d]/g,"");
					var url = '/study_experience/write/' + course_id + '/' + sub_id;
					window.open(url,"_blank");
				});
			});
		</script>
		
		<!--赞某个状态的按钮-->
		<script type="text/javascript">
            $(document).ready(function () {
				$(document).on('click', 'a[name="like_discussion"]', function() {
					var id = $(this).attr('id');
					var primary_key = id.replace(/[^\d]/g,"");
					$.get('/course/like_discussion/' + Math.random(), {course_primary_key: course_primary_key, flag: '1', primary_key: primary_key} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								if(result.tag == '1')
									$('#' + id).html('<div class="icon-like pull-left"></div>赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) + 1) + ')');
								else
									$('#' + id).html('<div class="icon-like pull-left"></div>赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) - 1) + ')');
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
				
				$(document).on('click', 'a[name="like_discussion_reply"]', function() {
					var id = $(this).attr('id');
					var primary_key = id.replace(/[^\d]/g,"");
					$.get('/course/like_discussion/' + Math.random(), {course_primary_key: course_primary_key, flag: '0', primary_key: primary_key} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								if(result.tag == 1)
									$('#' + id).html('赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) + 1) + ')');
								else
									$('#' + id).html('赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) - 1) + ')');
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			});
		</script>

		<!--赞某个学习经验的按钮-->
		<script type="text/javascript">
            $(document).ready(function () {
				$(document).on('click', 'a[name="like_study_experience"]', function() {
					var id = $(this).attr('id');
					var primary_key = id.replace(/[^\d]/g,"");
					$.get('/course/like_study_experience/' + Math.random(), {course_primary_key: course_primary_key, flag: '1', primary_key: primary_key} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								if(result.tag == '1')
									$('#' + id).html('<div class="icon-like pull-left"></div>赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) + 1) + ')');
								else
									$('#' + id).html('<div class="icon-like pull-left"></div>赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) - 1) + ')');
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
				
				/*$(document).on('click', 'a[name="like_discussion_reply"]', function() {
					var id = $(this).attr('id');
					var primary_key = id.replace(/[^\d]/g,"");
					$.get('/course/like/' + Math.random(), {course_primary_key: course_primary_key, flag: '0', primary_key: primary_key} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								if(result.tag == 1)
									$('#' + id).html('赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) + 1) + ')');
								else
									$('#' + id).html('赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) - 1) + ')');
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});*/
			});
		</script>
		
		<!--回复新鲜事按钮-->
        <script type="text/javascript">
            $(document).ready(function() {
				$(document).on('click', 'input[name="pinglun_button"]', function() {
					var discussion_pk = $(this).attr("id").replace(/[^\d]/g,"");
					var content = $('#reply-' + discussion_pk).val();
					var tag = $('#tag-' + discussion_pk).val();
					var pk = $('#reply_pk-' + discussion_pk).val();
					$.post('/course/reply_discussion/', {content: content, tag: tag, primary_key: pk} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								var discusstemp = '';
								discusstemp += '                    <div id="comment-' + discussion_pk + '-' + result.reply_key + '">\n';
								discusstemp += '                        <div  style="display:inline-block">\n';
								discusstemp += '                             <img class="img-head-small" src="/img/head1.jpg">\n';
								discusstemp += '                        </div>\n';
								discusstemp += '                        <div class="pinglun-content">\n';
								discusstemp += '                            <div class="pinglun-bubble">\n';
								discusstemp += '                                <s class="pinglun-bubble-backg"></s>\n';
								discusstemp += '                                <s class="pinglun-bubble-shelter"></s>  \n';
								discusstemp += '                                <div style="display:inline" class="orange-text">' + user_name + '：</div>\n';
								discusstemp += '                                <div style="display:inline">\n';
								discusstemp += '                                    ' + content + '\n';
								discusstemp += '                                </div>\n';
								discusstemp += '                            </div>\n';
								discusstemp += '                            <div class="margin-t-5">\n';
								discusstemp += '                                <div class="text-hint pingluln-foot">\n';
								discusstemp += '                                    ' + result.time + '\n';
								discusstemp += '                                </div>\n';
								discusstemp += '                                <div class="pingluln-foot" style="margin-left:5px;">\n';
								discusstemp += '                                    <a href="#reply" class="text-hint" onclick="javascript:pinglun_reply(\'reply-' + discussion_pk + '\',\'' + user_name + '\', \'' + result.reply_key + '\')">回复</a>\n';
								discusstemp += '                                </div>\n';
								discusstemp += '                                <div class="pingluln-foot" style="margin-left:5px;">\n';
								discusstemp += '                                    <a href="#like" class="text-hint">赞(999)</a>\n';
								discusstemp += '                                    <a href="#like" name = "like_discussion_reply" id="like_discussion_reply-' + result.reply_key + '" class="text-hint">赞(0)</a>\n';
								discusstemp += '                                </div>   \n';     								
								discusstemp += '                            </div>      \n';
								discusstemp += '                        </div>\n';
								discusstemp += '                    </div>\n';

								$(discusstemp).insertAfter($('#comment-' + discussion_pk + '-' + $('#last_reply_pk-' + discussion_pk).val()));
									
								$('#last_reply_pk-' + discussion_pk).val(result.reply_key);
								
								$('#reply-' + discussion_pk).val('');		
								changeBegin($('#reply-' + discussion_pk).get(0));
								$('#reply_number-' + discussion_pk).html('<div class="icon-reply pull-left"></div>回复(' + (parseInt($('#reply_number-' + discussion_pk).html().replace(/[^\d]/g,""))+1) + ')');
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			});
		</script>
		
		<!--刷新新鲜事按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#refresh_content').click(function(){
					$.fn.get_discussion('0');
				});
			});
		</script>
		
		<link rel="shortcut icon" href="/img/icon.gif" >
		<title><?php echo $course_name ?>_最学术网站</title>
    </head>
	
    
    <body class="back-light text-font">
		<input type="hidden" id="last_pk" value="0"/>
        <div>
            <div class="container padding-t-10">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <div align="center"><img src="/img/logo-shadow.png"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row size-guidebar guidebar-backg">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="padding:0;">
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r">
                    <a id="guidebar-1" href="#zuixueshu" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-1" class="guide-change guidebar-tab-content" >最学术</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-2" href="#zuikecheng" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-2" class="guide-change guidebar-tab-content" 
                    style="color:white; background-color:#69b7d4;-webkit-box-shadow: 1px 3px 3px;-moz-box-shadow: 1px 3px 3px; box-shadow: 0px 3px 3px -1px #2a6a81;font-size:17px;">最课程</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-3" href="#zuihuodong" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-3" class="guide-change guidebar-tab-content">最活动</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-4" href="#zuishetuan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-4" class="guide-change guidebar-tab-content">最社团</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-5" href="#zuishenghuo" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-5" class="guide-change guidebar-tab-content">最生活</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-6" href="#zuikeyan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-6" class="guide-change guidebar-tab-content">最科研</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab" style="margin-left:-4px;" >
                    <a id="guidebar-7" href="#zuiqiuzhi" class="guidebar-word" onclick="javascript:guidebar_choose(this);"> <div id="guidebar-content-7" class="guide-change guidebar-tab-content">最求职</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab pull-right" style="margin-left:-5px;" >
                    <a href="#about_us" class="guidebar-word"><div  id="guanyuwomen" class="size-guidebart guidebar-tab-content ">关于我们</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a id="signup_link" href="#signup_link" class="guidebar-word" ><div id="zhuce" class="size-guidebart guidebar-tab-2-content">退出</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a  id="login_link"  href="#login_link" class="guidebar-word"><div id="denglu" class="size-guidebart guidebar-tab-2-content"></div></a>
                </div>
                
                <div class="search-bar pull-right" style="margin-top:8px;">
                        <input id="Text1" class="pull-left search-input text-click" placeholder="search for something :)">
                        <div class="pull-right size-search-button"></div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

        
        
        
        <!--以下为主题内容-->
        <div class="container">
            <div id="main" class="row">
            	<div id="main-left" class="col-md-8">
            		<div id="titlezone" class="titlezone blue-text margin-tb-10">
                    </div>
                    <div id="input-group" class="">
                    	<div id="input-icons" class="">
                            <table>
                                <tr>
                                    <td>
                                        <a href="#fazhuangtai">
                                            <div id="fazhaungtai" class="fazhuangtai"></div>
                                        </a>
                                    </td>
                                    <td>
                                        <a id = "xiejingyan-button" href="#xiejingyan">
                                            <div id="xiejingyan" class="xiejingyan"></div>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#chuanziliao">
                                            <div id="chuanziliao" class="chuanziliao"></div>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="bubble-fazhuangtai" >
                            <div class="bubble-shadow bubble">
                                <s class="bubble-shadow-backg">
                                    <s class="bubble-shadow-shelter">
                                    </s>
                                </s>
                                <textarea id="fazhaugntai-text" name="10" rows="1" class="bubble-text text-click width-99" onblur="javascript:zhuangtai_shouhui()" onclick="javascript:zhuangtai_xiala()" placeholder="说点什么吧"></textarea>
                                <div class=" bubble-sumbit-frame">
                                    <button id="fazhaugntai-button" name="fazhaugntai-button" class="bubble-submit text-click" style="display:none;">提交</button>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    
                    <div id="content" class="margin-t-20">
                        <div id="content-tabs" class="normaltab">
                            <div id="content-tools" class="pull-right size-w-40 margin-t-10">
                                <table class="abs">
                                    <tr>
                                       <!-- <td>
                                            <a href="#xxs_shuaxin" class="">
                                                <div id="xxs_shuaxin" class="xxs_shuaxin margin-lr-5"></div>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#xxs_shezhi" class="">
                                                <div id="xxs_shezhi" class="xxs_shezhi margin-lr-5"></div>
                                            </a>
                                        </td>-->
                                        <td>
                                            <a id="refresh_content" href="#xxs_paixu" class="">
                                                <div id="xxs_paixu" class="xxs_paixu margin-lr-5"></div>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs underline-bold-dblue">
                                <li class="active"><a href="#xinxianshi-page" data-toggle="tab">新鲜事</a></li>
								<li class=""><a href="#kechengpingjia-page" data-toggle="tab">课程评价</a></li>
                                <li class=""><a href="#study_experience-page" data-toggle="tab">学习经验</a></li>
                                <li class=""><a href="#xuexiziliao-page" data-toggle="tab">学习资料</a></li>
                            </ul>
                        </div>
                        <div id="content-pages" class="back-white maincontent">
                        	<!-- Tab panes -->
                            <div class="tab-content">
							
                                <!-- 左侧栏1：新鲜事 -->
                                <div class="tab-pane active" id="xinxianshi-page">
                                    <div class="xinxianshi-content">
                                        <div id = "xinxianshi-body">
                                        </div>
                                        <!--加载更多-->
                                        <div align="center"><a id="icon-getmore" href="#getmore" ><div class="icon-getmore"></div></a></div>
                                    </div>
                                </div>
								
								<div class="tab-pane active" id="kechengpingjia-page">
                                    <div class="kechengpingjia-content">
                                        <div id = "kechengpingjia-body">                         
                                        </div>
                                    </div>
                                </div>
                            
                            	<div class="tab-pane" id="study_experience-page">
                                    <div class="study_experience-content">
										<div id = "study_experience-body">
											<!--type1.灰底儿日志-->
											<!--<div class="rzh-content-gray">
												<div class="rzh-name-size">
													<a href="#rzh" class="rzh-name-link-red">
														钗头凤
													</a>
												</div>
												<div class="rzh-author-size">
													作者：路由
												</div>
												<div class="rzh-text">
													红酥手。黄滕酒。满城春色宫墙柳。东风恶。欢情薄。一怀愁绪，几年离索。错错错。
													春如旧。人空瘦。泪痕红浥鲛绡透。桃花落。闲池阁。山盟虽在，锦书难托。莫莫莫。                                                
												</div>
												<div class="rzh-foot">
													<div class="pull-left">
														<div class="pull-left">
															<div class="icon-public pull-left"></div>公开
														</div>                                                
														<div class="pull-left margin-l-10">
															 今天 09:20
														</div>
													</div>                                                
													<div class="pull-right">
														<div class="pull-right size-w-90">
															<a href="#reply"><div class="icon-reply pull-left"></div>评论4)</a>
														</div>
														<div class="pull-right size-w-90">
															<a href="#reply"><div class="icon-reply pull-left"></div>阅读(33)</a>
														</div>
													</div>                                                
												</div>                                           
											</div>-->
											<!--type2.白底儿日志-->
											<!--div class="rzh-content-white">
												<div class="rzh-name-size">
													<a href="#rzh" class="rzh-name-link-yellow">
														钗头凤
													</a>
												</div>
												<div class="rzh-author-size">
													作者：糖碗
												</div>
												<div class="rzh-text">
													世情薄，人情恶，雨送黄昏花易落。晓风干，泪痕残。欲笺心事，独语斜阑。难，难，难！
													人成各，今非昨，病魂常似秋千索。角声寒，夜阑珊。怕人寻问，咽泪装欢。瞒，瞒，瞒！
												</div>
												<div class="rzh-foot">
													<div class="pull-left">
														<div class="pull-left">
															<div class="icon-public pull-left"></div>公开
														</div>                                                
														<div class="pull-left margin-l-10">
															 昨天
														</div>
													</div>
													<div class="pull-right">
														<div class="pull-right size-w-90">
															<a href="#reply"><div class="icon-reply pull-left"></div>评论14)</a>
														</div>
														<div class="pull-right size-w-90">
															<a href="#reply"><div class="icon-reply pull-left"></div>阅读(133)</a>
														</div>
													</div>                                                
												</div>
											</div-->
											<!--type1.灰底儿日志-->
											<!--div class="rzh-content-gray">
												<div class="rzh-name-size">
													<a href="#rzh" class="rzh-name-link-green">
														钗头凤
													</a>
												</div>
												<div class="rzh-author-size">
													作者：路由
												</div>
												<div class="rzh-text">
													红酥手。黄滕酒。满城春色宫墙柳。东风恶。欢情薄。一怀愁绪，几年离索。错错错。
													春如旧。人空瘦。泪痕红浥鲛绡透。桃花落。闲池阁。山盟虽在，锦书难托。莫莫莫。                                                
												</div>
												<div class="rzh-foot">
													<div class="pull-left">
														<div class="pull-left">
															<div class="icon-public pull-left"></div>公开
														</div>                                                
														<div class="pull-left margin-l-10">
															 今天 09:20
														</div>
													</div>                                                
													<div class="pull-right">
														<div class="pull-right size-w-90">
															<a href="#reply"><div class="icon-reply pull-left"></div>评论4)</a>
														</div>
														<div class="pull-right size-w-90">
															<a href="#reply"><div class="icon-reply pull-left"></div>阅读(33)</a>
														</div>
													</div>                                                
												</div>                                           
											</div-->
											<!--type2.白底儿日志-->
											<!--div class="rzh-content-white">
												<div class="rzh-name-size">
													<a href="#rzh" class="rzh-name-link-blue">
														钗头凤
													</a>
												</div>
												<div class="rzh-author-size">
													作者：糖碗
												</div>
												<div class="rzh-text">
													世情薄，人情恶，雨送黄昏花易落。晓风干，泪痕残。欲笺心事，独语斜阑。难，难，难！
													人成各，今非昨，病魂常似秋千索。角声寒，夜阑珊。怕人寻问，咽泪装欢。瞒，瞒，瞒！
												</div>
												<div class="rzh-foot">
													<div class="pull-left">
														<div class="pull-left">
															<div class="icon-public pull-left"></div>公开
														</div>                                                
														<div class="pull-left margin-l-10">
															 昨天
														</div>
													</div>
													<div class="pull-right">
														<div class="pull-right size-w-90">
															<a href="#reply"><div class="icon-reply pull-left"></div>评论14)</a>
														</div>
														<div class="pull-right size-w-90">
															<a href="#reply"><div class="icon-reply pull-left"></div>阅读(133)</a>
														</div>
													</div>                                                
												</div>
											</div-->
										</div>
                                    </div>
                                </div>
                                
                                <div class="tab-pane" id="xuexiziliao-page">
                                    hehe3
                                </div>
                            </div>
                        </div>
                    
                    </div>
                    
                    
            	</div>
            	<!-- 右侧栏 -->
            	<div id="main-right" class="col-md-4 margin-t-80">
                    <div id="course-tabs" class="normaltab">                        
                        <ul class="nav nav-tabs underline-bold-dorange">
                            <li class="active"><a href="#kechengxinxi-page" data-toggle="tab">课程信息</a></li>
                        </ul>
                    </div>
                    <div  id="content-pages" class="back-white maincontent">
                        <div class="tab-content">
                            <!-- 右侧栏1：课程信息 -->
                            <div class="tab-pane active" id="kechengxinxi-page">
                                <div class="kechengxinxi-content">
                                    <div id="course_name">
                                        <span class="gray-text">课程名：</span><span class="blue-text righttitlezone" id="course_name_span" ></span> <a href="#recommend" class="no-underline pull-right"><span class="recommend gray-text">推荐(56)</span></a>
                                    </div>
                                    <div id="course_teacher" class="margin-t-10 div-border">
                                    </div>
                                    <div id="course_place" class="margin-t-10 div-border">
                                        <span class="gray-text">上课地点：</span>FIT楼3228
                                    </div>
                                    <div id="course_time" class="margin-t-10 div-border">
                                    </div>
                                    <!--div id="course_week" class="margin-t-10">
                                        <span class="gray-text">周次：</span>前八周（当前为第十六周）
                                    </div-->
                                    <div id="course_cap_1" class="margin-t-10">
                                    </div>
                                    <div id="course_cap_2" class="margin-t-10">
                                    </div>
                                    <div id="course_credit" class="margin-t-10">
                                    </div>
                                    <div id="course_id" class="margin-t-10">
                                    </div>
                                    <div id="sub_id" class="margin-t-10">
                                    </div>
                                    <div id="course_department" class="margin-t-10">
                                    </div>
                                    <div id="course_feature" class="margin-t-10">
                                    </div>
                                    <div id="course_description" class="margin-t-10">
                                    </div>
                                    
                                    <div class="thanks_bubble thanks_bubble-shadow">
                                        <s class="thanks_bubble-shadow-backg">
                                            <s class="thanks_bubble-shadow-shelter"> 
                                            </s>
                                        </s>
                                        <div class="thank-ganxie">感谢</div>
                                        <div class="thank-head">
                                            <div class="thank-head-frame">
                                                <img class="img-head-small" src="/img/head1.jpg">
                                                <div class="thank-name"><a  href="#namelink" class="text-name-link">曾小贤</a></div>
                                            </div>
                                        </div>
                                        <div class="thank-fenxiang">的分享</div>
                                        <s class="thanks_bubble-shadow-backg">
                                            <s class="thanks_bubble-shadow-shelter"> 
                                            </s>
                                        </s>
                                    </div>
                                    <br/>
                                    <br/>
                                    <!--数据展示窗口-->
                                    <div class="xxs-content-status">
                                        <div id="pingfen">
                                            <span class="blue-text righttitlezone" ><b>我来评分</b></span> <span class="gray-text pull-right">(已有2人参与评分)</span>
                                        </div>
                                        <div id="hanjinliang" class="margin-t-10">
                                            <div class="slider-name">
                                                含金量
                                            </div>
                                            <div id="slidercontainer-hjl" class="slider-slider">  
                                            </div> 
                                            <div id="hjl" class="slider-fenshu">10</div>                                
                                        </div>
                                        <div class="zhushi gray-text">
                                            课程水不水
                                        </div>
                                        <div id="qingsongdu" class="margin-t-10">
                                            <div class="slider-name">
                                                轻松度
                                            </div>
                                            <div id="slidercontainer-qsd" class="slider-slider">  
                                            </div> 
                                            <div id="qsd" class="slider-fenshu">10</div>                                
                                        </div>
                                        <div class="zhushi gray-text">
                                            点名、签到、作业...
                                        </div>
                                        <div id="chengjibiao" class="margin-t-10">
                                            <div class="slider-name">
                                                成绩表
                                            </div>
                                            <div id="slidercontainer-chjb" class="slider-slider">  
                                            </div> 
                                            <div id="chjb" class="slider-fenshu">10</div>                                
                                        </div>
                                        <div class="zhushi gray-text">
                                            给分厚道不厚道
                                        </div>
                                        <br />
                                        <div>
                                            <button name="fazhaugntai-button" class="pull-right pingfen-submit text-click">提交</button>
                                        </div>
                                        <br />
                                        <div id="pingfen">
                                            <span class="orange-text righttitlezone" ><b>评分标准</b></span>
                                        </div>
                                        <div id="circle-graph">
                                            <div id="weights">
                                                <div>作业 <span id="w-zuoye">65</span>%</div>
                                                <div>签到 <span id="w-qiandao">20</span>%</div>
                                                <div>考试 <span id="w-kaoshi">10</span>%</div>
                                                <div>小组活动 <span id="w-xiaozu">5</span>%</div>
                                            </div>
                                            <div id="holder">
                                                <!--此处用于绘制图形-->
                                            </div>
                                        </div>      
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>                
            </div>
            <br/>
            <div class="test">
            	Copyright © 2014 zuixueshu.sinaapp.com
            </div>
        </div>
        <!--修改密码表单-->
        <div id="CPWcontent" class="abs" style="display: none;">
            <div id="changepw-inner">
                <div id="cpw-head">
                    <div class="login-cancel" onclick="uCPWdisplay();"></div>
                </div>
                <form id="form_login" name="form-login1">
                    <div>
                        <div id="cpw-fragment-oldpw">
                            <input class="cpw-input text-click" type="password" />
                        </div>
                        <div id="cpw-fragment-newpw">
                            <input class="cpw-input text-click" type="password" />
                        </div>
                        <div id="cpw-fragment-conpw">
                            <input class="cpw-input text-click" type="password" />
                        </div>
                        <div class="cpw-fragment-blank-s"></div>
                        <div id="cpw-fragment-save" onclick="uCPWdisplay();"></div>
                    </div>
                </form>
                <div id="cpw-foot">
                </div>
            </div>
        </div>
        <!--登录表单-->
        <div id="logincontent" class="abs" style="display: none;">
            <div id="login-innner">
                <div id="login-head">
                    <div class="login-cancel" onclick="uLogindisplay();"></div>
                </div>
                <form id="form1" name="form-login1">
                    <div>
                        <div class="login-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="login-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="login-fragment-checkcode">
                            <input class="login-input-checkcode text-click" type="text"/>
                            <img class="login-checkcode-pic" src=""/>
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-blank-m autologin" >
                            <input type="checkbox" id="input_autologin"/>
                            下次自动登录
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-loginbtn" onclick="uLogindisplay();"></div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-tosignup" onclick="uLogindisplay();uRegShow();"></div>
                    </div>
                </form>
                <div id="login-foot">
                </div>
            </div>
        </div>
        <!--注册表单-->
        <div id="signupcontent" class="abs" style="display: none;">
            <div id="signup-innner">
                <div id="signup-head">
                    <div class="login-cancel" onclick="uRegdisplay();"></div>
                </div>
                <form id="form_signup" name="form-signup1">
                    <div>
                        <div class="signup-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-idnum">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-name">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-checkcode">
                        	<input class="login-input-checkcode text-click" type="text"/>
                            <img class="signup-checkcode-pic" src=""/>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-agreement" onclick="open_agreement();">
                        	<div id="signup-agree" class="signup-fragment-needagree" onclick="agreement_toggle();"></div>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-signupbtn" onclick="uRegdisplay();"></div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-tologin" onclick="uRegdisplay();uLoginShow();"></div>
                    </div>
                </form>
                <div id="signup-foot">
                </div>
            </div>
        </div>   
    </body>
    <script type="text/javascript">
        
        var oImgs = $(".group-stream-img img");
        var l = oImgs.length, urlArr = [];
        for(var i=0; i<l; i++){
            urlArr.push(oImgs[i].src.replace("s256","s512"));
            oImgs[i].onclick = function(i){
                return function(){
                    fnPicShow(urlArr,i);
                }
            }(i);
        }
    </script>
    <script src="/js/index.js"></script>

</html>
﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <!--script type="text/javascript" src="/js/comment.js"></script-->  
        <link rel="stylesheet" type="text/css" href="/css/index.css">
        <link rel="stylesheet" type="text/css" href="/css/liulanrizhi.css">

        <!--复选框专用-->
        <style>
            label { 
				display: block; 
				cursor: pointer; 
				line-height: 19px; 
				margin-bottom: 10px; 
				text-shadow: 0 -1px 0 rgba(0,0,0,.2); 
			}
			
			.label_check input{ 
				margin-right: 5px; 
			}
			
			.has-js .label_check{ 
				padding-left: 34px; 
			}
						
			.has-js .label_check{ 
				background: url("/img/checkbox-bg.png") no-repeat; 
			}
			
			.has-js .label_check { 
				background-position: 0 0px;
			}
			.has-js label.c_on { 
				background-position: 0 -35px;
			}
			
			.has-js .label_check input{ 
				position: absolute; 
				left: -9999px; 
			} 
        </style>
        <!--PHP变量赋值给js-->
		<script type = "text/javascript">
			<?php
				if (isset($user_name))
				{
					echo "var user_name = '" . $user_name . "';\n";
				}
				if (isset($user_id))
				{
					echo "var user_id = '" . $user_id ."';\n";
				}
				if (isset($primary_key))
				{
					echo "var course_primary_key = '" . $primary_key ."';\n";
				}
				if (isset($study_experience_key))
				{
					echo "var study_experience_key = '" . $study_experience_key ."';\n";
				}
			?>
		</script>
		
		<!--页面初始化-->
		<script type="text/javascript">
			$(document).ready(function(){
				//加载姓名部分
				$('#denglu').html(user_name);
				$('#zhuce').html("退出");	
				getCourseInfo(course_primary_key);
				getRizhiInfo(study_experience_key);	
				$.fn.get_study_experience_reply();
			});
			//载入用户信息及退出函数
			function uInfoShow()
			{
				location.href = '/user/' + user_id;
			}
			function uLogout()
			{
				$.get('/login/logout/'+ Math.random(),function(result){
					location.href = '/';
				});
			}
			
			//加载评论部分
			$.fn.get_study_experience_reply = function(callback)
			{
				$.get('/study_experience/get_study_experience_reply/', {study_experience_key: study_experience_key} ,function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							var replytemp = '';
							var i = 0;
							var len = result.study_experience_reply_list.length;
							if(len != 0)
							{
								for(i = 0; i < len; i++)
								{
									replytemp += '<div id="comment-1-' + i + '">\n';
									replytemp += '	<div  style="display:inline-block">\n';
									replytemp += '		<img class="img-head-small" src="/img/head1.jpg">\n';
									replytemp += '	</div>\n';
									replytemp += '	<div class="liulanrzh-pinglun-content">\n';
									replytemp += '		<div class="liulanrzh-pinglun-bubble">\n';
									replytemp += '			<s class="liulanrzh-pinglun-bubble-backg"></s>\n';
									replytemp += '			<s class="liulanrzh-pinglun-bubble-shelter"></s>\n';
									replytemp += '			<div style="display:inline" class="orange-text">' + result.study_experience_reply_list[i].user_name + '</div>\n';
									replytemp += '			<div style="display:inline">\n';
									replytemp += '				' + result.study_experience_reply_list[i].content + '\n';
									replytemp += '			</div>\n';
									replytemp += '		</div>\n';
									replytemp += '		<div class="margin-t-5">\n';
									replytemp += '			<div class="text-hint pingluln-foot">' + result.study_experience_reply_list[i].time + '</div>\n';
									replytemp += '			<div class="liulanrzh-pingluln-foot" style="margin-left:5px;">\n';
									replytemp += '				<a href="#reply" class="text-hint" onclick="javascript:pinglun_reply(\'' + result.study_experience_reply_list[i].primary_key + '\',\'' + result.study_experience_reply_list[i].user_name + '\')">回复(' + result.study_experience_reply_list[i].reply + ')</a>\n';
									replytemp += '			</div>\n';
									replytemp += '			<div class="liulanrzh-pingluln-foot" style="margin-left:5px;">\n';
									replytemp += '				<a href="#like" class="text-hint" name="like_study_experience_reply" id="like_study_experience_reply-' + result.study_experience_reply_list[i].primary_key + '" >赞(' + result.study_experience_reply_list[i].like + ')</a>\n';
									replytemp += '			</div>\n';
									replytemp += '		 </div>\n';
									replytemp += '	</div>\n';
									replytemp += '</div>\n';
								}
								
								$('#comment-list-in').html(replytemp);
							}
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
					if(callback)
						callback();
				});
			}
		</script>
		<!--注册登录按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#login_link').click(function(){
					uInfoShow();
				});
				$('#signup_link').click(function(){
					uLogout();
				});
			});
		</script>
		<!--函数-->
        <script type="text/javascript">
			//加载课程信息部分
			function getCourseInfo(course_primary_key)
			{
				$.get('/course/get_course_info/', {primary_key: course_primary_key} ,function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							$('#titlezone_course_name').html('<a href="/course/id/' + result.course_info['course_id'] + '/' + result.course_info['sub_id'] + '">' + result.course_info['course_name'] + '</a>');
							$('#course_teacher').html('主讲教师：' + result.course_info['teacher_name']);
							$('#course_place').html('教室：');
							$('#course_cap1').html('本科生课容量：' + result.course_info['student_capacity_1']);
							$('#course_cap2').html('研究生课容量：' + result.course_info['student_capacity_2']);
							$('#course_credit').html('学分：' + result.course_info['credit']);
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
				});
			}
			
			//加载日志信息部分
			function getRizhiInfo(study_experience_key)
			{
				$.get('/study_experience/get_rizhi_info/', {study_experience_key: study_experience_key} ,function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							$('#rizhi-title').html(result.study_experience['title']);
							$('#rizhi-author').html(result.study_experience['user_name']);
							$('#rizhi-content').html(result.study_experience['content']);
							$('#study_experience_like').html("<div class=\"icon-like pull-left\"></div>赞(" + result.study_experience.like + ")");
							$('#study_experience_reply').html("<div class=\"icon-reply pull-left\"></div>评论(" + result.study_experience.reply + ")");
							$('#study_experience_click').html("<div class=\"icon-public pull-left\"></div>阅读(" + result.study_experience.click + ")");
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
				});
			}
		</script>
		
		<script type="text/javascript">
			//提交评论
			$(document).ready(function() {
				$('#liulanrzh-pinglun-button1').click(function() {
					$.post('/study_experience/submit_study_experience_reply/', {study_experience_key: study_experience_key, content: $('#reply-1').val(), reply_pk: $('#reply-pk').val()} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								$('#reply-1').val('');
								changeEnd($('#reply-1').get(0));
								$.fn.get_study_experience_reply();
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			});
			
			//赞学习经验
			$(document).ready(function() {
			    $('#study_experience_like').click(function() {
					$.get('/course/like_study_experience/' + Math.random(), {course_primary_key: course_primary_key, flag: '1', primary_key: study_experience_key} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								if(result.tag == '1')
									$('#study_experience_like').html('<div class="icon-like pull-left"></div>赞(' + (parseInt($('#study_experience_like').html().replace(/[^\d]/g,"")) + 1) + ')');
								else
									$('#study_experience_like').html('<div class="icon-like pull-left"></div>赞(' + (parseInt($('#study_experience_like').html().replace(/[^\d]/g,"")) - 1) + ')');
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
			    });
			});
			
			//赞学习经验回复
			$(document).ready(function() {
			    $(document).on('click', 'a[name="like_study_experience_reply"]', function() {
			    	var id = $(this).attr('id');
					var primary_key = id.replace(/[^\d]/g,"");
					$.get('/course/like_study_experience/' + Math.random(), {course_primary_key: course_primary_key, flag: '2', primary_key: primary_key} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								if(result.tag == '1')
									$('#' + id).html('赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) + 1) + ')');
								else
									$('#' + id).html('赞(' + (parseInt($('#' + id).html().replace(/[^\d]/g,"")) - 1) + ')');
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
			    });
			});
		</script>
		<link rel="shortcut icon" href="/img/icon.gif" >
		<title>学习经验_最学术网站</title>
    </head>
 

    <body class="back-light text-font">
        <div>
            <div class="container padding-t-10">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <div align="center"><img src="/img/logo-shadow.png"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row size-guidebar guidebar-backg">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="padding:0;">
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r">
                    <a id="guidebar-1" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-1" class="guide-change guidebar-tab-content guidebar-tab-rad">最学术</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-2" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-2" class="guide-change guidebar-tab-content" 
                    style="color:white; background-color:#69b7d4;-webkit-box-shadow: 1px 3px 3px;-moz-box-shadow: 1px 3px 3px; box-shadow: 0px 3px 3px -1px #2a6a81;font-size:17px;">最课程</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-3" href="#zuihuodong" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-3" class="guide-change guidebar-tab-content">最活动</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-4" href="#zuishetuan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-4" class="guide-change guidebar-tab-content">最社团</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-5" href="#zuishenghuo" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-5" class="guide-change guidebar-tab-content">最生活</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-6" href="#zuikeyan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-6" class="guide-change guidebar-tab-content">最科研</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab" style="margin-left:-4px;" >
                    <a id="guidebar-7" href="#zuiqiuzhi" class="guidebar-word" onclick="javascript:guidebar_choose(this);"> <div id="guidebar-content-7" class="guide-change guidebar-tab-content">最求职</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab pull-right" style="margin-left:-5px;" >
                    <a href="#about_us" class="guidebar-word"><div  id="guanyuwomen" class="size-guidebart guidebar-tab-content ">关于我们</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a id="signup_link" href="javascript:;" class="guidebar-word"><div id="zhuce" class="size-guidebart guidebar-tab-2-content"></div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a  id="login_link"  href="javascript:;" class="guidebar-word"><div id="denglu" class="size-guidebart guidebar-tab-2-content"></div></a>
                </div>
                
                <div class="search-bar pull-right" style="margin-top:8px;">
                        <input id="Text1" class="pull-left search-input text-click" placeholder="search for something :)">
                        <div class="pull-right size-search-button"></div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        
              
        <!--以下为主题内容-->
        <div class="container">            
            <div id="main">
                <!--日志title-->
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-offset-1  row">
                        <div id ='titlezone_course_name' class="titlezone blue-text pull-left" style="margin-left:-15px;margin-right:40px;">
                        </div>
                        <div class="col-md-9" style="margin-top:11px;">
                            <!--div class="col-md-1" style="margin-left:-25px;padding:0;">
                                 <a href="#addconcern" class="no-underline"><span id="titlezone_concern" class="liulanrzh-add-concern">+ 关注</span></a>
                             </div-->
                             <div class="col-md-11 gray-text">
                             <div id ="course_teacher" style="display:inline-block; margin-left:20px; font-size:16px;">教师：某某</div>
                             <div id ="course_place" style="display:inline-block; margin-left:25px; font-size:16px;">教室：六教6A109</div>
                             <div id ="course_credit" style="display:inline-block; margin-left:25px; font-size:16px;">学分：2</div>
                             <div id ="course_cap1" style="display:inline-block; margin-left:25px; font-size:16px;">课容量：200</div>
                             <div id ="course_cap2" style="display:inline-block; margin-left:25px; font-size:16px;">研究生课容量：0</div>
                             </div>
                        </div>
                    </div>
                  
                    
                </div>
                <!--日志正文-->
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-10 col-md-offset-1 row liulanrzh-content" >
                        <div style="position:relative;">
                            <s class="liulanrzh-bubble-shadow"></s>
                        </div>
                        <!--标题-->
                        <div style="height:30px; margin-top:15px;" class="">
                            <div id="rizhi-title" ="display:inline-block;font-size:18px; font-weight:bold; color:#f8a501;">
                            </div>
                            <div  style="display:inline-block;float:right;">
                                <a href="#share">  <img src="/img/rizhi-share.png" /> </a>
                            </div>
                        </div>
                        <!--作者-->
                        <div>
                            <div style="display:inline-block;">作者：</div>
                            <div id="rizhi-author" style="display:inline-block;" class="blue-text"></div>
                        </div>
                        <!--正文-->
                        <div id="rizhi-content" style="margin-top:25px;">
                        </div>
                        <!--foot-->
                        <div class="margin-t-10 padding-lr-10 size-h-40">
                            <div class="text-hint">
								<div class="pull-left">
									<div class="icon-public pull-left"></div>公开
								</div>
								<div class="pull-left margin-l-10"></div>
							</div>
							<div class="pull-right">
								<div class="pull-right size-w-90">
									<a href="#like" id="study_experience_like"><div class="icon-like pull-left"></div>赞()</a>
								</div>
								<!--div class="pull-right size-w-90">
									<a href="#share"><div class="icon-public pull-left"></div>分享()</a>
								</div-->
								<div class="pull-right size-w-90">
									<a href="#reply" onclick="javascript:slideShowReply('comment-list-1');" id="study_experience_reply"><div class="icon-reply pull-left"></div>评论()</a>
								</div>
								<div class="pull-right size-w-90">
									<a href="#read" id="study_experience_click"><div class="icon-public pull-left"></div>阅读()</a>
								</div>
							</div>
						</div>
						
                        <!--显示评论列表-->
                        <div class="xxs-content-status">
                            <div id="comment-list-1">
								<div id="comment-list-in">
								</div>
							<div id=".liulanrzh-commentinputdiv-1" class="liulanrzh-comment-inputdiv">
								<textarea  id="reply-1"  rows="1"  class="text-click liulanrzh-comment-input" onblur="javascript:changeBegin(this)" onclick="javascript:changeEnd(this)" placeholder="评论："  value="" ></textarea>
								<div id="liulanrzh-pinglun-tijiao-1" class="liulanrzh-pinglun-tijiao"  style="display:none">
									<input type="button" class="pull-right" style="margin-right:7px;" id="liulanrzh-pinglun-button1" value="提交">
									<input type="hidden" id="reply-pk" value="0">
								</div>
							</div>
						</div>
                        <!--评论列表结束-->
                        <!--评论框-->
					</div>
				</div>
			</div>
        </div>
        <!--修改密码表单-->
        <div id="CPWcontent" class="abs" style="display: none;">
            <div id="changepw-inner">
                <div id="cpw-head">
                    <div class="login-cancel" onclick="uCPWdisplay();"></div>
                </div>
                <form id="form_login" name="form-login1">
                    <div>
                        <div id="cpw-fragment-oldpw">
                            <input class="cpw-input text-click" type="password" />
                        </div>
                        <div id="cpw-fragment-newpw">
                            <input class="cpw-input text-click" type="password" />
                        </div>
                        <div id="cpw-fragment-conpw">
                            <input class="cpw-input text-click" type="password" />
                        </div>
                        <div class="cpw-fragment-blank-s"></div>
                        <div id="cpw-fragment-save" onclick="uCPWdisplay();"></div>
                    </div>
                </form>
                <div id="cpw-foot">
                </div>
            </div>
        </div>
        <!--登录表单-->
        <div id="logincontent" class="abs" style="display: none;">
            <div id="login-innner">
                <div id="login-head">
                    <div class="login-cancel" onclick="uLogindisplay();"></div>
                </div>
                <form id="form1" name="form-login1">
                    <div>
                        <div class="login-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="login-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="login-fragment-checkcode">
                            <input class="login-input-checkcode text-click" type="text"/>
                            <img class="login-checkcode-pic" src="http://placehold.it/120x50"/>
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-blank-m autologin" >
                            <input type="checkbox" id="input_autologin"/>
                            下次自动登录
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-loginbtn" onclick="uLogindisplay();"></div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-tosignup" onclick="uLogindisplay();uRegShow();"></div>
                    </div>
                </form>
                <div id="login-foot">
                </div>
            </div>
        </div>
        <!--注册表单-->
        <div id="signupcontent" class="abs" style="display: none;">
            <div id="signup-innner">
                <div id="signup-head">
                    <div class="login-cancel" onclick="uRegdisplay();"></div>
                </div>
                <form id="form_signup" name="form-signup1">
                    <div>
                        <div class="signup-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-idnum">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-name">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-checkcode">
                        	<input class="login-input-checkcode text-click" type="text"/>
                            <img class="signup-checkcode-pic" src="http://placehold.it/120x50"/>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-agreement" onclick="open_agreement();">
                        	<div id="signup-agree" class="signup-fragment-needagree" onclick="agreement_toggle();"></div>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-signupbtn" onclick="uRegdisplay();"></div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-tologin" onclick="uRegdisplay();uLoginShow();"></div>
                    </div>
                </form>
                <div id="signup-foot">
                </div>
            </div>
        </div>
    </body>
    <script src="/js/index.js"></script>
    <script src="/js/raphael-min.js"></script>


	<!--回复框-->
	<script type="text/javascript">
		function changeBegin(obj) {
			var textid = obj.id;
			flag = document.getElementById(textid).value;
			var num = textid.replace(/[^\d]/g,"");
			var pingluntijiao = 'liulanrzh-pinglun-tijiao-' + num;
			if (flag == "") {
				$("#" + textid).attr('style', 'border:0px solid #f0f2f5;height:26px;');
				$("#" + pingluntijiao).attr("style", "display:none");
			}
			else {
				return;
			}
			$("#reply-pk").val('0');
		}
		function changeEnd(obj) {
			var textid = obj.id;
			$("#" + textid).attr('style', 'border:1px solid #f0f2f5;height:52px;');
			var num = textid.replace(/[^\d]/g,"");
			var pingluntijiao = 'liulanrzh-pinglun-tijiao-' + num;
			$("#" + pingluntijiao).attr("style", "display:block");
		}
		function pinglun_reply(primary_key, l) {
			var a;
			a = '回复' + l + '： ';
			$("#reply-1").val(a);
			$("#reply-pk").val(primary_key);
		}
	</script>
</html>
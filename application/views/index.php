<!DOCTYPE html>
<!--2014年1月29日17:12:49 盛茂家 更新-->
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/raphael-min.js"></script>
        <script type="text/javascript" src="/js/jquery-1.2.6.js"></script>  
        <script type="text/javascript" src="/js/albumshow.js"></script> 
        <link rel="stylesheet" type="text/css" href="/css/index.css">
		
        <link rel="shortcut icon" href="/img/icon.gif" >
		<title>最学术网站</title>
		
		<!--PHP变量赋值给js-->
        <script type = "text/javascript">
			<?php
				if (isset($name))
				{
					echo "var name = '" . $name . "';\n";
				}
				if (isset($user_id))
				{
					echo "var user_id = '" . $user_id ."';\n";
				}
				if (isset($flag_authCode))
				{
					echo "var flag_authCode = " . ($flag_authCode? "true": "false") . ";\n";
				}
				echo "var flag_login = " . ($flag_login? "true": "false") . ";\n";
			?>
		</script>
		<!--页面初始化-->
		<script type="text/javascript">
			$(document).ready(function(){
				//加载姓名部分
				if (flag_login)
				{
					$('#denglu').html(name);
					$('#zhuce').html("退出");
					//$('#login_link').attr("onclick", "").click(function(){
					//	uInfoShow();});
					//$('#signup_link').attr("onclick", "").click(function(){
					//	uLogout();});
				}
				else
				{
					$('#denglu').html("登陆");
					$('#zhuce').html("注册");
				}
				
			});
			//载入用户信息及退出函数
			function uInfoShow()
			{
				location.href = '/user/' + user_id;
			}
			function uLogout()
			{
				//alert('1234');
				$.get('/login/logout/'+ Math.random(),function(result){
				//$('#denglu').html('');
				//$('#denglu').css("background-image","url(http://zuixueshu-file.stor.sinaapp.com/img/bar-guide.png)");
				//$('#zhuce').html('');
				//$('#zhuce').css("background-image","url(http://zuixueshu-file.stor.sinaapp.com/img/bar-guide.png)");
				//$('#login_link').attr("onclick", "").click(function(){
				//	uLoginShow();});
				//$('#signup_link').attr("onclick", "").click(function(){
				//	uRegShow();});
				//	alert(result);
					location.href = '/';
				});
			}	
		</script>
        <script type="text/javascript">
			$(document).ready(function(){
				$('#form_login').submit(function(){
					$.post("/login", $("#form_login").serialize(), function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								location.href = result.url;
							}
							else
							{
								alert(result.failDesc);
								if (result.authCode)
								{
									$('#authCode_part_login').show();
									auth_refresh_login();
								}
								else
								{
									$('#authCode_part_login').hide();
								}
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			})

			$(document).ready(function(){
				$('#form_signup').submit(function(){
					$.post("/register", $("#form_signup").serialize(), function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								location.href = result.url;
							}
							else
							{
								alert(result.failDesc);
								if (result.authCode)
								{
									$('#authCode_part_register').show();
									auth_refresh_signup();
								}
								else
								{
									$('#authCode_part_register').hide();
								}
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
						
					});
				});
			})
		</script>
		
		<!--验证码部分-->
		<script type="text/javascript">
			function auth_refresh_login()
			{
				var url = '<?php echo "/imgauthcode/show/";?>'+Math.random();
				$('#authcode_image_login').attr('src', url);
			}
			function auth_refresh_signup()
			{
				var url = '<?php echo "/imgauthcode/show/";?>'+Math.random();
				$('#authcode_image_signup').attr('src', url);
			}	
		</script>
		
		<!--注册登录按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#login_link').click(function(){
					if (!flag_login)
					{
						uLoginShow();
						if (!flag_authCode)
						{
							$('#authCode_part_login').hide();
						}
						else
						{
							$('#authCode_part_login').show();
							auth_refresh_login();
						}
					}
					else
					{
						uInfoShow();
					}
				});
				$('#signup_link').click(function(){
					if (!flag_login)
					{
						uRegShow();
						if (!flag_authCode)
						{
							$('#authCode_part_register').hide();
						}
						else
						{
							$('#authCode_part_register').show();
							auth_refresh_signup();
						}
					}
					else
					{
						uLogout();
					}
				});
				$('#signup_to_login_btn').click(function(){
					uRegdisplay();
					uLoginShow();
					if (!flag_authCode)
					{
						$('#authCode_part_login').hide();
					}
					else
					{
						$('#authCode_part_login').show();
						auth_refresh_login();
					}
				});
				$('#login_to_signup_btn').click(function(){
					uLogindisplay();
					uRegShow();
					if (!flag_authCode)
					{
						$('#authCode_part_signup').hide();
					}
					else
					{
						$('#authCode_part_signup').show();
						auth_refresh_signup();
					}
				});
               /* $.fn.jSlider({
                    renderTo: '#slidercontainer-hjl',
                    size: {
                        barWidth: 220,
                        sliderWidth: 15
                    },
                    onChanging: function (percentage, e) {
                        hjl.innerText = Math.floor(percentage * 10);
                        //在控制台输出信息  
                        window.console && console.log('percentage: %s', percentage);
                    }
                });
                $.fn.jSlider({
                    renderTo: '#slidercontainer-qsd',
                    size: {
                        barWidth: 220,
                        sliderWidth: 15
                    },
                    onChanging: function (percentage, e) {
                        qsd.innerText = Math.floor(percentage * 10);
                        //在控制台输出信息  
                        window.console && console.log('percentage: %s', percentage);
                    }
                });
                $.fn.jSlider({
                    renderTo: '#slidercontainer-chjb',
                    size: {
                        barWidth: 220,
                        sliderWidth: 15
                    },
                    onChanging: function (percentage, e) {
                        chjb.innerText = Math.floor(percentage * 10);
                        //在控制台输出信息  
                        window.console && console.log('percentage: %s', percentage);
                    }
                });*/
            });  
        </script> 
    </head>
    
    <body class="back-light text-font">
        <div>
            <div class="container padding-t-10">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <div align="center"><img src="/img/logo-shadow.png"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row size-guidebar guidebar-backg" style="margin-bottom:2px;">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="padding:0;">
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r">
                    <a id="guidebar-1" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-1" class="guide-change guidebar-tab-content guidebar-tab-rad" 
                    style="color:white; background-color:#69b7d4;-webkit-box-shadow: 1px 3px 3px;-moz-box-shadow: 1px 3px 3px; box-shadow: 0px 3px 3px -1px #2a6a81;font-size:17px;">最学术</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-2" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-2" class="guide-change guidebar-tab-content">最课程</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-3" href="#zuihuodong" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-3" class="guide-change guidebar-tab-content">最活动</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-4" href="#zuishetuan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-4" class="guide-change guidebar-tab-content">最社团</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-5" href="#zuishenghuo" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-5" class="guide-change guidebar-tab-content">最生活</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-6" href="#zuikeyan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-6" class="guide-change guidebar-tab-content">最科研</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab" style="margin-left:-4px;" >
                    <a id="guidebar-7" href="#zuiqiuzhi" class="guidebar-word" onclick="javascript:guidebar_choose(this);"> <div id="guidebar-content-7" class="guide-change guidebar-tab-content">最求职</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab pull-right" style="margin-left:-5px;" >
                    <a href="#about_us" class="guidebar-word"><div  id="guanyuwomen" class="size-guidebart guidebar-tab-content ">关于我们</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a id="signup_link" href="javascript:;" class="guidebar-word"><div id="zhuce" class="size-guidebart guidebar-tab-2-content"></div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a  id="login_link"  href="javascript:;" class="guidebar-word"><div id="denglu" class="size-guidebart guidebar-tab-2-content"></div></a>
                </div>
                
                <div class="search-bar pull-right" style="margin-top:8px;">
                        <input id="Text1" class="pull-left search-input text-click" placeholder="search for something :)">
                        <div class="pull-right size-search-button"></div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        
        
        <!--以下为主题内容-->
        <div class="">
            <div id="main" class="">
            	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                      <img src="http://placehold.it/1366x500" alt="..." style="width:100%;">
                      <div class="carousel-caption">
                        介绍文字1
                      </div>
                    </div>
                    <div class="item">
                      <img src="http://placehold.it/1366x500" alt="..." style="width:100%;">
                      <div class="carousel-caption">
                        介绍文字2
                      </div>
                    </div>
                  </div>
                
                  <!-- Controls -->
                  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                  </a>
                  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-4">
                    <div class="firstpage-intro1">介绍文字</div>
                </div>
                <div class="col-md-4">
                    <div class="firstpage-intro1">介绍文字</div>
                </div>
                <div class="col-md-4">
                    <div class="firstpage-intro1">介绍文字</div>
                </div>
                
                
            </div>
            <br/>
        </div>
        
        <!--修改密码表单-->
        <div id="CPWcontent" class="abs" style="display: none;">
            <div id="changepw-inner">
                <div id="cpw-head">
                    <div class="login-cancel" onclick="uCPWdisplay();"></div>
                </div>
                <form id="form_change_password" name="form-login1">
                    <div>
                        <div id="cpw-fragment-oldpw">
                            <input class="cpw-input text-click" type="password" />
                        </div>
                        <div id="cpw-fragment-newpw">
                            <input class="cpw-input text-click" type="password" />
                        </div>
                        <div id="cpw-fragment-conpw">
                            <input class="cpw-input text-click" type="password" />
                        </div>
                        <div class="cpw-fragment-blank-s"></div>
                        <div id="cpw-fragment-save" onclick="uCPWdisplay();"></div>
                    </div>
                </form>
                <div id="cpw-foot">
                </div>
            </div>
        </div>
        <!--登录表单-->
        <div id="logincontent" class="abs" style="display: none;">
            <div id="login-innner">
                <div id="login-head">
                    <div class="login-cancel" onclick="uLogindisplay();"></div>
                </div>
                <form id="form_login" name="form-login1" method = "post" action = "javascript:;">
                    <div>
                        <div class="login-fragment-email">
                            <input class="login-input text-click" type="text" name = "mail" />
                        </div>
                        <div class="login-fragment-passwd">
                            <input class="login-input text-click" type="password" name = "password" />
                        </div>
                        <div class="login-fragment-checkcode" id = "authCode_part_login">
                            <input class="login-input-checkcode text-click" type="text" name = "authCode" />
                            <img class="login-checkcode-pic" id = "authcode_image_login" src = "./imgauthcode/show" onclick = "auth_refresh_login()"/>
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-blank-m autologin" >
                            <input type="checkbox" id="input_autologin" name = "auto_login"/>
                            下次自动登录
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-loginbtn"><input id = "login_submit" type = "submit" value = "" style = "position:relative;left:75px;top:4px;width:480px;height:60px;Opacity:0;background-color:white;" /></div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-tosignup" id="login_to_signup_btn" ></div>
                    </div>
                </form>
                <div id="login-foot">
                </div>
            </div>
        </div>
        <!--注册表单-->
        <div id="signupcontent" class="abs" style="display: none;">
            <div id="signup-innner">
                <div id="signup-head">
                    <div class="login-cancel" onclick="uRegdisplay();"></div>
                </div>
                <form id="form_signup" name="form-signup1" method="post" action = "javascript:;">
                    <div>
                        <div class="signup-fragment-email">
                            <input class="login-input text-click" type="text" name = "mail" />
                        </div>
                        <div class="signup-fragment-idnum">
                            <input class="login-input text-click" type="text" name = "studentId" />
                        </div>
                        <div class="signup-fragment-name">
                            <input class="login-input text-click" type="text" name = "name" />
                        </div>
                        <div class="signup-fragment-passwd">
                            <input class="login-input text-click" type="password" name = "password" />
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-checkcode" id = "authCode_part_register">
                        	<input class="login-input-checkcode text-click" type="text" name = "authCode" />
                            <img class="signup-checkcode-pic" src="./imgauthcode/show" id = "authcode_image_signup" onclick = "auth_refresh_signup()"/>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-agreement" onclick="open_agreement();">
                        	<div id="signup-agree" class="signup-fragment-needagree" onclick="agreement_toggle();"></div>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-signupbtn"><input id = "register_submit" type = "submit" value = "" style = "position:relative;left:75px;top:4px;width:480px;height:60px;Opacity:0;background-color:white;" /></div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-tologin" id="signup_to_login_btn"></div>
                    </div>
                </form>
                <div id="signup-foot">
                </div>
            </div>
        </div>
    </body>
    <script src="/js/index.js"></script>
    <script src="/js/raphael-min.js"></script>
    
</html>
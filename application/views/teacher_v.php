<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <!--script type="text/javascript" src="/js/jquery-1.2.6.js"></script-->

        <script src="/js/raphael-min.js"></script>
        <script src="/js/slider.js"></script>
        <script src="/js/polar.js"></script>
        <script type="text/javascript" src="/js/comment.js"></script>  
        <script type="text/javascript" src="/js/albumshow.js"></script>  
        <script src="/js/ajaxfileupload.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/index.css">

		<!--PHP变量赋值给js-->
        <script type="text/javascript">
			<?php
				if (isset($user_name))
				{
					echo "var user_name = '" . $user_name . "';\n";
				}
				if (isset($user_id))
				{
					echo "var user_id = '" . $user_id ."';\n";
				}
				if (isset($teacher_info))
				{
					echo "var teacher_primary_key = '" . $teacher_info->primary_key ."';\n";
				}
			?>
		</script>
		
		<!--页面初始化-->
		<script type="text/javascript">
			$(document).ready(function(){
				//加载姓名部分
				$('#denglu').html(user_name);
				$('#zhuce').html("退出");
			});
			
			//载入用户信息及退出函数
			function uInfoShow()
			{
				location.href = '/user/' + user_id;
			}
			function uLogout()
			{
				$.get('/login/logout/'+ Math.random(),function(result){
					location.href = '/';
				});
			}
		</script>	
		
		<!--注册登录按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#login_link').click(function(){
					uInfoShow();
				});
				$('#signup_link').click(function(){
					uLogout();
				});
			});
		</script>
		
		<title><?php echo $teacher_info->school . '_' .$teacher_info->teacher_name ?></title>
	</head>
	
    <body class="back-light text-font">
		<input type="hidden" id="last_pk" value="0"/>
        <div>
            <div class="container padding-t-10">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <div align="center"><img src="/img/logo-shadow.png"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row size-guidebar guidebar-backg">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="padding:0;">
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r">
                    <a id="guidebar-1" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-1" class="guide-change guidebar-tab-content" >最学术</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-2" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-2" class="guide-change guidebar-tab-content" 
                    style="color:white; background-color:#69b7d4;-webkit-box-shadow: 1px 3px 3px;-moz-box-shadow: 1px 3px 3px; box-shadow: 0px 3px 3px -1px #2a6a81;font-size:17px;">最课程</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-3" href="#zuihuodong" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-3" class="guide-change guidebar-tab-content">最活动</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-4" href="#zuishetuan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-4" class="guide-change guidebar-tab-content">最社团</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-5" href="#zuishenghuo" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-5" class="guide-change guidebar-tab-content">最生活</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-6" href="#zuikeyan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-6" class="guide-change guidebar-tab-content">最科研</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab" style="margin-left:-4px;" >
                    <a id="guidebar-7" href="#zuiqiuzhi" class="guidebar-word" onclick="javascript:guidebar_choose(this);"> <div id="guidebar-content-7" class="guide-change guidebar-tab-content">最求职</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab pull-right" style="margin-left:-5px;" >
                    <a href="#about_us" class="guidebar-word"><div  id="guanyuwomen" class="size-guidebart guidebar-tab-content ">关于我们</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a id="signup_link" href="javascript:;" class="guidebar-word" ><div id="zhuce" class="size-guidebart guidebar-tab-2-content">退出</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a  id="login_link"  href="javascript:;" class="guidebar-word"><div id="denglu" class="size-guidebart guidebar-tab-2-content"></div></a>
                </div>
                
                <div class="search-bar pull-right" style="margin-top:8px;">
                        <input id="Text1" class="pull-left search-input text-click" placeholder="search for something :)">
                        <div class="pull-right size-search-button"></div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        
        <div>
            <img src="<?php echo $teacher_info->photo; ?>.jpg" />
        </div>
        
        
        
    </body>
		
    <script src="/js/index.js"></script>

</html>
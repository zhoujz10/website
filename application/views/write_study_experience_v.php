﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/raphael-min.js"></script>
 
        <script type="text/javascript" src="/js/albumshow.js"></script>  
		<link rel="stylesheet" type="text/css" href="/css/index.css">
		
		<!--PHP变量赋值给js-->
		<script type = "text/javascript">
			<?php
				if (isset($user_name))
				{
					echo "var user_name = '" . $user_name . "';\n";
				}
				if (isset($user_id))
				{
					echo "var user_id = '" . $user_id ."';\n";
				}
				if (isset($primary_key))
				{
					echo "var course_primary_key = '" . $primary_key ."';\n";
				}
				if (isset($course_name))
				{
					echo "var course_name = '" . $course_name ."';\n";
				}
				if (isset($tag))
				{
					echo "var tag = '" . $tag ."';\n";
				}
			?>
		</script>
		
		<!--页面初始化-->
		<script type="text/javascript">
			$(document).ready(function(){
				//加载姓名部分
				$('#denglu').html(user_name);
				$('#zhuce').html("退出");				
			});
			//载入用户信息及退出函数
			function uInfoShow()
			{
				location.href = '/user/' + user_id;
			}
			function uLogout()
			{
				$.get('/login/logout/'+ Math.random(),function(result){
					location.href = '/';
				});
			}
		</script>
		<!--注册登录按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#login_link').click(function(){
					uInfoShow();
				});
				$('#signup_link').click(function(){
					uLogout();
				});
			});
		</script>
		
        <script type="text/javascript">  
            $(document).ready(function(){   
                /*$.fn.jSlider({  
                    renderTo: '#slidercontainer-hjl',  
                    size: {  
                        barWidth: 220,  
                        sliderWidth: 15  
                    },  
                    onChanging: function(percentage, e){  
                        hjl.innerText=Math.floor(percentage*10);
                        //在控制台输出信息  
                        window.console && console.log('percentage: %s', percentage);  
                    }  
                });  
                $.fn.jSlider({  
                    renderTo: '#slidercontainer-qsd',  
                    size: {  
                        barWidth: 220,  
                        sliderWidth: 15  
                    },  
                    onChanging: function(percentage, e){  
                        qsd.innerText=Math.floor(percentage*10);
                        //在控制台输出信息  
                        window.console && console.log('percentage: %s', percentage);  
                    }  
                }); 
                $.fn.jSlider({  
                    renderTo: '#slidercontainer-chjb',  
                    size: {  
                        barWidth: 220,  
                        sliderWidth: 15  
                    },  
                    onChanging: function(percentage, e){  
                        chjb.innerText=Math.floor(percentage*10);
                        //在控制台输出信息  
                        window.console && console.log('percentage: %s', percentage);  
                    }  
                }); */
            });  
        </script>
		
		<!--发布日志按钮-->
		<script type="text/javascript"> 
			$(document).ready(function(){
				$('#submit_study_experience').click(function() {
					var title = $('#ifm1')[0].contentWindow.$("#article-title").val();
					var content = $('#ifm1')[0].contentWindow.$("#editor").html();
					$.post('/study_experience/submit_study_experience/', {course_primary_key: course_primary_key, title: title, content: content} ,function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								location.href = result.url;
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			});
			
		</script>
		
        <!--写日志页面专用-->
        <style>
            .title{
                margin:15px 0px 10px 10px;
                font-size:16px;
                color:#555555;
            }
        </style>
    </head>
    
    <body class="back-light text-font">
        <div>
            <div class="container padding-t-10">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <div align="center"><img src="/img/logo-shadow.png"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row size-guidebar guidebar-backg">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="padding:0;">
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r">
                    <a id="guidebar-1" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-1" class="guide-change guidebar-tab-content guidebar-tab-rad" 
                    style="color:white; background-color:#69b7d4;-webkit-box-shadow: 1px 3px 3px;-moz-box-shadow: 1px 3px 3px; box-shadow: 0px 3px 3px -1px #2a6a81;font-size:17px;">最学术</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-2" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-2" class="guide-change guidebar-tab-content">最课程</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-3" href="#zuihuodong" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-3" class="guide-change guidebar-tab-content">最活动</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-4" href="#zuishetuan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-4" class="guide-change guidebar-tab-content">最社团</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-5" href="#zuishenghuo" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-5" class="guide-change guidebar-tab-content">最生活</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-6" href="#zuikeyan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-6" class="guide-change guidebar-tab-content">最科研</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab" style="margin-left:-4px;" >
                    <a id="guidebar-7" href="#zuiqiuzhi" class="guidebar-word" onclick="javascript:guidebar_choose(this);"> <div id="guidebar-content-7" class="guide-change guidebar-tab-content">最求职</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab pull-right" style="margin-left:-5px;" >
                    <a href="#about_us" class="guidebar-word"><div  id="guanyuwomen" class="size-guidebart guidebar-tab-content ">关于我们</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a id="signup_link" href="javascript:;" class="guidebar-word"><div id="zhuce" class="size-guidebart guidebar-tab-2-content"></div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a  id="login_link"  href="javascript:;" class="guidebar-word"><div id="denglu" class="size-guidebart guidebar-tab-2-content"></div></a>
                </div>
                
                <div class="search-bar pull-right" style="margin-top:8px;">
                        <input id="Text1" class="pull-left search-input text-click" placeholder="search for something :)">
                        <div class="pull-right size-search-button"></div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

        <div class="container">
            <div class="title">写学习经验</div>
            <div id="loading-text">正在加载编辑器，请稍候...</div>
            <iframe id="ifm1" style="width:100%;height:700px;border:none;" src="/editor/myeditor.php" onload="javascript:$('#loading-text').slideUp();"></iframe>
            <a class="btn" id="submit_study_experience">发布</a>
        </div>
        
        <!--登录表单-->
        <div id="logincontent" class="abs" style="display: none;">
            <div id="login-innner">
                <div id="login-head">
                    <div class="login-cancel" onclick="uLogindisplay();"></div>
                </div>
                <form id="form1" name="form-login1">
                    <div>
                        <div class="login-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="login-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="login-fragment-checkcode">
                            <input class="login-input-checkcode text-click" type="text"/>
                            <img class="login-checkcode-pic"/>
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-blank-m autologin" >
                            <input type="checkbox" id="input_autologin"/>
                            下次自动登录
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-loginbtn" onclick="uLogindisplay();"></div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-tosignup" onclick="uLogindisplay();uRegShow();"></div>
                    </div>
                </form>
                <div id="login-foot">
                </div>
            </div>
        </div>
        <!--注册表单-->
        <div id="signupcontent" class="abs" style="display: none;">
            <div id="signup-innner">
                <div id="signup-head">
                    <div class="login-cancel" onclick="uRegdisplay();"></div>
                </div>
                <form id="form_signup" name="form-signup1">
                    <div>
                        <div class="signup-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-idnum">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-name">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-checkcode">
                        	<input class="login-input-checkcode text-click" type="text"/>
                            <img class="signup-checkcode-pic"/>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-agreement" onclick="open_agreement();">
                        	<div id="signup-agree" class="signup-fragment-needagree" onclick="agreement_toggle();"></div>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-signupbtn" onclick="uRegdisplay();"></div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-tologin" onclick="uRegdisplay();uLoginShow();"></div>
                    </div>
                </form>
                <div id="signup-foot">
                </div>
            </div>
        </div>     
    	
        
    </body>
    <script src="/js/index.js"></script>
</html>
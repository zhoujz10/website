<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/raphael-min.js"></script>

        <link rel="stylesheet" type="text/css" href="/css/zuiguanzhu.css">
        <link rel="stylesheet" type="text/css" href="/css/index.css">
        <!--复选框专用-->
        <style>
            label { 
				display: block; 
				cursor: pointer; 
				line-height: 19px; 
				margin-bottom: 10px; 
				text-shadow: 0 -1px 0 rgba(0,0,0,.2); 
			}
			
			.label_check input{ 
				margin-right: 5px; 
			}
			
			.has-js .label_check{ 
				padding-left: 34px; 
			}
						
			.has-js .label_check{ 
				background: url("img/checkbox-bg.png") no-repeat; 
			}
			
			.has-js .label_check { 
				background-position: 0 0px;
			}
			.has-js label.c_on { 
				background-position: 0 -35px;
			}
			
			.has-js .label_check input{ 
				position: absolute; 
				left: -9999px; 
			} 
        </style>
		<link rel="shortcut icon" href="/img/icon.gif" >
		<title>最课程</title>
		
		<!--PHP变量赋值给js-->
        <script type = "text/javascript">
			<?php
				if (isset($user_name))
				{
					echo "var user_name = '" . $user_name . "';\n";
				}
				if (isset($user_id))
				{
					echo "var user_id = '" . $user_id ."';\n";
				}
			?>
		</script>
		
		<!--页面初始化-->
		<script type="text/javascript">
			$(document).ready(function(){
				//加载姓名部分
				$('#denglu').html(user_name);
				$('#zhuce').html("退出");
				$.fn.init_my_course_schedule();
				$.fn.init_my_care();
				$.fn.query_course();
				//全选框
				$('#checkbox-10').click(function(){
					obj = document.getElementsByName('sample-checkbox-1');
					for(var i=0; i<obj.length; i++){
						obj[i].checked = $('#checkbox-10').is(':checked');
					}
				});
				//课程点击
				$(document).on('click', 'div[name="course_name"]', function(){
					var obj_id = $(this).attr('id');
					//location.href = '/course/id/' + $('#course_id-' + obj_id).val() + '/' + $('#sub_id-' + obj_id).val();
					window.open('/course/id/' + $('#course_id-' + obj_id).val() + '/' + $('#sub_id-' + obj_id).val(),"_blank");
				});
			});
			//载入用户信息及退出函数
			function uInfoShow()
			{
				location.href = '/user/' + user_id;
			}
			function uLogout()
			{
				$.get('/login/logout/'+ Math.random(),function(result){
					location.href = '/';
				});
			}
		</script>
		<!--注册登录按钮-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#login_link').click(function(){
					uInfoShow();
				});
				$('#signup_link').click(function(){
					uLogout();
				});
			});
		</script>
		
		<!--搜索部分-->
        <script type="text/javascript">
            $(document).ready(function () {
				$('#search_result').click(function(){
					$('#query_page').val('1');
					$.fn.query_course();
				});
			});
		</script>

		<!--我的课表初始化-->
		<script type = "text/javascript">
			$.fn.init_my_course_schedule = function(callback)
			{
				$.get('/course/course_schedule/' + Math.random(), function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
						    var scheduletmp = '<p>我的课程表</p>\n';
						    var len = result.course_number;
						    var i = 0;
						    if(len > 0)
						    {
						        for(i=0; i<len; i++)
						        {
						            scheduletmp += '<span>' + result.course_schedule[i].course_name + '</span>\n';
						            scheduletmp += '<span>' + result.course_schedule[i].teacher_name + '</span>\n';
						            scheduletmp += '<span>周' + result.course_schedule[i].day + '第' + result.course_schedule[i].section + '节</span>\n';
						        }
						        $('#course_schedule').html(scheduletmp);
						    }
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
					if (callback)
					{
						callback();
					}
				});
				
			}
		</script>
		
		<!--我的关注初始化-->
		<script type = "text/javascript">
			$.fn.init_my_care = function(callback)
			{
				$.get('/user/get_my_course/' + Math.random(), function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							var i = 0;
							var len = result.course_data.length;
							var tabletemp = '';
							for (i = 0; i < len; i++)
							{
								if (i%2 == 0)
									tabletemp +='<div class="row cxresult-content-gray">\n';
								else
									tabletemp +='<div class="row cxresult-content-white">\n';
								tabletemp +=    '	<div class="col-md-2  padding-0">\n';
								tabletemp +=    '		<div style="margin-left:20px;"class="padding-0">\n';
								tabletemp +=    '			<div class="cxresult-frame" style="padding:0;">\n';
								tabletemp +=    '				<label for="checkbox-1' + (i+1) + '" class="label_check wgzhd" style= "display:none">\n';
								tabletemp +=    '					<input type="checkbox" value="1" id="checkbox-1' + (i+1) + '" name="sample-checkbox-1" /><p>&nbsp;</p>\n';
								tabletemp +=    '                	<input type="hidden"  value="' + result.course_data[i].primary_key + '" id="pk-1' + (i+1) + '"\n';
								tabletemp +=    '				</label>\n';
								tabletemp +=    '			</div>\n';
								tabletemp +=    '			<div style="margin-left:20px;"class="display-in-bl">' + result.course_data[i].department + '</div>\n';
								tabletemp +=    '		</div>\n';
								tabletemp +=    '	</div>\n';
								tabletemp +=    '	<div  class="col-md-1  padding-0">0020041</div>\n';
								tabletemp +=    '	<div class="col-md-1  padding-0">90</div>\n';
								tabletemp +=    '	<div style="padding:0 10px 0 0;"  class="col-md-2 blue-text">' + result.course_data[i].course_name + '</div>\n';
								tabletemp +=    '	<div class="col-md-1  padding-0">' + result.course_data[i].credit + '</div>\n';
								tabletemp +=    '	<div class="col-md-1  padding-0">' + result.course_data[i].teacher_name + '</div>\n';
								tabletemp +=    '	<div class="col-md-2  padding-0">' + result.course_data[i].course_time + '</div>\n';
								tabletemp +=    '	<div class="col-md-2  padding-0">' + result.course_data[i].course_feature + '</div>\n';
								tabletemp +=    '</div>\n';
							}
							//$(tabletemp).insertAfter('#woguanzhude-head');
							$('#woguanzhude-body').html(tabletemp);
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
					if (callback)
					{
						callback();
					}
				});
				
			}
		</script>
		<!--查询课程初始化-->
		<script type = "text/javascript">
			$.fn.query_course = function(callback)
			{
				var data = {};
				var k = 2;
				data['page'] = $('#query_page').val();
				data['keyword'] = $('#chaxun-inp').val();
				data['max_record_number'] = $('#query_max_record_number').val();	
				data['de' + $('#query_sel_yuanxi').val()] = 'true';
				data['te' + $('#query_sel_feature').val()] = 'true';
				data['ye' + $('#query_sel_year').val()] = 'true';

				$.get('/course/query', data, function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							var querytemp = '';
							var i = 0;
							var len = result.query_course_list.length;
							for (i = 0; i < len; i++)
							{
								if (i%2 == 0)
									querytemp += '<div class="row cxresult-content-gray">\n';
								else
									querytemp += '<div class="row cxresult-content-white">\n';
								querytemp += '		<div class="col-md-2  padding-0">\n';
                                querytemp += '			<div style="margin-left:20px;"class="padding-0">\n';
                                querytemp += '    				<div class="cxresult-frame" style="padding:0;">\n';
                                querytemp += '        				<label for="checkbox-0' + (i+1) + '" class="label_check" style="display:none">\n';
                                querytemp += '            				<input type="checkbox"  value="1" id="checkbox-0' + (i+1) + '" name="sample-checkbox-0' + (i+1) + '" /><p>&nbsp;</p>\n';
			                    querytemp += '        				</label>\n';
                                querytemp += '   				</div>\n';
                                querytemp += '    				<div style="margin-left:20px;"class="display-in-bl">' + result.query_course_list[i].department + '</div>\n';
                                querytemp += '			</div>\n';
								querytemp += '		</div>\n';
								querytemp += '		<div  class="col-md-1  padding-0">' + result.query_course_list[i].course_id + '</div>\n';
								querytemp += '		<div class="col-md-1  padding-0">90</div>\n';
								querytemp += '		<div style="padding:0 10px 0 0;cursor:pointer;" class="col-md-2 blue-text" name="course_name" id="' + result.query_course_list[i].primary_key + '">' + result.query_course_list[i].course_name + '</div>\n';
								querytemp += '		<input type="hidden" id="course_id-' + result.query_course_list[i].primary_key + '" value="' + result.query_course_list[i].course_id + '">\n';
								querytemp += '		<input type="hidden" id="sub_id-' + result.query_course_list[i].primary_key + '" value="' + result.query_course_list[i].sub_id + '">\n';
								querytemp += '		<div class="col-md-1  padding-0">' + result.query_course_list[i].credit + '</div>\n';
								querytemp += '		<div class="col-md-1  padding-0">' + result.query_course_list[i].teacher_name + '</div>\n';
								querytemp += '		<div class="col-md-2  padding-0">' + result.query_course_list[i].course_time + '</div>\n';
								querytemp += '		<div class="col-md-2  padding-0">' + result.query_course_list[i].course_feature + '</div>\n';
								querytemp += '</div>\n';
                        
							}
							$('#query_course_body').html(querytemp);
							
							var page_turning_temp = '';
							var page = Math.ceil(result.query_num/data['max_record_number']);
							var j;
							
							page_turning_temp += '<a href="#page" class="text-white-link"><div id="prevpage" class="margin-11 back-blue divpage-number pull-left">&lt;</div></a>\n';
							
							if (page > 8)
							{
								if (data['page'] <= 4)
								{
									for(j = 1;j <= 4;j++)
										page_turning_temp += '<a href="#page" class="text-white-link"><div id="page-' + j + '" class="margin-11 margin-lr-3 back-grey divpage-number pull-left">' + j + '</div></a>\n';
									page_turning_temp += '<a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">...</div></a>\n';
									for(j = page-1;j <= page;j++)
										page_turning_temp += '<a href="#page" class="text-white-link"><div id="page-' + j + '" class="margin-11 margin-lr-3 back-grey divpage-number pull-left">' + j + '</div></a>\n';
								}
								else if (data['page'] > 4 && (page - data['page']) > 4)
								{
									for(j = 1;j <= 2;j++)
										page_turning_temp += '<a href="#page" class="text-white-link"><div id="page-' + j + '" class="margin-11 margin-lr-3 back-grey divpage-number pull-left">' + j + '</div></a>\n';
									page_turning_temp += '<a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">...</div></a>\n';
									for(j = data['page']-1;j <= parseInt(data['page'])+1;j++)
										page_turning_temp += '<a href="#page" class="text-white-link"><div id="page-' + j + '" class="margin-11 margin-lr-3 back-grey divpage-number pull-left">' + j + '</div></a>\n';
									page_turning_temp += '<a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">...</div></a>\n';
									for(j = page-2;j <= page;j++)
										page_turning_temp += '<a href="#page" class="text-white-link"><div id="page-' + j + '" class="margin-11 margin-lr-3 back-grey divpage-number pull-left">' + j + '</div></a>\n';
								}
								else
								{
									for(j = 1;j <= 2;j++)
										page_turning_temp += '<a href="#page" class="text-white-link"><div id="page-' + j + '" class="margin-11 margin-lr-3 back-grey divpage-number pull-left">' + j + '</div></a>\n';
									page_turning_temp += '<a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">...</div></a>\n';
									for(j = page-4;j <= page;j++)
										page_turning_temp += '<a href="#page" class="text-white-link"><div id="page-' + j + '" class="margin-11 margin-lr-3 back-grey divpage-number pull-left">' + j + '</div></a>\n';
								}
							}
							else
							{
								for(j = 1;j <= page;j++)
									page_turning_temp += '<a href="#page" class="text-white-link"><div id="page-' + j + '" class="margin-11 margin-lr-3 back-grey divpage-number pull-left">' + j + '</div></a>\n';
							}
							page_turning_temp += '<a href="#page" class="text-white-link"><div id="nextpage" class="margin-11 back-blue divpage-number pull-left">&gt;</div></a>\n';
							page_turning_temp += '<div class="divpage-inputdiv pull-left "><input id="gopage_input" class="divpage-input" /></div>\n';
							page_turning_temp += '<a href="#page" class="text-white-link"><div id="gopage" class="margin-11 back-blue divpage-number pull-left">Go</div></a-->\n';
							$("#page_turning").html(page_turning_temp);
							$("#page-" + data['page']).attr("class","margin-11 back-blue divpage-number pull-left");
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
					if (callback)
					{
						callback();
					}
				});	
			}
		</script>
		
		<script type = "text/javascript">
			$(document).ready(function(){
				$(document).on("click", "div[id^='page-']", function(){
					var str = $(this).attr("id").replace(/[^0-9]/ig, "");
					$('#query_page').val(str);
					$.fn.query_course();
				});
				
				$(document).on("click", "div[id='prevpage']", function(){
					$('#query_page').val(parseInt($('#query_page').val())-1);
					$.fn.query_course();
				});
				
				$(document).on("click", "div[id='nextpage']", function(){
					$('#query_page').val(parseInt($('#query_page').val())+1);
					$.fn.query_course();
				});
				
				$(document).on("click", "div[id='gopage']", function(){
					if($("#gopage_input").val().length == 0)
						alert("页码不能为空！");
					else
					{
						$('#query_page').val(parseInt($("#gopage_input").val()));
						$.fn.query_course();
					}
				});
			});
		</script>
		
		<!--取消课程关注-->
		<script type = "text/javascript">
			$(document).ready(function(){
				$('#zgzh-wgzhd-delete').click(function(){
					var i = 0;
					var j = 0;
					var data = {};
					while (true)
					{
						if ($('#checkbox-1' + (i+1)).length == 0)
							break;
						if ($('#checkbox-1' + (i+1)).is(':checked'))
						{
							data['cancel' + j] = $('#pk-1' + (i+1)).val();
							j = j + 1;
						}
						i = i + 1;
					}
					$.post("/user/cancel_course_care", data, function(result){
						result = eval("("+result+")");
						if(result.status != null)
						{
							if(result.status == true)
							{
                                $("#bb").slideUp($.fn.init_my_care());
                                $(".wgzhd").attr("style", "display:none");
							}
							else
							{
								alert(result.failDesc);
								location.href = result.url;
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			});
		</script>
    </head>
 

    <body class="back-light text-font">
        <div>
            <div class="container padding-t-10">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <div align="center"><img src="/img/logo-shadow.png"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row size-guidebar guidebar-backg">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="padding:0;">
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r">
                    <a id="guidebar-1" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-1" class="guide-change guidebar-tab-content" >最学术</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-2" href="javascript:;" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-2" class="guide-change guidebar-tab-content" 
                    style="color:white; background-color:#69b7d4;-webkit-box-shadow: 1px 3px 3px;-moz-box-shadow: 1px 3px 3px; box-shadow: 0px 3px 3px -1px #2a6a81;font-size:17px;">最课程</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-3" href="#zuihuodong" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-3" class="guide-change guidebar-tab-content">最活动</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-4" href="#zuishetuan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-4" class="guide-change guidebar-tab-content">最社团</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-5" href="#zuishenghuo" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-5" class="guide-change guidebar-tab-content">最生活</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-6" href="#zuikeyan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-6" class="guide-change guidebar-tab-content">最科研</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab" style="margin-left:-4px;" >
                    <a id="guidebar-7" href="#zuiqiuzhi" class="guidebar-word" onclick="javascript:guidebar_choose(this);"> <div id="guidebar-content-7" class="guide-change guidebar-tab-content">最求职</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab pull-right" style="margin-left:-5px;" >
                    <a href="#about_us" class="guidebar-word"><div  id="guanyuwomen" class="size-guidebart guidebar-tab-content ">关于我们</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a id="signup_link" href="javascript:;" class="guidebar-word"><div id="zhuce" class="size-guidebart guidebar-tab-2-content">退出</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a  id="login_link"  href="javascript:;" class="guidebar-word"><div id="denglu" class="size-guidebart guidebar-tab-2-content"></div></a>
                </div>
                
                <div class="search-bar pull-right" style="margin-top:8px;">
                        <input id="Text1" class="pull-left search-input text-click" placeholder="search for something :)">
                        <div class="pull-right size-search-button"></div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        
        <!--以下为主题内容-->
        <div class="container">            
            <div id="main">
                
                <!--图片展示区>
                <div class="margin-0 row size-h-300-only margin-t-20">
                    <div class="col-sm-5 size-h-300-only padding-0">
                        <div class="size-h-300">
                            <div class="margin-3">
                                <img class="photo-crop-big" src="http://www.isl.ac.cn/kxcb/kpwz/201012/W020101202386661300231.jpg"/>
                                <div class="photo-scribe-big-b size-h-80 back-blue">
                                    对图片的描述信息
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 size-h-300-only padding-0">
                        <div class=" size-h-100">
                            <div class="margin-3">
                                <img class="photo-crop-xsmall" src="http://www.isl.ac.cn/kxcb/kpwz/201012/W020101202386661300231.jpg"/>
                                <div class="photo-scribe-xsmall-b back-shalow-orienge">
                                    对图片的描述信息
                                </div>
                            </div>
                        </div>
                        <div class=" size-h-200">
                            <div class="margin-3">
                                <img class="photo-crop-middle" src="http://www.isl.ac.cn/kxcb/kpwz/201012/W020101202386661300231.jpg"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 size-h-300 padding-0">
                        <div class=" size-h-150">
                            <div class="margin-3">
                                <img class="photo-crop-small" src="http://www.isl.ac.cn/kxcb/kpwz/201012/W020101202386661300231.jpg"/>
                            </div>
                        </div>
                        <div class=" size-h-150">
                            <div class="margin-3">
                                <img class="photo-crop-small" src="http://www.isl.ac.cn/kxcb/kpwz/201012/W020101202386661300231.jpg"/>
                                <div class="photo-scribe-small-b back-shalow-blue">
                                    对图片的描述信息
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 size-h-300 padding-0">
                        <div class=" size-h-300">
                            <div class="margin-3">
                                <img class="photo-crop-big" src="http://www.isl.ac.cn/kxcb/kpwz/201012/W020101202386661300231.jpg"/>
                                <div class="photo-scribe-big-t size-h-80 back-shalow-green">
                                    图片描述
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div-->
                
                <div id="course_schedule">
                </div>
                 
                <!--查询菜单-->
                <div id="searach" class="margin-t-20 chaxun">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-2 gray-text">
                                    开课院系
                                </div>
                                <div class="col-md-10  chaxun-border padding-0">
                                
                                   <div  >
                                        <div id="yuanxi-all" class="chaxun-xuanxiang-yuanxi chaxun-choose">全部</div> <div id="yuanxi-jianshuxueyuan" class="chaxun-xuanxiang-yuanxi">建筑学院</div> <div id="yuanxi-chenggui" class="chaxun-xuanxiang-yuanxi">城规系</div>
                                        <div id="yuanxi-jianzhuxi" class="chaxun-xuanxiang-yuanxi">建筑系</div> <div id="yuanxi-tumu" class="chaxun-xuanxiang-yuanxi">土木系</div> <div id="yuanxi-shuili" class="chaxun-xuanxiang-yuanxi">水利系</div>
                                        <div id="yuanxi-huanjing" class="chaxun-xuanxiang-yuanxi">环境学院</div> <div id="yuanxi-jixie" class="chaxun-xuanxiang-yuanxi">机械系</div> <div id="yuanxi-jingyi" class="chaxun-xuanxiang-yuanxi">精仪系</div>
                                        <div id="yuanxi-reneng" class="chaxun-xuanxiang-yuanxi">热能系</div> <div id="yuanxi-qch" class="chaxun-xuanxiang-yuanxi">汽车系</div> <div id="yuanxi-shuxue" class="chaxun-xuanxiang-yuanxi">数学系</div>
                                        <div id="yuanxi-xinxixueyuan" class="chaxun-xuanxiang-yuanxi">信息学院</div> <div id="yuanxi-gongyegongcheng" class="chaxun-xuanxiang-yuanxi">工业工程系</div><div id="yuanxi-dj" class="chaxun-xuanxiang-yuanxi">电机系</div>
                                         <div id="yuanxi-dz" class="chaxun-xuanxiang-yuanxi">电子系</div> <div id="yuanxi-jsjx" class="chaxun-xuanxiang-yuanxi">计算机系</div>
                                        <div id="aa" style="display:none;">
                                            <div id="yuanxi-zdh" class="chaxun-xuanxiang-yuanxi">自动化系</div> <div id="yuanxi-wndz" class="chaxun-xuanxiang-yuanxi">微纳电子系</div> <div id="yuanxi-hy" class="chaxun-xuanxiang-yuanxi">航院</div>
                                            <div id="yuanxi-gw" class="chaxun-xuanxiang-yuanxi">工物系</div> <div id="yuanxi-hg" class="chaxun-xuanxiang-yuanxi">化工系</div> <div id="yuanxi-clxy" class="chaxun-xuanxiang-yuanxi">材料学院</div>
                                            <div id="yuanxi-shx" class="chaxun-xuanxiang-yuanxi">数学系</div> <div id="yuanxi-wl" class="chaxun-xuanxiang-yuanxi">物理系</div> <div id="yuanxi-hxx" class="chaxun-xuanxiang-yuanxi">化学系</div>
                                            <div id="yuanxi-shmxy" class="chaxun-xuanxiang-yuanxi">生命学院</div> <div id="yuanxi-dqkx" class="chaxun-xuanxiang-yuanxi">地球科学中心</div><div id="yuanxi-jcxxxy" class="chaxun-xuanxiang-yuanxi">交叉信息学院</div>
                                            <div id="yuanxi-zhpyy" class="chaxun-xuanxiang-yuanxi">周培源应</div> <div id="yuanxi-jgxy" class="chaxun-xuanxiang-yuanxi">经管学院</div>
                                            <div id="yuanxi-gggl" class="chaxun-xuanxiang-yuanxi">公共管理</div> <div id="yuanxi-jrxy" class="chaxun-xuanxiang-yuanxi">金融学院</div> <div id="yuanxi-rwxy" class="chaxun-xuanxiang-yuanxi">人文学院</div>
                                            <div id="yuanxi-shkxy" class="chaxun-xuanxiang-yuanxi">社科学院</div> <div id="yuanxi-zwx" class="chaxun-xuanxiang-yuanxi">中文系</div> <div id="yuanxi-wwx" class="chaxun-xuanxiang-yuanxi">外文系</div>
                                            <div id="yuanxi-fxy" class="chaxun-xuanxiang-yuanxi">法学院</div> <div id="yuanxi-xwxy" class="chaxun-xuanxiang-yuanxi">新闻学院</div> <div id="yuanxi-mks" class="chaxun-xuanxiang-yuanxi">马克思主义学院</div>
                                            <div id="yuanxi-tyb" class="chaxun-xuanxiang-yuanxi">体育部</div> <div id="yuanxi-djzx" class="chaxun-xuanxiang-yuanxi">电教中心</div><div id="yuanxi-tshg" class="chaxun-xuanxiang-yuanxi">图书馆</div>
                                            <div id="yuanxi-yjzhx" class="chaxun-xuanxiang-yuanxi">艺教中心</div> <div id="yuanxi-my" class="chaxun-xuanxiang-yuanxi">美术学院</div> <div id="yuanxi-tsxy" class="chaxun-xuanxiang-yuanxi">土水学院</div>
                                            <div id="yuanxi-jgx" class="chaxun-xuanxiang-yuanxi">建管系</div> <div id="yuanxi-jzhjs" class="chaxun-xuanxiang-yuanxi">建筑技术</div> <div id="yuanxi-hyy" class="chaxun-xuanxiang-yuanxi">核研院</div>
                                            <div id="yuanxi-jyy" class="chaxun-xuanxiang-yuanxi">教研院</div> <div id="yuanxi-wlzhx" class="chaxun-xuanxiang-yuanxi">网络中心</div><div id="yuanxi-xlzhx" class="chaxun-xuanxiang-yuanxi">训练中心</div>
                                            <div id="yuanxi-dgdz" class="chaxun-xuanxiang-yuanxi">电工电子中心</div> <div id="yuanxi-xchb" class="chaxun-xuanxiang-yuanxi">宣传部</div> <div id="yuanxi-xshb" class="chaxun-xuanxiang-yuanxi">学生部</div>
                                            <div id="yuanxi-wzhb" class="chaxun-xuanxiang-yuanxi">武装部</div> <div id="yuanxi-yjshy" class="chaxun-xuanxiang-yuanxi">研究生院</div> <div id="yuanxi-shyshy" class="chaxun-xuanxiang-yuanxi">深研生院</div>
                                            <div id="yuanxi-xyy" class="chaxun-xuanxiang-yuanxi">校医院</div> <div id="yuanxi-yxy" class="chaxun-xuanxiang-yuanxi">医学院</div> <div id="yuanxi-shyx" class="chaxun-xuanxiang-yuanxi">生医系</div>
                                            <div id="yuanxi-rjxy" class="chaxun-xuanxiang-yuanxi">软件学院</div> 
                                        </div>
                                   </div>
                                   <div  class=" pull-right">
                                   <form name="chaxun_form" method="post" action="">
                                       <input name="ok" onclick="javascript:slideShowReply('aa')" type="button" class="chaxun-more text-click" id="ok" value="更多">
                                   </form>
                                   </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 gray-text">
                                    课程特色
                                </div>
                                <div class="col-md-10  chaxun-border padding-0">
                                    <div id="tese-all" class="chaxun-xuanxiang-tese chaxun-choose">全部</div> <div id="tese-wsh" class="chaxun-xuanxiang-tese">文化素质核心课</div> <div id="tese-ty" class="chaxun-xuanxiang-tese">体育课</div>
                                    <div id="tese-xs" class="chaxun-xuanxiang-tese">新生研讨课</div> <div id="tese-shuy" class="chaxun-xuanxiang-tese">双语课</div> <div id="tese-zht" class="chaxun-xuanxiang-tese">专题研讨课</div>
                                    <div id="tese-shiy" class="chaxun-xuanxiang-tese">实验课</div> <div id="tese-sj" class="chaxun-xuanxiang-tese">实践课</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 gray-text">
                                    年级限制
                                </div>
                                <div class="col-md-10  chaxun-border padding-0">
                                    <div id="xianzhi-all" class="chaxun-xuanxiang-xianzhi chaxun-choose">全部</div>
                                    <div id="xianzhi-3" class="chaxun-xuanxiang-xianzhi">2013级可选</div> <div id="xianzhi-2" class="chaxun-xuanxiang-xianzhi">2012级可选</div>
                                    <div id="xianzhi-1" class="chaxun-xuanxiang-xianzhi">2011级可选</div> <div id="xianzhi-0" class="chaxun-xuanxiang-xianzhi">2010级可选</div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="chaxun-left">
                                <div class="gray-text">搜索</div>
                                <div>
                                    <input id="chaxun-inp" type="text" class="chaxun-input text-click" onkeydown='if(event.keyCode=="13"){document.getElementById("search_result").click();}'/>                   
                                </div>
                                <input name="search_result"  type="button" class="chaxun-submit text-click" id="search_result" value="搜索">
                            </div>
                        </div>
                    </div>
                </div>
               
      
                
                <!--查询结果-->
                               
                <div class="margin-t-20 ">
                    <div id="cx-result" class="normaltab">                        
                        <ul class="nav nav-tabs underline-bold-dblue">
                            <li class="active"><a href="#kechengxinxi-page" data-toggle="tab">查询结果</a></li>
                        </ul>
                    </div>
                    <div class="back-white maincontent">
                        <!--表头-->
                        <div style="margin-bottom:15px;"class="row chaxun-titlefont">
                            <div class="col-md-2  padding-0">
                                <div style="margin-left:20px;"class="padding-0">
                                    <div class="cxresult-frame" style="padding:0;">
                                        <label for="checkbox-00" class="label_check" style="display:none">
                                            <input type="checkbox" checked="" value="1" id="checkbox-00" name="sample-checkbox-00" /><p>&nbsp;</p>
			                            </label>
                                    </div>
                                    <div style="margin-left:20px;"class="display-in-bl">开课院系</div>
                                </div>
                            </div>
                            <div  class="col-md-1  padding-0">课程号</div>
                            <div class="col-md-1  padding-0">课序号</div>
                            <div class="col-md-2  padding-0">课程名</div>
                            <div class="col-md-1  padding-0">学分</div>
                            <div class="col-md-1  padding-0">主讲教师</div>
                            <div class="col-md-2  padding-0">上课时间</div>
                            <div class="col-md-2  padding-0">课程特色</div>
                        </div>
                        <!--表项-->
						<div id="query_course_body">
						</div>
                        
                    </div>
                </div>
                
                <!--分页：既然有了上面这个加载按钮，这个分页真的在此处没有必要吧-->
                <div class="margin-t-20 size-h-50">
                    <div id="page_turning" class="divpage-bar pull-right">
                        <!--a href="#page" class="text-white-link"><div class="margin-11 back-blue divpage-number pull-left">&lt;</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">1</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">2</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">3</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">4</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">10</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 back-blue divpage-number pull-left">&gt;</div></a>
                        <div class="divpage-inputdiv pull-left "><input id="gopage_input" class="divpage-input" /></div>
                        <a href="#page" class="text-white-link"><div class="margin-11 back-blue divpage-number pull-left">Go</div></a-->
                    </div>
                </div>
				<input type="hidden" id="query_sel_yuanxi" value="1" />
				<input type="hidden" id="query_sel_feature" value="1" />
				<input type="hidden" id="query_sel_year" value="1" />
				<input type="hidden" id="query_page" value="1" />
                <input type="hidden" id="query_max_record_number" value="20" />
				
                <!--我关注的/热门课程-->
                <div class="margin-t-20 ">
                    <div id="zgzcontent-tabs" class="zgzh-normaltab">
                            <div id="content-tools" class="pull-right size-w-120 margin-t-10">
                                 <a href="#wgzhd_shezhi" onclick="javascript:delete_edit('bb');" id="wgzhd-edit">
                                      <div id="wgzhd_shezhi" class="xxs_shezhi pull-right" style="margin-right:0px;"></div>
                                 </a>                                      
                            </div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs underline-bold-dorange">
                                <li class="active"><a href="#woguanzhude-page" data-toggle="tab">我关注的</a></li>
                                <li class=""><a href="#remenkecheng-page" data-toggle="tab">热门课程</a></li>
                            </ul>
                    </div>
                    <div id="zgzh-content-pages" class="back-white maincontent">
                        <!-- Tab panes -->
                        <div class="tab-content">
                                <!-- 低侧栏1：我关注的 -->
                                <div class="tab-pane active" id="woguanzhude-page">
                                    <!--表头-->
                                    <div style="margin-bottom:15px;"class="row chaxun-titlefont">
                                        <div class="col-md-2  padding-0">
                                            <div style="margin-left:20px;"class="padding-0">
                                                <div class="cxresult-frame" style="padding:0;">
                                                    <label for="checkbox-10" class="label_check wgzhd" style= "display:none">
                                                        <input type="checkbox"  value="1" id="checkbox-10" name="sample-checkbox-10" /><p>&nbsp;</p>
			                                        </label>
                                                </div>
                                                <div style="margin-left:20px;"class="display-in-bl">开课院系</div>
                                            </div>
                                        </div>
                                        <div  class="col-md-1  padding-0">课程号</div>
                                        <div class="col-md-1  padding-0">课序号</div>
                                        <div class="col-md-2  padding-0"> 课程名</div>
                                        <div class="col-md-1  padding-0">学分</div>
                                        <div class="col-md-1  padding-0">主讲教师</div>
                                        <div class="col-md-2  padding-0">上课时间</div>
                                        <div class="col-md-2  padding-0">课程特色</div>
                                    </div>
                                    <!--表项-->
									<div id="woguanzhude-body">
                                    </div>
                                    <div id="bb" style="display:none">
                                       <input name="search_result"  type="button"  class="info-delete" id="zgzh-wgzhd-delete" value="删除">
                                    </div>
                                </div>
                                
                                <!-- 低侧栏2：热门课程 -->
                                <div class="tab-pane" id="remenkecheng-page">
                                    <div style="margin-top:-15px;">
                                        <div class="row rmkch-content-white">
                                            <div class="col-md-4 rmkc-border-white">
                                                <div class="col-md-5">
                                                    <div class="rmkch-course-name">
                                                        大学生心理健康
                                                    </div>
                                                    <div>
                                                        某某某 1223票
                                                    </div>
                                                    <div>
                                                        地点：六教6A206
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div>
                                                        <div class="rmkch-pingfen-name">含金量</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:108px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">轻松度</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:114px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9.5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">成绩表</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:96px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">8</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-4 rmkc-border-white">
                                                <div class="col-md-5">
                                                    <div class="rmkch-course-name">
                                                        大学语文
                                                    </div>
                                                    <div>
                                                        某某某 1183票
                                                    </div>
                                                    <div>
                                                        地点：六教6A206
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div>
                                                        <div class="rmkch-pingfen-name">含金量</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:114px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9.5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">轻松度</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:60px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">成绩表</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:101px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">8.4</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-5">
                                                    <div class="rmkch-course-name">
                                                        大学语文
                                                    </div>
                                                    <div>
                                                        某某某 1183票
                                                    </div>
                                                    <div>
                                                        地点：六教6A206
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div>
                                                        <div class="rmkch-pingfen-name">含金量</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:114px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9.5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">轻松度</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:60px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">成绩表</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:101px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">8.4</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row rmkch-content-gray">
                                            <div class="col-md-4 rmkc-border-gray">
                                                <div class="col-md-5">
                                                    <div class="rmkch-course-name">
                                                        大学生心理健康
                                                    </div>
                                                    <div>
                                                        某某某 1223票
                                                    </div>
                                                    <div>
                                                        地点：六教6A206
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div>
                                                        <div class="rmkch-pingfen-name">含金量</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:108px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">轻松度</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:114px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9.5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">成绩表</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:96px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">8</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 rmkc-border-gray">
                                                <div class="col-md-5">
                                                    <div class="rmkch-course-name">
                                                        大学生心理健康
                                                    </div>
                                                    <div>
                                                        某某某 1223票
                                                    </div>
                                                    <div>
                                                        地点：六教6A206
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div>
                                                        <div class="rmkch-pingfen-name">含金量</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:108px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">轻松度</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:114px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9.5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">成绩表</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:96px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">8</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="col-md-5">
                                                    <div class="rmkch-course-name">
                                                        大学生心理健康
                                                    </div>
                                                    <div>
                                                        某某某 1223票
                                                    </div>
                                                    <div>
                                                        地点：六教6A206
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div>
                                                        <div class="rmkch-pingfen-name">含金量</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:108px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">轻松度</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:114px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9.5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">成绩表</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:96px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">8</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row rmkch-content-white">
                                            <div class="col-md-4 rmkc-border-white">
                                                <div class="col-md-5">
                                                    <div class="rmkch-course-name">
                                                        大学生心理健康
                                                    </div>
                                                    <div>
                                                        某某某 1223票
                                                    </div>
                                                    <div>
                                                        地点：六教6A206
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div>
                                                        <div class="rmkch-pingfen-name">含金量</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:108px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">轻松度</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:114px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9.5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">成绩表</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:96px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">8</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-4 rmkc-border-white">
                                                <div class="col-md-5">
                                                    <div class="rmkch-course-name">
                                                        大学语文
                                                    </div>
                                                    <div>
                                                        某某某 1183票
                                                    </div>
                                                    <div>
                                                        地点：六教6A206
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div>
                                                        <div class="rmkch-pingfen-name">含金量</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:114px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9.5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">轻松度</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:60px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">成绩表</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:101px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">8.4</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-5">
                                                    <div class="rmkch-course-name">
                                                        大学语文
                                                    </div>
                                                    <div>
                                                        某某某 1183票
                                                    </div>
                                                    <div>
                                                        地点：六教6A206
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div>
                                                        <div class="rmkch-pingfen-name">含金量</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:114px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">9.5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">轻松度</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:60px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">5</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="rmkch-pingfen-name">成绩表</div>
                                                        <div class="rmkch-pingfen-backg">
                                                            <div style="width:101px;"class="rmkch-pingfen-con ">
                                                                <div class="rmkch-pingfen-num pull-right">8.4</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    function delete_edit(comment_list_number) {
                        if ($("#" + comment_list_number)[0].style.display == "none") {
                            $("#" + comment_list_number).slideDown();
                            $(".wgzhd").attr("style", "display:block");
                        } else {
                            $("#" + comment_list_number).slideUp();
                            $(".wgzhd").attr("style", "display:none");
                        }
                    }              
                </script>
                
            </div>
            
        </div>
        <!--登录表单-->
        <div id="logincontent" class="abs" style="display: none;">
            <div id="login-innner">
                <div id="login-head">
                    <div class="login-cancel" onclick="uLogindisplay();"></div>
                </div>
                <form id="form1" name="form-login1">
                    <div>
                        <div class="login-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="login-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="login-fragment-checkcode">
                            <input class="login-input-checkcode text-click" type="text"/>
                            <img class="login-checkcode-pic" src=""/>
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-blank-m autologin" >
                            <input type="checkbox" id="input_autologin"/>
                            下次自动登录
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-loginbtn" onclick="uLogindisplay();"></div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-tosignup" onclick="uLogindisplay();uRegShow();"></div>
                    </div>
                </form>
                <div id="login-foot">
                </div>
            </div>
        </div>
        <!--注册表单-->
        <div id="signupcontent" class="abs" style="display: none;">
            <div id="signup-innner">
                <div id="signup-head">
                    <div class="login-cancel" onclick="uRegdisplay();"></div>
                </div>
                <form id="form_signup" name="form-signup1">
                    <div>
                        <div class="signup-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-idnum">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-name">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-checkcode">
                        	<input class="login-input-checkcode text-click" type="text"/>
                            <img class="signup-checkcode-pic" src=""/>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-agreement" onclick="open_agreement();">
                        	<div id="signup-agree" class="signup-fragment-needagree" onclick="agreement_toggle();"></div>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-signupbtn" onclick="uRegdisplay();"></div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-tologin" onclick="uRegdisplay();uLoginShow();"></div>
                    </div>
                </form>
                <div id="signup-foot">
                </div>
            </div>
        </div>   
        
    </body>
    


    <script src="/js/index.js"></script>
    <script type="text/javascript" src="/js/checkbox.js"></script>
    <script type="text/javascript" src="/js/search.js"></script>
</html>
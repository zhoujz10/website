<?php

class Teacher_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function query_teacher_id($teacher_key) {
        return $this->db->query('SELECT teacher_id FROM teacher WHERE primary_key = ' . $teacher_key)->result()[0]->teacher_id;
    }
    
    function query_teacher_info($school_key,$teacher_id) {
        return $this->db->query('SELECT * FROM teacher WHERE school_key = ' . $school_key . ' AND teacher_id = ' . $teacher_id)->result();
    }
}

?>

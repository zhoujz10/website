<?php

class Material_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function generate($length) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $code = '';
        for ( $i = 0; $i < $length; $i++ )
        {
            // 这里提供两种字符获取方式
            // 第一种是使用 substr 截取$chars中的任意一位字符；
            // 第二种是取字符数组 $chars 的任意元素
            // $shorturl .= substr($chars, mt_rand(0, strlen($chars) – 1), 1);
            $code .= $chars[ mt_rand(0, strlen($chars) - 1) ];
        }
        return $code;
    }

    function upload_file($title,$title_tag,$user_id,$user_name,$course_key,$course_id,$sub_id,$file_tag,$description,$upload_data){
        $CI = & get_instance();
        log_message('debug','function');
        foreach($upload_data as $item => $value) {
            if($item=="raw_name") { $raw_name=$value; }
            else if($item=="orig_name") { $orig_name=$value; }
            else if($item=="client_name") { $client_name=$value; }
            else continue;
        }
        
        $primary_key = $this->db->count_all('material') + 1;
        
        //如果使用客户端上的文件名
        if ($title_tag == true)
            $title = $client_name;
        log_message('debug',$orig_name);
        log_message('debug',$raw_name);
        log_message('debug',$client_name);
        if (file_exists('./uploads/'.$orig_name)) {
            if (rename('./uploads/'.$orig_name,'./uploads/'.$raw_name ))
            {
                $info = array("primary_key" => $primary_key, "user_id" => $user_id, "user_name" => $user_name, "course_key" => $course_key, "school" => "清华大学", "course_id" => $course_id, "sub_id" => $sub_id, "title" => $title, "file_name" => $upload_data['raw_name'], "tag" => $file_tag, "type" => $upload_data['file_type'], "ext" => $upload_data['file_ext'], "is_image" => $upload_data['is_image'], "image_width" => $upload_data['image_width'], "image_height" => $upload_data['image_height'], "description" => $description, "content_preview" => "", "click" => 0, "download" => 0);
                $this->db->insert("material", $info);
                return 1;
            }
        }
        else return 0;
    }

    function download_info($primary_key) {
        return $this->db->query("SELECT title,raw_name FROM material WHERE primary = " . $primary_key);
    }


}
?>
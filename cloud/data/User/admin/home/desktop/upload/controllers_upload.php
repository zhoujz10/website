<?php

class Upload extends CI_Controller {
 
 function __construct()
 {
  parent::__construct();
  $this->load->helper(array('form', 'url'));
 }
 
 function index()
 { 
  
   $this->load->model("upload_m");
   $result=$this->upload_m->ip_find();
   $errors="file upload from this ip:</br>-------------------------------------------</br><b>___Date___|_Code_|_Filename</b></br>";
		foreach ($result as $row)
{if($row->id==1)
{
   $errors.=$row->date."_|_".$row->sfile."_|_".$row->lfile."</br>";
}
}
$this->load->view('upload_form', array('error' => $errors ));
 }
 
 function do_upload()
 {
    $this->load->model("upload_m");
     
  $config['upload_path'] = './uploads/';
  $config['allowed_types'] = 'gif|jpg|png';
  $config['max_size'] = '2000';
  $config['remove_spaces'] = 'TRUE';
  $config['file_name'] = $this->upload_m->generate(4);
  //$config['max_width']  = '1024';
  //$config['max_height']  = '768';
  
  $this->load->library('upload', $config);
 
  if ( ! $this->upload->do_upload())
  {
   $error = array('error' => $this->upload->display_errors());
   
   $this->load->view('upload_form', $error);
  } 
  else
  {
   $data = array('upload_data' => $this->upload->data());
   $this->upload_m->save_upload_sql($data['upload_data']);
   $this->load->view('upload_success', $data);
  }
 }
 
 function do_download()
 {
     
     $code=$this->input->post('filecode', TRUE);
      if(strlen($code)==4)
      {
          $this->load->model("upload_m");
          $url=$this->upload_m->downloadurl($code);
          $this->load->helper('download');
          $file = file_get_contents("./uploads/".$code); // 读文件内容
          force_download($url, $file);
          $data = array('data' => "Success!");
      }
      else 
      {$data = array('data' => 'Error!');}
      $this->load->view('download_success',$data);
 }
}
?>

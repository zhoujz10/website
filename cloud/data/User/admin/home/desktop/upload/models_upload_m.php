<?php

    
class Upload_m extends CI_Model {
    
    
 function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
function generate( $length) {  
// 短链接所使用的字符集，可任意添加你需要的字符  
$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';  
$code = '';  
for ( $i = 0; $i < $length; $i++ )  
{  
// 这里提供两种字符获取方式  
// 第一种是使用 substr 截取$chars中的任意一位字符；  
// 第二种是取字符数组 $chars 的任意元素  
// $shorturl .= substr($chars, mt_rand(0, strlen($chars) – 1), 1);  
$code .= $chars[ mt_rand(0, strlen($chars) - 1) ];  
}  
return $code; 
} 



function save_upload_sql($upload_data){
    $CI =& get_instance();
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) { 
            $ip=$_SERVER['HTTP_CLIENT_IP']; 
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { 
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; 
         }else { 
            $ip=$_SERVER['REMOTE_ADDR']; 
         } 
         foreach($upload_data as $item => $value) {
 if($item=="raw_name") { $raw_name=$value; }
 else if($item=="orig_name") { $orig_name=$value; }
 else if($item=="client_name") { $client_name=$value; }
 else continue;
}
    if (file_exists('./uploads/'.$orig_name)){
if (rename('./uploads/'.$orig_name,'./uploads/'.$raw_name ))
{
    $info = array("userip" => $ip, "sfile" => $raw_name, "lfile" => $client_name, "date"=>date("Y-m-d"));
    $this->db->insert("filesave", $info);
    return 1;
}
}
else return 0;
}


function downloadurl($code){
    $this->db->where("sfile", $code);
		$this->db->select("lfile");
		$R = $this->db->get("filesave");
		foreach ($R->result() as $row)
{
   return $row->lfile;
}

}

function ip_find()
{
    $CI =& get_instance();
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) { 
            $ip=$_SERVER['HTTP_CLIENT_IP']; 
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { 
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; 
         }else { 
            $ip=$_SERVER['REMOTE_ADDR']; 
         } 
           $this->db->where("userip", $ip);
		$this->db->select("*");
		$R = $this->db->get("filesave");
	    return $R->result();
}
}
?>

﻿//查询条件

        $(document).ready(function () {
            $("div.chaxun-xuanxiang-yuanxi").click(function () {
                var imgid = $(this).attr("id");
                $(".chaxun-xuanxiang-yuanxi").siblings().css('background-color', 'white');
                $(".chaxun-xuanxiang-yuanxi").siblings().css('box-shadow', '0px 0px 0px #fff');
                $(".chaxun-xuanxiang-yuanxi").siblings().css('border-radius', '0px');
                $("div#" + imgid).css('background-color', '#ffeeaa');
                $("div#" + imgid).css('box-shadow', 'inset 0px 2px 2px #bbbbbb');
                $("div#" + imgid).css('border-radius', '3px');
				
				var sel = '0';
				switch($(this).html())
				{
				case '全部':
					sel = '1';
					break;
				case '建筑学院':
					sel = '2';
					break;
				case '城规系':
					sel = '3';
					break;
				case '建筑系':
					sel = '4';
					break;
				case '土木系':
					sel = '5';
					break;
				case '水利系':
					sel = '6';
					break;
				case '环境学院':
					sel = '7';
					break;
				case '机械系':
					sel = '8';
					break;
				case '精仪系':
					sel = '9';
					break;
				case '热能系':
					sel = '10';
					break;
				case '汽车系':
					sel = '11';
					break;
				case '工业工程系':
					sel = '12';
					break;
				case '信息学院':
					sel = '13';
					break;
				case '电机系':
					sel = '14';
					break;
				case '电子系':
					sel = '15';
					break;
				case '计算机系':
					sel = '16';
					break;
				case '自动化系':
					sel = '17';
					break;
				case '微纳电子系':
					sel = '18';
					break;
				case '航院':
					sel = '19';
					break;
				case '工物系':
					sel = '20';
					break;
				case '化工系':
					sel = '21';
					break;
				case '材料学院':
					sel = '22';
					break;
				case '数学系':
					sel = '23';
					break;
				case '物理系':
					sel = '24';
					break;
				case '化学系':
					sel = '25';
					break;
				case '生命学院':
					sel = '26';
					break;
				case '地球科学中心':
					sel = '27';
					break;
				case '交叉信息学院':
					sel = '28';
					break;
				case '周培源应':
					sel = '29';
					break;
				case '经管学院':
					sel = '30';
					break;
				case '公共管理':
					sel = '31';
					break;
				case '金融学院':
					sel = '32';
					break;
				case '人文学院':
					sel = '33';
					break;
				case '社科学院':
					sel = '34';
					break;
				case '中文系':
					sel = '35';
					break;
				case '外文系':
					sel = '36';
					break;
				case '法学院':
					sel = '37';
					break;
				case '新闻学院':
					sel = '38';
					break;
				case '马克思主义学院':
					sel = '39';
					break;
				case '人文学院':
					sel = '40';
					break;
				case '社科学院':
					sel = '41';
					break;
				case '体育部':
					sel = '42';
					break;
				case '电教中心':
					sel = '43';
					break;
				case '图书馆':
					sel = '44';
					break;
				case '艺教中心':
					sel = '45';
					break;
				case '美术学院':
					sel = '46';
					break;
				case '土水学院':
					sel = '47';
					break;
				case '建管系':
					sel = '48';
					break;
				case '建筑技术':
					sel = '49';
					break;
				case '核研院':
					sel = '50';
					break;
				case '教研院':
					sel = '51';
					break;
				case '网络中心':
					sel = '52';
					break;
				case '训练中心':
					sel = '53';
					break;
				case '电工电子中心':
					sel = '54';
					break;
				case '宣传部':
					sel = '55';
					break;
				case '学生部':
					sel = '56';
					break;
				case '武装部':
					sel = '57';
					break;
				case '研究生院':
					sel = '58';
					break;
				case '深研生院':
					sel = '59';
					break;
				case '校医院':
					sel = '60';
					break;
				case '医学院':
					sel = '61';
					break;
				case '生医系':
					sel = '62';
					break;
				case '软件学院':
					sel = '63';
					break;
				default:
					sel = '0';
					break;
				}
				$('#query_page').val('1');
				$('#query_sel_yuanxi').val(sel);
				$.fn.query_course();
            });

            $("div.chaxun-xuanxiang-tese").click(function () {
                var imgid = $(this).attr("id");
                $("div#" + imgid).siblings().css('background-color', 'white');
                $("div#" + imgid).siblings().css('box-shadow', '0px 0px 0px #fff');
                $("div#" + imgid).siblings().css('border-radius', '0px');
                $("div#" + imgid).css('background-color', '#ffeeaa');
                $("div#" + imgid).css('box-shadow', 'inset 0px 2px 2px #bbbbbb');
                $("div#" + imgid).css('border-radius', '3px');
				
				var sel = '0';
				switch($(this).html())
				{
				case '全部':
					sel = '1';
					break;
				case '文化素质核心课':
					sel = '2';
					break;
				case '体育课':
					sel = '3';
					break;
				case '新生研讨课':
					sel = '4';
					break;
				case '双语课':
					sel = '5';
					break;
				case '专题研讨课':
					sel = '6';
					break;
				case '实验课':
					sel = '7';
					break;
				case '实践课':
					sel = '8';
					break;
				default:
					sel = '0';
					break;
				}
				$('#query_page').val('1');
				$('#query_sel_feature').val(sel);
				$.fn.query_course();
            });

            $("div.chaxun-xuanxiang-xianzhi").click(function () {
                var imgid = $(this).attr("id");
                $("div#" + imgid).siblings().css('background-color', 'white');
                $("div#" + imgid).siblings().css('box-shadow', '0px 0px 0px #fff');
                $("div#" + imgid).siblings().css('border-radius', '0px');
                $("div#" + imgid).css('background-color', '#ffeeaa');
                $("div#" + imgid).css('box-shadow', 'inset 0px 2px 2px #bbbbbb');
                $("div#" + imgid).css('border-radius', '3px');
				
				var sel = '0';
				switch($(this).html())
				{
				case '全部':
					sel = '1';
					break;
				case '2013级可选':
					sel = '2';
					break;
				case '2012级可选':
					sel = '3';
					break;
				case '2011级可选':
					sel = '4';
					break;
				case '2010级可选':
					sel = '5';
					break;
				default:
					sel = '0';
					break;
				}
				$('#query_page').val('1');
				$('#query_sel_year').val(sel);
				$.fn.query_course();
            });
        });

        function slideShowReply(comment_list_number) {
            if ($("#" + comment_list_number)[0].style.display == "none") {
                $("#" + comment_list_number).slideDown();
                document.chaxun_form.ok.value = "收起";
            } else {
                $("#" + comment_list_number).slideUp();
                document.chaxun_form.ok.value = "更多";
            }
        }
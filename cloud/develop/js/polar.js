//图表显示js
    	    window.onload = function () {
    	        //初始化
    	        size = 200; //尺寸参数

    	        var r = Raphael("holder", size, size), //绘图区域大小
                R = size / 3, //刻度点最大半径
                    init = true, //初始化
                    param = { stroke: "#fff", "stroke-width": size / 20 }, //环形条宽度
                    hash = document.location.hash,
                    marksAttr = { fill: hash || "#444", stroke: "none" },
                    html = [//数据源
                        document.getElementById("w-zuoye"),
                        document.getElementById("w-qiandao"),
                        document.getElementById("w-kaoshi"),
                        document.getElementById("w-xiaozu")
                    ];
    	        // Custom Attribute
    	        //计算环形条角度和颜色的函数，用户定义
    	        r.customAttributes.arc = function (value, total, R) {
    	            var alpha = 360 / total * value,
                        a = (90 - alpha) * Math.PI / 180,
                        x = size / 2 + R * Math.cos(a),
                        y = size / 2 - R * Math.sin(a),
                        color = "hsb(".concat(Math.round(R) / R, ",", value / total, ", .75)"),
                        path;
    	            if (total == value) {
    	                path = [["M", size / 2, size / 2 - R], ["A", R, R, 0, 1, 1, 299.99, 300 - R]];
    	            } else {
    	                path = [["M", size / 2, size / 2 - R], ["A", R, R, 0, +(alpha > 180), 1, x, y]];
    	            }
    	            return { path: path, stroke: color };
    	        };

    	        var zuoye = r.path().attr(param).attr({ arc: [0, 100, R] });
    	        R -= size / 15; //缩减半径，再次绘制
    	        var qiandao = r.path().attr(param).attr({ arc: [0, 100, R] });
    	        R -= size / 15;
    	        var kaoshi = r.path().attr(param).attr({ arc: [0, 100, R] });
    	        R -= size / 15;
    	        var xiaozu = r.path().attr(param).attr({ arc: [0, 100, R] });

    	        //更新数据
    	        function updateVal(value, total, R, hand, id) {
    	            var color = "hsb(".concat(Math.round(R) / R, ",", value / total, ", .75)");
    	            if (init) {
    	                hand.animate({ arc: [value, total, R] }, 900, ">");
    	            } else {
    	                if (!value || value == total) {
    	                    value = total;
    	                    hand.animate({ arc: [value, total, R] }, 750, "bounce", function () {
    	                        hand.attr({ arc: [0, total, R] });
    	                    });
    	                } else {
    	                    hand.animate({ arc: [value, total, R] }, 750, "elastic");
    	                }
    	            }
    	            html[id].innerHTML = (value < 10 ? "0" : "") + value;
    	            html[id].style.color = Raphael.getRGB(color).hex;
    	        }

    	        //绘制刻度点的函数
    	        function drawMarks(R, total) {
    	            var color = "hsb(".concat(Math.round(R) / R, ", 1, .75)"),
                        out = r.set();
    	            for (var value = 0; value < total; value++) {
    	                var alpha = 360 / total * value,
                            a = (90 - alpha) * Math.PI / 180,
                            x = size / 2 + R * Math.cos(a),
                            y = size / 2 - R * Math.sin(a);
    	                out.push(r.circle(x, y, 2).attr(marksAttr));
    	            }
    	            return out;
    	        }

    	        (function () {
    	            var w = [
                        document.getElementById("w-zuoye"),
                        document.getElementById("w-qiandao"),
                        document.getElementById("w-kaoshi"),
                        document.getElementById("w-xiaozu")
                    ];
    	            R = size / 3;
    	            updateVal(w[0].innerHTML, 100, R, zuoye, 0);
    	            updateVal(w[1].innerHTML, 100, R -= size / 15, qiandao, 1);
    	            updateVal(w[2].innerHTML, 100, R -= size / 15, kaoshi, 2);
    	            updateVal(w[3].innerHTML, 100, R -= size / 15, xiaozu, 3);
    	            init = false;
    	        })();
    	    };
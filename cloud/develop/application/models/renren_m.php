<?php

class Renren_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
	function connectRenren() {
	}
	function generateQuestionImg($content, $user_id) {
		$height = 500;
		$width = 700;
		$heightuser = 300;
		$widthuser = 300;
		$dst_x = 10;
		$dst_y = 10;
		$pct = 80;
		$userpath = "./userpic/pic".$user_id.".jpg";
		$imuser = imagecreatefromjpeg($userpath);
		$im = imageCreateTrueColor($width, $height);
		$white = imageColorAllocate($im, 255, 255, 255);
		$blue = imageColorAllocate($im, 0, 0, 64);
		$black = imageColorAllocate($im, 0, 0, 0);
		$red = imageColorAllocate($im, 240, 0, 0);
		imageFill($im, 0, 0, $white);
		//imageString($im, 4, 50, 150, $content, $white);
		$font = '/font/renrenQuestion.TTF';
		$imuserStd = imagecreatetruecolor($widthuser, $heightuser);
		$imusersize = getimagesize($userpath);
		imagecopyresampled($imuserStd, $imuser, 0,0,0,0, $widthuser, $heightuser, $imusersize[0], $imusersize[1]);
		imageDestroy($imuser);
		imagesetthickness($im, 8);
		imageline($im, 0, 85, $width, 85, $red);
		imagettftext($im, 20, 0, 40, 60, $black, $font, $content);
		imagecopymerge($im, $imuserStd, 10, 100, 0, 0, $widthuser, $heightuser, $pct);
		imageDestroy($imuserStd);
		return $im;
	}
	function addPhoto($user_id, $content) {
		$im = $this->generateQuestionImg($content, $user_id);
		$path = "./imgtemp/questionImg".$user_id.".png";
		$file = fopen($path, "w");
		fclose($file);
		imagePng($im, $path);
		imageDestroy($im);
	}

	function insert($info) {
		$this->db->insert("questionrrswap", $info);
	}
}
?>

<?php

class study_experience_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

	function get_study_experience_list($course_key, $page) {
		//获取某门课程的日志列表
        $sql = "SELECT * FROM study_experience WHERE course_key=".$course_key." limit ". (10*($page-1)).","."10";
        $R = $this->db->query($sql);
        return $R->result();
    }
    function query_num($course_key) {
		//获取某门课程的学习经验数
        $sql = "SELECT count(*) AS num FROM study_experience WHERE course_key=".$course_key;
        $R = $this->db->query($sql);
        return $R->result()[0]->num;
    }

	function querymaxid() {
		//查询id最大的study_experience的id，这个函数需要改，可以通过更简单的方法实现
		$sql = "select * from study_experience order by primary_key DESC limit 0,1";
		$R = $this->db->query($sql);
		return $R->result()[0]->primary_key;
	}

	function insert_new_study_experience($info) {
		//将新的study_experience插入数据库
		$this->db->insert("study_experience", $info);
	}
	
	function insert_new_reply($info) {
		//将新的reply插入数据库
		$this->db->insert("study_experience_reply", $info);
		
		$sql = "SELECT * FROM study_experience WHERE id = ?";
		$R = $this->db->query($sql, array($info['study_experience_key']));
		$primary_key = $R->result()[0]->primary_key;
		$reply = $R->result()[0]->reply + 1;
		$data = array("reply" => $reply);
		$this->db->where("primary_key", $primary_key);
		$this->db->update("study_experience", $data);
	}

	function count_study_experience() {
		//返回study_experience中的最大的primary_key
		return $this->db->count_all('study_experience');
	}
	
	function count_reply() {
		//返回study_experience_reply中的最大的primary_key
		return $this->db->count_all('study_experience_reply');
	}
	
	function query_study_experience_list($course_primary_key,$page,$max_study_experience_number) {
		//查询某一门课某一页的学习经验列表
		$R = $this->db->query("SELECT * FROM study_experience WHERE course_key = ? limit " . (($page-1)*$max_study_experience_number) . "," . $max_study_experience_number, array($course_primary_key));
		return $R->result();
	}
	
	function query_study_experience_reply_list($study_experience_key) {
		//查询某一门课某一页的学习经验列表
		$R = $this->db->query("SELECT * FROM study_experience_reply WHERE study_experience_key = ? ORDER BY primary_key", array($study_experience_key));
		return $R->result();
	}

	function get_study_experience($id) {
		//取某个study_experience的内容，并更新点击数
		$this->db->where("primary_key", $id);
		$this->db->select("*");
		$R = $this->db->get("study_experience");

		$primary_key = $R->result()[0]->primary_key;
		$click = $R->result()[0]->click + 1;
		$data = array("click" => $click);
		$this->db->where("primary_key", $primary_key);
		$this->db->update("study_experience", $data);
		return $R->result();
	}
	
	function get_course_key_by_study_experience_key($id) {
		//获取课程主码
		$this->db->where("primary_key", $id);
		$this->db->select("*");
		$R = $this->db->get("study_experience");
		$result = $R->result();
		if (count($result) == 0)
			return null;
		$primary_key = $result[0]->course_key;
		return $primary_key;
	}
	
	function get_reply($id) {
		//取某个study_experience的reply的内容
		$this->db->where("study_experience_key", $id);
		$this->db->select("*");
		$R = $this->db->get("study_experience_reply");
		return $R->result();
	}
	
	function insert_study_experience($course_primary_key,$user_id,$user_name,$title,$content_preview) {
		//写学习经验
		$R = $this->db->query("SELECT course_id,sub_id,school FROM course WHERE primary_key = ?", array($course_primary_key));
		if(count($R->result()) == 0)
			return 0;
		else
		{
			$primary_key = $this->db->count_all('study_experience')+1;
			$info = array("primary_key" => $primary_key, "user_id" => $user_id, "user_name" => $user_name, "course_key" => $course_primary_key, "school" => $R->result()[0]->school, "course_id" => $R->result()[0]->course_id, "sub_id" => $R->result()[0]->sub_id, "title" => $title, "content_preview" => $content_preview, "store_address" => "./study_experience/" . $course_primary_key . "_" . $user_id . "_" . $primary_key . ".data", "click" => 0, "reply" => 0, "like" => 0);
			$this->db->insert("study_experience",$info);
			return $primary_key;
		}
	}
	
	function query_study_experience($primary_key) {
        $R = $this->db->query("SELECT * FROM study_experience WHERE primary_key = " . $primary_key);
        return $R->result();
	}
	
	function query_study_experience_reply($primary_key) {
        $R = $this->db->query("SELECT * FROM study_experience_reply WHERE primary_key = " . $primary_key);
        return $R->result();
	}
	
	function like($flag, $user_id, $primary_key) {
		$tag = 0;//用于标记是增加赞还是取消赞
		if ($flag == '1')
		{
			$R = $this->db->query("SELECT * from study_experience_like where study_experience_key = " . $primary_key . " AND user_id = " . $user_id);
			if(count($R->result()) == 0)
			{
				$info = array("primary_key" => $this->db->count_all('study_experience_like')+1, "user_id" => $user_id, "study_experience_key" => $primary_key, "tag" => 1);
				$this->db->insert('study_experience_like',$info);
				$tag = 1;
			}
			else if($R->result()[0]->tag == 0)
			{
				$this->db->where("primary_key", $R->result()[0]->primary_key);
				$info = array("tag" => '1');
				$this->db->update("study_experience_like", $info);
				$tag = 1;
			}
			else
			{
				$this->db->where("primary_key", $R->result()[0]->primary_key);
				$info = array("tag" => '0');
				$this->db->update("study_experience_like", $info);
			}
			
			if($tag == 1)
			{
				$R = $this->db->query("SELECT * from study_experience where primary_key = " . $primary_key);
				$this->db->where("primary_key", $primary_key);
				$info = array("like" => $R->result()[0]->like+1);
				$this->db->update("study_experience", $info);
			}
			else
			{
				$R = $this->db->query("SELECT * from study_experience where primary_key = " . $primary_key);
				$this->db->where("primary_key", $primary_key);
				$info = array("like" => $R->result()[0]->like-1);
				$this->db->update("study_experience", $info);
			}
		}
		else
		{
			$R = $this->db->query("SELECT * from study_experience_reply_like where study_experience_reply_key = " . $primary_key . " AND user_id = " . $user_id);
			if(count($R->result()) == 0)
			{
				$info = array("primary_key" => $this->db->count_all('study_experience_reply_like')+1, "user_id" => $user_id, "study_experience_reply_key" => $primary_key, "tag" => 1);
				$this->db->insert('study_experience_reply_like',$info);
				$tag = 1;
			}
			else if($R->result()[0]->tag == 0)
			{
				$this->db->where("primary_key", $R->result()[0]->primary_key);
				$info = array("tag" => '1');
				$this->db->update("study_experience_reply_like", $info);
				$tag = 1;
			}
			else
			{
				$this->db->where("primary_key", $R->result()[0]->primary_key);
				$info = array("tag" => '0');
				$this->db->update("study_experience_reply_like", $info);
			}
			
			if($tag == 1)
			{
				$R = $this->db->query("SELECT * from study_experience_reply where primary_key = " . $primary_key);
				$this->db->where("primary_key", $primary_key);
				$info = array("like" => $R->result()[0]->like+1);
				$this->db->update("study_experience_reply", $info);
			}
			else
			{
				$R = $this->db->query("SELECT * from study_experience_reply where primary_key = " . $primary_key);
				$this->db->where("primary_key", $primary_key);
				$info = array("like" => $R->result()[0]->like-1);
				$this->db->update("study_experience_reply", $info);
			}
		}
		return $tag;
	}
	
	function query_max_reply_id() {
        $R = $this->db->query("SELECT MAX(primary_key) AS max_reply_id FROM study_experience_reply");
        return $R->result()[0]->max_reply_id;
	}
	
	function insert_new_rely($user_id,$user_name,$primary_key,$content) {
		$R = $this->db->query("SELECT * FROM study_experience WHERE primary_key = " . $primary_key);
		if(count($R->result()) == 0)
			return array("tag" => 0);
		else
		{
			$reply_primary_key = $this->query_max_reply_id() + 1;
			$info = array("primary_key" => $reply_primary_key, "study_experience_key" => $primary_key, "user_id" => $user_id, "user_name" => $user_name, "course_key" => $R->result()[0]->course_key, "content" => $content, "reply" => 0, "like" => 0);
			$this->db->insert("study_experience_reply", $info);
			
			$this->db->where("primary_key", $primary_key);
			$info = array("reply" => $R->result()[0]->reply+1);
			$this->db->update("study_experience",$info);
			
			$time = $this->db->query("SELECT time FROM study_experience_reply WHERE primary_key = " . $reply_primary_key)->result()[0]->time;
			return array("tag" => 1, "reply_primary_key" => $reply_primary_key, "time" => $time);
		}
	}
	
	function insert_new_sub_rely($user_id,$user_name,$primary_key,$content,$reply_pk) {
		$R = $this->db->query("SELECT * FROM study_experience_reply WHERE primary_key = " . $reply_pk);
		if(count($R->result()) == 0)
			return array("tag" => 0);
		else
		{
			$reply_primary_key = $this->query_max_reply_id() + 1;
			$info = array("primary_key" => $reply_primary_key, "study_experience_key" => $primary_key, "user_id" => $user_id, "user_name" => $user_name, "course_key" => $R->result()[0]->course_key, "content" => $content, "reply" => 0, "like" => 0);
			$this->db->insert("study_experience_reply", $info);
			
			$this->db->where("primary_key", $reply_pk);
			$info = array("reply" => $R->result()[0]->reply+1);
			$this->db->update("study_experience_reply",$info);
			
			$RR = $this->db->query("SELECT * FROM study_experience WHERE primary_key = " . $primary_key);
			$this->db->where("primary_key", $primary_key);
			$info = array("reply" => $RR->result()[0]->reply+1);
			$this->db->update("study_experience",$info);
						
			$time = $this->db->query("SELECT time FROM study_experience_reply WHERE primary_key = " . $reply_primary_key)->result()[0]->time;
			return array("tag" => 1, "reply_primary_key" => $reply_primary_key, "time" => $time);
		}
	}
}
?>

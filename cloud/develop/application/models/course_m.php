<?php

class Course_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

	function department($de_course_name) {
		//查询某一院系开设的所有课程信息
		$this->db->where("department", $de_course_name);
		$this->db->select("*");
		$R = $this->db->get("course");
		return $R->result();
	}

	function query_num($a,$b,$c,$d) {
		//查询满足条件的课程门数
		$num1 = count($a);
		$num2 = count($b);
		$num3 = count($c);
		$R = array();
		if($num1 == '0' || $a[0] == '全部')
			$s1 = "";
		else
		{
			$s1 = "department in ('";
			$s1 = $s1 . $a[0] . '\'';
			for($i=1; $i<$num1; $i++)
				$s1 = $s1 . "," . '\'' .$a[$i] . '\'';
			$s1 = $s1 . ")";
			$s1 = "(" . $s1 . ") ";
			$s1 = " AND " . $s1;
		}

		if($num2 == '0' || $b[0] == '全部')
			$s2 = "";
		else
		{
			if($b[0] == '体育课')
				$s2 = "department = '体育部' OR department = '武装部'";
			else
			{
				$s2 = "course_feature like '%";
				$s2 = $s2 . $b[0] . '%\'';
			}
			for($i=1; $i<$num2; $i++)
			{
				if($b[$i] == '体育课')
					$s2 = $s2 . " OR department = '体育部' OR department = '武装部'";
				else
					$s2 = $s2 . " OR course_feature like '%". $b[$i] . "%'";
			}
			$s2 = "(" . $s2 . ")";
			$s2 = " AND " . $s2;
		}

		if($num3 == '0' || $c[0] == '全部')
			$s3 = "";
		else
		{
			$s3 = "course_year is null or course_year = \"\" ";
			for($i=0; $i<$num3; $i++)
				$s3 = $s3 . " OR course_year like '%". $c[$i] . "%'";
			$s3 = "(" . $s3 . ")";
			$s3 = " AND " . $s3;
		}

		$sql = "select count(*) as num FROM course WHERE concat(course_id,sub_id,department,teacher_name,course_name,course_feature) like '%" . $d . "%'";
		$sql = $sql . $s1 . $s2 . $s3;
		if($num1 == '0' && $num2 == '0' && $num3 == '0')
			$sql = "select count(*) as num FROM course WHERE concat(course_id,sub_id,department,teacher_name,course_name,course_feature) like '%" . $d . "%'";
		$R = $this->db->query($sql);
			return $R->result()[0]->num;
	}

	function query_course($a,$b,$c,$d,$page,$max_record_number) {
		//查询满足条件的课程信息
		$num1 = count($a);
		$num2 = count($b);
		$num3 = count($c);
		$R = array();
		if($num1 == '0' || $a[0] == '全部')
			$s1 = "";
		else
		{
			$s1 = "department in ('";
			$s1 = $s1 . $a[0] . '\'';
			for($i=1; $i<$num1; $i++)
				$s1 = $s1 . "," . '\'' .$a[$i] . '\'';
			$s1 = $s1 . ")";
			$s1 = "(" . $s1 . ") ";
			$s1 = " AND " . $s1;
		}

		if($num2 == '0' || $b[0] == '全部')
			$s2 = "";
		else
		{
			if($b[0] == '体育课')
				$s2 = "department = '体育部' OR department = '武装部'";
			else
			{
				$s2 = "course_feature like '%";
				$s2 = $s2 . $b[0] . '%\'';
			}
			for($i=1; $i<$num2; $i++)
			{
				if($b[$i] == '体育课')
					$s2 = $s2 . " OR department = '体育部' OR department = '武装部'";
				else
					$s2 = $s2 . " OR course_feature like '%". $b[$i] . "%'";
			}
			$s2 = "(" . $s2 . ") ";
			$s2 = " AND " . $s2;
		}

		if($num3 == '0' || $c[0] == '全部')
			$s3 = "";
		else
		{
			$s3 = "course_year is null or course_year = \"\" ";
			for($i=0; $i<$num3; $i++)
				$s3 = $s3 . " OR course_year like '%". $c[$i] . "%'";
			$s3 = "(" . $s3 . ")";
			$s3 = " AND " . $s3;
		}

		$sql = "select * FROM course WHERE concat(course_id,sub_id,department,teacher_name,course_name,course_feature) like '%" . $d . "%'";
		$sql = $sql . $s1 . $s2 . $s3;
		if($num1 == '0' && $num2 == '0' && $num3 == '0')
			$sql = "select * FROM course WHERE concat(course_id,sub_id,department,teacher_name,course_name,course_feature) like '%" . $d . "%'";
		$sql = $sql . " limit " . ($max_record_number * ($page - 1)) . "," . $max_record_number;
		$R = $this->db->query($sql);
		return $R->result();
	}

	function searchcourse_name($searchinput) {
		//查询关键字返回课程信息，这个是整个网站的搜索功能，需要重做
		$this->db->where("course_name", $searchinput);
		$this->db->select("*");
		$R = $this->db->get("course");
		return $R->result();
	}

	function search_primary_key_by_id($course_id,$sub_id) {
		//通过course_id,sub_id查询课程primary_key
		$sql = "SELECT primary_key FROM course WHERE course_id = ? AND sub_id = ?";
		$R = $this->db->query($sql, array($course_id, $sub_id));
		return $R->result();
	}
	
	function search_course_name_by_key($primary_key) {
		$sql = "SELECT course_name FROM course WHERE primary_key = ?";
		$R = $this->db->query($sql, array($primary_key));
		return $R->result();
	}
	
	function search_course_by_key($primary_key) {
		//通过course_key查询课程信息
		$sql = "SELECT * FROM course WHERE primary_key = ?";
		$R = $this->db->query($sql, array($primary_key));
		return $R->result();
	}
	
}

?>

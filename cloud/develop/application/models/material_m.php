<?php

class Material_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

	function count_material() {
		return $this->db->count_all('material');
	}

	function get_zl_list($course_key) {
		//查询某课程的资料
		$sql = "SELECT * FROM material WHERE course_key = ?";
		$R = $this->db->query($sql, array($course_key));
		return $R->result();
	}

	function insert_material($info) {
		//插入资料
		$this->db->insert("material", $info);
	}

}
?>
<?php

class Operate_count_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	function ip_status($ip_address) {
		$this->db->where('ip_address',$ip_address);
		$this->db->select("*");
		$R = $this->db->get("operate_count");
		if(!count($R->result()))
			return 0;
		else if($R->result()[0]->operate_count >= 1000)
			return 2;
		else if($R->result()[0]->login_wrong_count >= 20)
			return 2;
		else if($R->result()[0]->operate_count >= 500)
			return 1;
		else if($R->result()[0]->login_wrong_count >= 5)
			return 1;
		else
			return 0;
	}
	
	function user_status($user_id) {
		$this->db->where('user_id',$user_id);
		$this->db->select("*");
		$R = $this->db->get("operate_count");
		if(!count($R->result()))
			return 0;
		else if($R->result()[0]->operate_count >= 1000)
			return 2;
		else if($R->result()[0]->operate_count >= 500)
			return 1;
		else
			return 0;
	}

	function user_id_count($user_id) {
		//用户每次操作后调用此函数，如果用户操作次数达到1000次，就强制跳转到首页，此函数需要更改
		$this->db->where('user_id',$user_id);
		$this->db->select("*");
		$R = $this->db->get("operate_count");
		if(count($R->result()) == '0')
		{
			$id = $this->db->count_all('operate_count')+1;
			$info = array('primary_key' => $id, 'ip_address' => '0', 'user_id' => $user_id, 'operate_count' => '1', 'login_count' => '0', 'login_wrong_count' => '0', 'register_count' => '0');
			$this->db->insert("operate_count",$info);
			return 1;//日操作次数不到1000次
		}
		else
		{
			if(ceil(strtotime(time("Y-m-d H:i:s")) - strtotime($R->result()[0]->time)) < 86400)
			{
				$info = array('primary_key' => $R->result()[0]->id, 'ip_address' => '0', 'user_id' => $user_id, 'operate_count' => $R->result()[0]->operate_count + 1, 'login_count' => $R->result()[0]->login_count, 'login_wrong_count' => $R->result()[0]->login_wrong_count, 'register_count' => '0');
				$this->db->where("user_id", $user_id);
				$this->db->update("operate_count", $info);
				if($R->result()[0]->operate_count+1 >= 1000)
					return 2;//日操作次数达到1000次
				else
					return 1;//日操作次数不到1000次
			}
			else
			{
				$info = array('primary_key' => $R->result()[0]->id, 'ip_address' => '0', 'user_id' => $user_id, 'operate_count' => '1', 'login_count' => '0', 'login_wrong_count' => '0', 'register_count' => '0');
				$this->db->where("user_id", $user_id);
				$this->db->update("operate_count", $info);
				return 1;//日操作次数不到1000次
			}
		}
	}

	function login($user_id,$userdata,$tag) {
		//此函数记录用户的登陆情况，如果登陆次数或登陆错误次数较多，则需要输入验证码
		//在operation_count表中寻找该user_id，并更新记录信息
		if($user_id != 0)
		{
			$this->db->where('user_id',$user_id);
			$this->db->select("*");
			$R = $this->db->get("operate_count");
			if(count($R->result()) == '0')
			{
				$primary_key = $this->db->count_all('operate_count')+1;
				$info = array();
				switch($tag)
				{
				case '1'://登陆信息正确
					$info = array('primary_key' => $primary_key, 'ip_address' => '0', 'user_id' => $user_id, 'operate_count' => '1', 'login_count' => '1', 'login_wrong_count' => '0', 'register_count' => '0');
					break;
				case '0'://密码错误
					$info = array('primary_key' => $primary_key, 'ip_address' => '0', 'user_id' => $user_id, 'operate_count' => '1', 'login_count' => '1', 'login_wrong_count' => '1', 'register_count' => '0');
					break;
				default:
					break;
				}
				$this->db->insert("operate_count",$info);
			}
			else
			{
				$info = array();	
				switch($tag)
				{
				case '1'://登录信息正确
					$info = array('primary_key' => $R->result()[0]->primary_key, 'ip_address' => '0', 'user_id' => $user_id, 'operate_count' => $R->result()[0]->operate_count+1, 'login_count' => $R->result()[0]->login_count+1, 'login_wrong_count' => $R->result()[0]->login_wrong_count, 'register_count' => '0');
					break;
				case '0'://密码错误
					$info = array('primary_key' => $R->result()[0]->primary_key, 'ip_address' => '0', 'user_id' => $user_id, 'operate_count' => $R->result()[0]->operate_count+1, 'login_count' => $R->result()[0]->login_count+1, 'login_wrong_count' => $R->result()[0]->login_wrong_count+1, 'register_count' => '0');
					break;
				default:
					break;
				}
				$this->db->where("user_id", $user_id);
				$this->db->update("operate_count", $info);
			}
		}

		//在operation_count表中寻找该ip，并更新记录信息
		$this->db->where('ip_address',$userdata['ip_address']);
		$this->db->select("*");
		$R = $this->db->get("operate_count");
		if(count($R->result()) == 0)
		{
			$primary_key = $this->db->count_all('operate_count')+1;
			switch($tag)
			{
			case '1'://登陆信息正确
				$info = array('primary_key' => $primary_key, 'ip_address' => $userdata['ip_address'], 'user_id' => '0', 'operate_count' => '1', 'login_count' => '1', 'login_wrong_count' => '0', 'register_count' => '0');
				break;
			case '0'://密码错误
				$info = array('primary_key' => $primary_key, 'ip_address' => $userdata['ip_address'], 'user_id' => '0', 'operate_count' => '1', 'login_count' => '1', 'login_wrong_count' => '1', 'register_count' => '0');
				break;
			case '2'://用户名不存在
				$info = array('primary_key' => $primary_key, 'ip_address' => $userdata['ip_address'], 'user_id' => '0', 'operate_count' => '1', 'login_count' => '1', 'login_wrong_count' => '1', 'register_count' => '0');
				break;
			default:
				break;
			}
			$this->db->insert("operate_count",$info);
		}
		else
		{
			switch($tag)
			{
			case '1'://登录信息正确
				$info = array('primary_key' => $R->result()[0]->primary_key, 'ip_address' => $userdata['ip_address'], 'user_id' => '0', 'operate_count' => $R->result()[0]->operate_count+1, 'login_count' => $R->result()[0]->login_count+1, 'login_wrong_count' => $R->result()[0]->login_wrong_count, 'register_count' => '0');
				break;
			case '0'://密码错误
				$info = array('primary_key' => $R->result()[0]->primary_key, 'ip_address' => $userdata['ip_address'], 'user_id' => '0', 'operate_count' => $R->result()[0]->operate_count+1, 'login_count' => $R->result()[0]->login_count+1, 'login_wrong_count' => $R->result()[0]->login_wrong_count+1, 'register_count' => '0');
				break;
			case '2'://用户名不存在
				$info = array('primary_key' => $R->result()[0]->primary_key, 'ip_address' => $userdata['ip_address'], 'user_id' => '0', 'operate_count' => $R->result()[0]->operate_count+1, 'login_count' => $R->result()[0]->login_count+1, 'login_wrong_count' => $R->result()[0]->login_wrong_count+1, 'register_count' => '0');
				break;
			default:
				break;
			}
			$this->db->where("ip_address", $userdata['ip_address']);
			$this->db->update("operate_count", $info);
		}
	}
}
?>
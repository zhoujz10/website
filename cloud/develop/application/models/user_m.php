<?php

class User_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

	function count_user() {
		//返回user数
		return $this->db->count_all('user');
	}

    function insert($info) {
		//插入新用户信息
		$this->db->insert("user", $info);
    }

    function update($user_id, $data) {
		//修改用户信息
		$this->db->where("user_id", $user_id);
		$this->db->update("user", $data);
    }

	function query_user_by_mail($mail) {
		//通过邮箱查询用户信息
		$this->db->where("user_mail", $mail);
		$this->db->select("*");
		$R = $this->db->get("user");
		return $R->result();
	}

    function query_user_by_student_id($student_id) {
		//通过student_id查询用户信息
		$this->db->where("student_id", $student_id);
		$this->db->select("*");
		$R = $this->db->get("user");
		return $R->result();
	}
    
	function query_user_by_id($user_id) {
		//通过user_id查询用户信息
		$this->db->where("user_id", $user_id);
		$this->db->select("*");
		$R = $this->db->get("user");
		return $R->result();
	}

}
?>

<?php

class Record_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

	function count_num() {
		return $this->db->count_all('login_record');
	}

	function insert($user_id,$ip_address) {
		//该函数记录用户每次的登陆信息
		$primary_key = $this->count_num() +1;
		$info = array('primary_key' => $primary_key, 'user_id' => $user_id, 'ip_address' => $ip_address);
		$this->db->insert('login_record',$info);
	}



}
?>
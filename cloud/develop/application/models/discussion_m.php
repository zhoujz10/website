<?php

class Discussion_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function get_discussion($course_primary_key,$last_primary_key,$max_discussion_number) {
		if ($last_primary_key == '0')
			$sql = "SELECT * FROM discussion WHERE course_key = " . $course_primary_key . " ORDER BY primary_key DESC" . " limit 0," . $max_discussion_number;
		else
			$sql = "SELECT * FROM discussion WHERE course_key = " . $course_primary_key . " AND primary_key < " . $last_primary_key . " ORDER BY primary_key DESC" . " limit 0," . $max_discussion_number;
		$R = $this->db->query($sql);
		return $R->result();
    }
	
    function get_discussion_reply($course_primary_key,$discussion_list) {
        $num = count($discussion_list);
        $sql = "SELECT * FROM discussion_reply WHERE course_key = " . $course_primary_key;
        if($num != 0)
        {
            $sql = $sql . " AND discussion_key in (";
            $sql = $sql . $discussion_list[0]->primary_key;
            for($i = 1; $i < $num; $i++)
            {
                $sql = $sql . "," . $discussion_list[$i]->primary_key;
            }
            $sql = $sql . ")";
			$sql = $sql . " ORDER BY time";
			$R = $this->db->query($sql);
			return $R->result();
		}
		else
		{
			return array();
		}
    }

    function insert_new_discussion($user_id, $user_name, $course_primary_key, $content){
		$R = $this->db->query("SELECT count(*) AS num FROM course WHERE primary_key = " . $course_primary_key);
		if($R->result()[0]->num == 0)
			return 0;
		else
		{
			$primary_key = $this->query_max_discussion_id() + 1;
			$info = array("primary_key" => $primary_key, "user_id" => $user_id, "user_name" => $user_name, "course_key" => $course_primary_key, "content" => $content, "reply" => 0);
			$this->db->insert("discussion", $info);
			return 1;
		}
    }
	
    function insert_new_reply($user_id, $user_name, $primary_key, $content){
		$R = $this->db->query("SELECT * FROM discussion WHERE primary_key = " . $primary_key);
		if(count($R->result()) == 0)
			return array("tag" => 0);
		else
		{
			$reply_primary_key = $this->query_max_reply_id() + 1;
			$info = array("primary_key" => $reply_primary_key, "discussion_key" => $primary_key, "user_id" => $user_id, "user_name" => $user_name, "course_key" => $R->result()[0]->course_key, "content" => $content, "reply" => 0, "like" => 0);
			$this->db->insert("discussion_reply", $info);
			
			$this->db->where("primary_key", $primary_key);
			$info = array("reply" => $R->result()[0]->reply+1);
			$this->db->update("discussion",$info);
			
			$time = $this->db->query("SELECT time FROM discussion_reply WHERE primary_key = " . $reply_primary_key)->result()[0]->time;
			return array("tag" => 1, "reply_primary_key" => $reply_primary_key, "time" => $time);
		}
    }
	
    function insert_new_sub_reply($user_id, $user_name, $primary_key, $content){
		$R = $this->db->query("SELECT * FROM discussion_reply WHERE primary_key = " . $primary_key);
		if(count($R->result()) == 0)
			return array("tag" => 0);
		else
		{
			$reply_primary_key = $this->query_max_reply_id() + 1;
			$info = array("primary_key" => $reply_primary_key, "discussion_key" => $R->result()[0]->discussion_key, "user_id" => $user_id, "user_name" => $user_name, "course_key" => $R->result()[0]->course_key, "content" => $content, "reply" => 0, "like" => 0);
			$this->db->insert("discussion_reply", $info);
			
			$this->db->where("primary_key", $primary_key);
			$info = array("reply" => $R->result()[0]->reply+1);
			$this->db->update("discussion_reply",$info);
			
			$R_reply = $this->db->query("SELECT * FROM discussion WHERE primary_key = " . $R->result()[0]->discussion_key);
			$this->db->where("primary_key", $R->result()[0]->discussion_key);
			$info = array("reply" => $R_reply->result()[0]->reply+1);
			$this->db->update("discussion",$info);
			
			$time = $this->db->query("SELECT time FROM discussion_reply WHERE primary_key = " . $reply_primary_key)->result()[0]->time;
			return array("tag" => 1, "reply_primary_key" => $reply_primary_key, "time" => $time);
		}
    }
	
    function query_max_discussion_id(){
        $R = $this->db->query("SELECT MAX(primary_key) AS max_discussion_id FROM discussion");
        return $R->result()[0]->max_discussion_id;
    }
	
    function query_max_reply_id(){
        $R = $this->db->query("SELECT MAX(primary_key) AS max_reply_id FROM discussion_reply");
        return $R->result()[0]->max_reply_id;
    }
	
	function query_discussion($primary_key) {
        $R = $this->db->query("SELECT * FROM discussion WHERE primary_key = " . $primary_key);
        return $R->result();
	}
	
	function query_discussion_reply($primary_key) {
        $R = $this->db->query("SELECT * FROM discussion_reply WHERE primary_key = " . $primary_key);
        return $R->result();
	}
	
	function like($flag, $user_id, $primary_key) {
		$tag = 0;//用于标记是增加赞还是取消赞
		if ($flag == '1')
		{
			$R = $this->db->query("SELECT * from discussion_like where discussion_key = " . $primary_key . " AND user_id = " . $user_id);
			if(count($R->result()) == 0)
			{
				$info = array("primary_key" => $this->db->count_all('discussion_like')+1, "user_id" => $user_id, "discussion_key" => $primary_key, "tag" => 1);
				$this->db->insert('discussion_like',$info);
				$tag = 1;
			}
			else if($R->result()[0]->tag == 0)
			{
				$this->db->where("primary_key", $R->result()[0]->primary_key);
				$info = array("tag" => '1');
				$this->db->update("discussion_like", $info);
				$tag = 1;
			}
			else
			{
				$this->db->where("primary_key", $R->result()[0]->primary_key);
				$info = array("tag" => '0');
				$this->db->update("discussion_like", $info);
			}
			
			if($tag == 1)
			{
				$R = $this->db->query("SELECT * from discussion where primary_key = " . $primary_key);
				$this->db->where("primary_key", $primary_key);
				$info = array("like" => $R->result()[0]->like+1);
				$this->db->update("discussion", $info);
			}
			else
			{
				$R = $this->db->query("SELECT * from discussion where primary_key = " . $primary_key);
				$this->db->where("primary_key", $primary_key);
				$info = array("like" => $R->result()[0]->like-1);
				$this->db->update("discussion", $info);
			}
		}
		else
		{
			$R = $this->db->query("SELECT * from discussion_reply_like where discussion_reply_key = " . $primary_key . " AND user_id = " . $user_id);
			if(count($R->result()) == 0)
			{
				$info = array("primary_key" => $this->db->count_all('discussion_reply_like')+1, "user_id" => $user_id, "discussion_reply_key" => $primary_key, "tag" => 1);
				$this->db->insert('discussion_reply_like',$info);
				$tag = 1;
			}
			else if($R->result()[0]->tag == 0)
			{
				$this->db->where("primary_key", $R->result()[0]->primary_key);
				$info = array("tag" => '1');
				$this->db->update("discussion_reply_like", $info);
				$tag = 1;
			}
			else
			{
				$this->db->where("primary_key", $R->result()[0]->primary_key);
				$info = array("tag" => '0');
				$this->db->update("discussion_reply_like", $info);
			}
			
			if($tag == 1)
			{
				$R = $this->db->query("SELECT * from discussion_reply where primary_key = " . $primary_key);
				$this->db->where("primary_key", $primary_key);
				$info = array("like" => $R->result()[0]->like+1);
				$this->db->update("discussion_reply", $info);
			}
			else
			{
				$R = $this->db->query("SELECT * from discussion_reply where primary_key = " . $primary_key);
				$this->db->where("primary_key", $primary_key);
				$info = array("like" => $R->result()[0]->like-1);
				$this->db->update("discussion_reply", $info);
			}
		}
		return $tag;
	}
 }


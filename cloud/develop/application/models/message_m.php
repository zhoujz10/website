<?php

class Message_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

	function get_message($user_id) {
		//获取某位用户的未读message
		$sql = "SELECT * FROM message WHERE user_id = '" . $user_id . "' AND already_read = '0' ORDER BY primary_key DESC";
		$R = $this->db->query($sql);
		return $R->result();
	}

	function insert_new_discussion_reply_message($course_key, $course_name, $leaver_user_id, $leaver_user_name, $discussion_primary_key, $user_id, $content) {
		$primary_key = $this->db->count_all('message') + 1;
		$info = array("primary_key" => $primary_key, "user_id" => $user_id, "leaver_user_id" => $leaver_user_id, "leaver_user_name" => $leaver_user_name, "course_key" => $course_key, "course_name" => $course_name, "message_type" => 0, "blog_id" => 0, "discussion_id" => $discussion_primary_key, "reply_id" => 0, "content" => $content, "already_read" => 0);
		$this->db->insert("message", $info);
	}
	
	function insert_new_study_experience_reply_message($course_key, $course_name, $leaver_user_id, $leaver_user_name, $study_experience_primary_key, $user_id, $content)
	{
		$primary_key = $this->db->count_all('message') + 1;
		$info = array("primary_key" => $primary_key, "user_id" => $user_id, "leaver_user_id" => $leaver_user_id, "leaver_user_name" => $leaver_user_name, "course_key" => $course_key, "course_name" => $course_name, "message_type" => 1, "blog_id" => $study_experience_primary_key, "discussion_id" => 0, "reply_id" => 0, "content" => $content, "already_read" => 0);
		$this->db->insert("message", $info);
	}

	function count_message() {
		return $this->db->count_all('message');
	}

	function set_message_read($user_id, $message_id_array) {
		//将一组message设为已读
		$sql = "SELECT * FROM message WHERE user_id = '" . $user_id . "' AND already_read = '0'";
		$result = $this->db->query($sql)->result();
		for($i = 0; $i < count($result); $i++)
		{
			$flag = 0;
			for($j = 0; $j < count($message_id_array); $j++)
			{
				if($message_id_array[$j] == $result[$i]->primary_key)
				{
					$flag = 1;
					break;
				}
			}
			if($flag == 1)
			{
				$this->db->where("primary_key", $result[$i]->primary_key);
				$this->db->update("message",array('already_read' => '1'));
			}
		}
	}
}
?>
<?php
//处理与course_care的数据表有关的操作
class Care_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

	function query_course_tag($user_id,$course_key) {
		//查询用户是否已经关注某一门课程
		$sql = "SELECT * FROM course_care WHERE user_id = ? AND course_key = ?";
		$R = $this->db->query($sql, array($user_id,$course_key));
		$r = $R->result();
		$num = count($r);
		if($num == 0 || $r[0] -> tag == 0)
			return 0;
		else
			return 1;
	}

	function querymycourse($user_id) {
		//查询用户关注的所有课程
		$sql = "SELECT * FROM course_care WHERE user_id = ? AND tag = ?";
		$R = $this->db->query($sql, array($user_id,'1'));
		return $R->result();
	}

	function change_course_care($user_id,$course_key) {
		//更改用户对某一课程的关注与否
		$sql = "SELECT * FROM course_care WHERE user_id = ? AND course_key = ?";
		$R1 = $this->db->query($sql, array($user_id,$course_key));
		$r1 = $R1->result();
		$num = count($r1);
		if($num == 0)
		{
			$sql = "SELECT * FROM course WHERE primary_key = ?";
			$R2 = $this->db->query($sql, array($course_key));
			$r2 = $R2->result();
			$info = array("primary_key" => $this->db->count_all('course_care')+1, "user_id" => $user_id, "course_key" => $course_key, "course_id" => $r2[0]->course_id, "sub_id" => $r2[0]->sub_id, "school" => $r2[0]->school, "department" => $r2[0]->department, "course_name" => $r2[0]->course_name, "credit" => $r2[0]->credit, "teacher_id" => $r2[0]->teacher_id, "teacher_name" => $r2[0]->teacher_name, "course_time" => $r2[0]->course_time, "course_feature" => $r2[0]->course_feature, "tag" => '1');
			$this->db->insert("course_care", $info);
			return 1;
		}
		else if($r1[0]->tag == 0)
		{
			$sql = "SELECT * FROM course_care WHERE user_id = ? AND course_key = ?";
			$R = $this->db->query($sql, array($user_id,$course_key));
			$r = $R->result()[0]->primary_key;
			$this->db->where("primary_key", $r);
			$info = array("tag" => '1');
			$this->db->update("course_care", $info);
			return 1;
		}
		else
		{
			$sql = "SELECT * FROM course_care WHERE user_id = ? AND course_key = ?";
			$R = $this->db->query($sql, array($user_id,$course_key));
			$r = $R->result()[0]->primary_key;
			$this->db->where("primary_key", $r);
			$info = array("tag" => '0');
			$this->db->update("course_care", $info);
			return 0;
		}
	}
	
	function cancel_course_care($user_id, $course_care_key_array) {
		//取消对一组课程的关注
		$sql = "SELECT * FROM course_care WHERE user_id = '" . $user_id . "' AND tag = '1'";
		$result = $this->db->query($sql)->result();
		for($i = 0; $i < count($result); $i++)
		{
			$flag = 0;
			for($j = 0; $j < count($course_care_key_array); $j++)
			{
				if($course_care_key_array[$j] == $result[$i]->primary_key)
				{
					$flag = 1;
					break;
				}
			}
			if($flag == 1)
			{
				$this->db->where("primary_key", $result[$i]->primary_key);
				$this->db->update("course_care",array('tag' => '0'));
			}
		}
	}
	
}
?>
﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" type="text/css" href="/css/index.css">
        <link rel="stylesheet" type="text/css" href="/css/zuiguanzhu.css">
        <link rel="stylesheet" type="text/css" href="/css/gerenxinxi.css">
        <!--复选框专用-->
        <style>
            label { 
				display: block; 
				cursor: pointer; 
				line-height: 19px; 
				margin-bottom: 10px; 
				text-shadow: 0 -1px 0 rgba(0,0,0,.2); 
			}
			
			.label_check input{ 
				margin-right: 5px; 
			}
			
			.has-js .label_check{ 
				padding-left: 34px; 
			}
						
			.has-js .label_check{ 
				background: url("/img/checkbox-bg.png") no-repeat; 
			}
			
			.has-js .label_check { 
				background-position: 0 0px;
			}
			.has-js label.c_on { 
				background-position: 0 -35px;
			}
			
			.has-js .label_check input{ 
				position: absolute; 
				left: -9999px; 
			} 
        </style>
		
		<link rel="shortcut icon" href="/img/icon.gif" >
		<title>个人信息_最学术网站</title>
		<!--PHP变量赋值给js-->
        <script type = "text/javascript">
			<?php
				if (isset($user_id))
				{
					echo "var user_id = '" . $user_id ."';\n";
				}
			?>
		</script>
		<!--页面初始化-->
		<script type = "text/javascript">
			$(document).ready(function(){
				$.get('/user/get_user_info/'+ Math.random(),function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							$('#denglu').html(result.user_name);
							$('#zhuce').html("退出");
							$('#user_name').html(result.user_name);
							$('#name-content').html(result.user_name);
							$('#id-content').html(result.student_id);
							$('#email-content').html(result.user_mail);
							$('#dept-content').html(result.department);
							$('#class-content').html(result.class);
							$('#birth-content').html(result.birthday);
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
				});
				
				$.fn.init_my_care();
				
				$.fn.init_my_message();
				
				$(document).on('click', 'div[name="course-i"]', function(){
					$.get('/course/get_course_id/', {course_primary_key:$(this).attr("id").replace(/[^\d]/g,"")}, function(result){
						result = eval("("+result+")");
						if (result.status != null)
						{
							if (result.status)
							{
								location.href = "/course/id/" + result.course_id + "/" + result.sub_id;
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
			
				});
				$(document).on('click', 'div[name="care_course_name"]', function(){
					location.href = "/course/id/" + $('#care_course_id-' + $(this).attr("id").replace(/[^\d]/g,"")).val() + "/" + $('#care_sub_id-' + $(this).attr("id").replace(/[^\d]/g,"")).val();
				});
				
				
				//全选框
				$('#checkbox-10').click(function(){
					obj = document.getElementsByName('sample-checkbox-1');
					for(var i=0; i<obj.length; i++){
						obj[i].checked = $('#checkbox-10').is(':checked');
					}
				});
				/*$('#checkbox-20').click(function(){
					//alert(this.innerHTML);
					obj = document.getElementsByName('sample-checkbox-2');
					for(var i=0; i<obj.length; i++){
						obj[i].checked = $('#checkbox-20').is(':checked');
					}
				});*/
			});
			//载入用户信息及退出函数
			function uInfoShow()
			{
				location.href = '/user/' + user_id;
			}
			function uLogout()
			{
				$.get('/login/logout/'+ Math.random(),function(result){
					location.href = '/';
				});
			}	
		</script>
		
		<!--我的关注初始化-->
		<script type = "text/javascript">
			$.fn.init_my_care = function(callback)
			{
				$.get('/user/get_my_course/' + Math.random(), function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							var i = 0;
							var len = result.course_data.length;
							var tabletemp = '';
							for (i = 0; i < len; i++)
							{
								if (i%2 == 0)
									tabletemp +='<div class="row cxresult-content-gray">\n';
								else
									tabletemp +='<div class="row cxresult-content-white">\n';
								tabletemp +=    '	<div class="col-md-2  padding-0">\n';
								tabletemp +=    '		<div style="margin-left:20px;"class="padding-0">\n';
								tabletemp +=    '			<div class="cxresult-frame" style="padding:0;">\n';
								tabletemp +=    '				<label for="checkbox-1' + (i+1) + '" class="label_check wgzhd" style= "display:none">\n';
								tabletemp +=    '					<input type="checkbox" value="1" id="checkbox-1' + (i+1) + '" name="sample-checkbox-1" /><p>&nbsp;</p>\n';
								tabletemp +=    '                	<input type="hidden"  value="' + result.course_data[i].primary_key + '" id="pk-1' + (i+1) + '"\n';
								tabletemp +=    '				</label>\n';
								tabletemp +=    '			</div>\n';
								tabletemp +=    '			<div style="margin-left:20px;"class="display-in-bl">' + result.course_data[i].department + '</div>\n';
								tabletemp +=    '		</div>\n';
								tabletemp +=    '	</div>\n';
								tabletemp +=    '	<div  class="col-md-1  padding-0">0020041</div>\n';
								tabletemp +=    '	<div class="col-md-1  padding-0">90</div>\n';
								tabletemp +=    '	<div style="padding:0 10px 0 0;cursor:pointer;"  id="care_course_name-' + result.course_data[i].primary_key + '"class="col-md-2 blue-text" name="care_course_name">' + result.course_data[i].course_name + '</div>\n';
								tabletemp +=	'   <input type="hidden"  value="' + result.course_data[i].course_id + '" id="care_course_id-' + result.course_data[i].primary_key + '" />\n';
								tabletemp +=	'   <input type="hidden"  value="' + result.course_data[i].sub_id + '" id="care_sub_id-' + result.course_data[i].primary_key + '" />\n';
								tabletemp +=    '	<div class="col-md-1  padding-0">' + result.course_data[i].credit + '</div>\n';
								tabletemp +=    '	<div class="col-md-1  padding-0">' + result.course_data[i].teacher_name + '</div>\n';
								tabletemp +=    '	<div class="col-md-2  padding-0">' + result.course_data[i].course_time + '</div>\n';
								tabletemp +=    '	<div class="col-md-2  padding-0">' + result.course_data[i].course_feature + '</div>\n';
								tabletemp +=    '</div>\n';
							}
							//$(tabletemp).insertAfter('#woguanzhude-head');
							$('#woguanzhude-body').html(tabletemp);
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
					if (callback)
					{
						callback();
					}	
				});			
			}
		</script>
		
		<!--我的消息初始化-->
		<script type = "text/javascript">
			$.fn.init_my_message = function (callback)
			{
				$.get('/user/get_my_message/'+ Math.random(),function(result){
					result = eval("("+result+")");
					if (result.status != null)
					{
						if (result.status)
						{
							var i = 0;
							var len = result.message_data.length;
							var infotemp = '';
							for (i = 0; i < len; i++)
							{
								if (i == 0)
								{
									infotemp += '<!--白底儿-->\n';
									infotemp += '<div style="margin-bottom:15px;"class="row chaxun-titlefont">\n';
									infotemp += '    <div style="margin-left:20px;"class="padding-0">\n';
									infotemp += '        <div class="cxresult-frame" style="padding:0;">\n';
									infotemp += '            <label for="checkbox-2' + (i+1) + '" class="label_check wdxx" style= "display:none" >\n';
									infotemp += '                <input type="checkbox"  value="1" id="checkbox-2' + (i+1) + '" name="sample-checkbox-2" /><p>&nbsp;</p>\n';
									infotemp += '                <input type="hidden"  value="' + result.message_data[i].primary_key + '" id="pk-2' + (i+1) + '"/>\n';
									infotemp += '            </label>\n';
									infotemp += '        </div>\n';
									infotemp += '        <div style="margin-left:20px;cursor:pointer;"class="display-i blue-text">' + result.message_data[i].leaver_user_name + '</div>\n';
									infotemp += '        <div class="display-i" style="margin-left:-5px;">在</div>\n';
									infotemp += '        <div class="display-i blue-text" style="margin-left:-5px;cursor:pointer;" name="course-i" id="course_pk-' + result.message_data[i].course_key + '">' + result.message_data[i].course_name + '</div>\n';
									infotemp += '        <div class="display-i" style="margin-left:-5px;">课程的</div>\n';
									infotemp += '        <div class="display-i" style="margin-left:-5px;">' + (result.message_data[i].message_type == '0'? '新鲜事' : '日志') + '</div>\n';
									infotemp += '        <div class="display-i blue-text" style="margin-left:-5px;cursor:pointer;" name="content-i">' + (result.message_data[i].content) + '</div>\n';
									infotemp += '        <div class="display-i" style="margin-left:-5px;">中回复了我</div>\n';
									infotemp += '    </div>\n';
									infotemp += '</div>\n';
								}
								else if (i%2 == 1)
								{
									infotemp += '<!--灰底儿-->\n';
									infotemp += '    <div class="row cxresult-content-gray">\n';
									infotemp += '        <div style="margin-left:20px;"class="padding-0">\n';
									infotemp += '            <div class="cxresult-frame" style="padding:0;">\n';
									infotemp += '                <label for="checkbox-2' + (i+1) + '" class="label_check wdxx" style= "display:none" >\n';
									infotemp += '                    <input type="checkbox"  value="1" id="checkbox-2' + (i+1) + '" name="sample-checkbox-2" /><p>&nbsp;</p>\n';
									infotemp += '                	 <input type="hidden"  value="' + result.message_data[i].primary_key + '" id="pk-2' + (i+1) + '"/>\n';
									infotemp += '                </label>\n';
									infotemp += '            </div>\n';
									infotemp += '            <div style="margin-left:20px;cursor:pointer;"class="display-i blue-text">' + result.message_data[i].leaver_user_name + '</div>\n';
									infotemp += '            <div class="display-i" style="margin-left:-5px;">在</div>\n';
									infotemp += '        <div class="display-i blue-text" style="margin-left:-5px;cursor:pointer;" name="course-i" id="course_pk-' + result.message_data[i].course_key + '">' + result.message_data[i].course_name + '</div>\n';
									infotemp += '            <div class="display-i" style="margin-left:-5px;">课程的</div>\n';
									infotemp += '            <div class="display-i" style="margin-left:-5px;">' + (result.message_data[i].message_type == '0'? '新鲜事' : '日志') + '</div>\n';
									infotemp += '            <div class="display-i blue-text" style="margin-left:-5px;cursor:pointer;">' + (result.message_data[i].content) + '</div>\n';
									infotemp += '            <div class="display-i" style="margin-left:-5px;">中回复了我</div>\n';
									infotemp += '        </div>\n';
									infotemp += '   </div>\n';
								}
								else
								{
									infotemp += '<!--白底儿-->\n';
									infotemp += '    <div class="row cxresult-content-white">\n';
									infotemp += '        <div style="margin-left:20px;"class="padding-0">\n';
									infotemp += '            <div class="cxresult-frame" style="padding:0;">\n';
									infotemp += '                <label for="checkbox-2' + (i+1) + '" class="label_check wdxx" style= "display:none" >\n';
									infotemp += '                    <input type="checkbox"  value="1" id="checkbox-2' + (i+1) + '" name="sample-checkbox-2" /><p>&nbsp;</p>\n';
									infotemp += '                	 <input type="hidden"  value="' + result.message_data[i].primary_key + '" id="pk-2' + (i+1) + '"/>\n';
									infotemp += '                </label>\n';
									infotemp += '            </div>\n';
									infotemp += '            <div style="margin-left:20px;cursor:pointer;"class="display-i blue-text">' + result.message_data[i].leaver_user_name + '</div>\n';
									infotemp += '            <div class="display-i" style="margin-left:-5px;">在</div>\n';
									infotemp += '        <div class="display-i blue-text" style="margin-left:-5px;cursor:pointer;" name="course-i" id="course_pk-' + result.message_data[i].course_key + '">' + result.message_data[i].course_name + '</div>\n';
									infotemp += '            <div class="display-i" style="margin-left:-5px;">课程的</div>\n';
									infotemp += '            <div class="display-i" style="margin-left:-5px;">' + (result.message_data[i].message_type == '0'? '新鲜事' : '日志') + '</div>\n';
									infotemp += '            <div class="display-i blue-text" style="margin-left:-5px;cursor:pointer;">' + (result.message_data[i].content) + '</div>\n';
									infotemp += '            <div class="display-i" style="margin-left:-5px;">中回复了我</div>\n';
									infotemp += '        </div>\n';
									infotemp += '   </div>\n';
								}
							}
							$('#wodexiaoxi-page').html(infotemp);
						}
						else
						{
							alert(result.failDesc);
						}
					}
					else
					{
						alert(result.failDesc);
						location.href = result.url;
					}
					if (callback)
					{
						callback();
					}
				});
			}
		</script>
		<!--修改密码-->
		<script type = "text/javascript">
			$(document).ready(function(){
				$('#cpw-fragment-save').click(function(){
					$.post("/user/change_user_password", {orig_password: $('#orig_password').val(), new_password: $('#new_password').val(), new_password_repeat: $('#new_password_repeat').val() } ,function(result){
						result = eval("("+result+")");
						if(result.status != null)
						{
							if(result.status == true)
							{
								uCPWdisplay();
								$('#change_succ').fadeIn(3000);
								setTimeout("$('#change_succ').fadeOut(3000)", 5000);
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			});
		</script>
		
		<!--消息标记为已读-->
		<script type = "text/javascript">
			$(document).ready(function(){
				$('#info-read').click(function(){
					var i = 0;
					var j = 0;
					var data = {};
					while (true)
					{
						if ($('#checkbox-2' + (i+1)).length == 0)
							break;
						if ($('#checkbox-2' + (i+1)).is(':checked'))
						{
							data['del' + j] = $('#pk-2' + (i+1)).val();
							j = j + 1;
						}
						i = i + 1;
					}
					$.post("/user/set_message_read", data, function(result){
						result = eval("("+result+")");
						if(result.status != null)
						{
							if(result.status == true)
							{
								$("#cc").slideUp($.fn.init_my_message());
								$(".wdxx").attr("style", "display:none");
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			});
		</script>

		<!--取消课程关注-->
		<script type = "text/javascript">
			$(document).ready(function(){
				$('#info-delete').click(function(){
					var i = 0;
					var j = 0;
					var data = {};
					while (true)
					{
						if ($('#checkbox-1' + (i+1)).length == 0)
							break;
						if ($('#checkbox-1' + (i+1)).is(':checked'))
						{
							data['cancel' + j] = $('#pk-1' + (i+1)).val();
							j = j + 1;
						}
						i = i + 1;
					}
					$.post("/user/cancel_course_care", data, function(result){
						result = eval("("+result+")");
						if(result.status != null)
						{
							if(result.status == true)
							{
                                $("#bb").slideUp($.fn.init_my_care());
                                $(".wgzhd").attr("style", "display:none");
							}
							else
							{
								alert(result.failDesc);
							}
						}
						else
						{
							alert(result.failDesc);
							location.href = result.url;
						}
					});
				});
			});
		</script>
    </head>
 

    <body class="back-light text-font">
        <div>
            <div class="container padding-t-10">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <div align="center"><img src="/img/logo-shadow.png"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row size-guidebar guidebar-backg">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="padding:0;">
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r">
                    <a id="guidebar-1" href="#zuixueshu" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-1" class="guide-change guidebar-tab-content guidebar-tab-rad" 
                    >最学术</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-2" href="#zuikecheng" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-2" class="guide-change guidebar-tab-content">最课程</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-3" href="#zuihuodong" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-3" class="guide-change guidebar-tab-content">最活动</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-4" href="#zuishetuan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-4" class="guide-change guidebar-tab-content">最社团</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-5" href="#zuishenghuo" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-5" class="guide-change guidebar-tab-content">最生活</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab guidebar-tab-border-r" style="margin-left:-4px;" >
                    <a id="guidebar-6" href="#zuikeyan" class="guidebar-word" onclick="javascript:guidebar_choose(this);"><div id="guidebar-content-6" class="guide-change guidebar-tab-content">最科研</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab" style="margin-left:-4px;" >
                    <a id="guidebar-7" href="#zuiqiuzhi" class="guidebar-word" onclick="javascript:guidebar_choose(this);"> <div id="guidebar-content-7" class="guide-change guidebar-tab-content">最求职</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab pull-right" style="margin-left:-5px;" >
                    <a href="#about_us" class="guidebar-word"><div  id="guanyuwomen" class="size-guidebart guidebar-tab-content ">关于我们</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a id="signup_link" href="#signup_link" class="guidebar-word" onclick="javascript:uLogout();"><div id="zhuce" class="size-guidebart guidebar-tab-2-content">退出</div></a>
                </div>
                <div class=" size-guidebar guidebar-tab-2  guidebar-tab-border-r  pull-right" style="margin-left:-5px;" >
                    <a  id="login_link"  href="#login_link" onclick="javascript:uInfoShow();" class="guidebar-word"><div id="denglu" class="size-guidebart guidebar-tab-2-content" style="color:white; background-color:#69b7d4;-webkit-box-shadow: 1px 3px 3px;-moz-box-shadow: 1px 3px 3px; box-shadow: 0px 3px 3px -1px #2a6a81;font-size:17px;margin-left:9px;"></div></a>
                </div>
                
                <div class="search-bar pull-right" style="margin-top:8px;">
                        <input id="Text1" class="pull-left search-input text-click" placeholder="search for something :)">
                        <div class="pull-right size-search-button"></div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        
              
        <!--以下为主题内容-->
        <div class="container">            
            <div id="main">
                
                <!--个人信息-->
                <div style="padding:10px 16px 10px 10px; position:relative;">
                    <s class="info-bubble-backg"></s>
                    <s class="info-bubble-shelter"></s>
                    <div class="row ">                        
                        <div class="col-md-3">
                            <div class="info-head-frame">
                                <img class="info-img-head" src="/img/head1.jpg">
                                <div class="info-name" id="user_name"></div>
                                <div class="info-school">就读于清华大学</div>
                            </div>
                        </div>
                        
                        <div class="col-md-9 row info-right" id="personal-info-content">
                            <div class="row" style="margin-top:15px;margin-bottom:15px;">
                                <div class="col-md-6" style="border-right:3px solid #f0f2f5;" >
                                    <div style="margin-bottom:10px;margin-left:40px;">
                                        <div class="info-middle1">邮箱</div>
                                        <div class="display-i-b">
                                        	<div id="email-content" class="info-middle2"></div>
                                        </div>
                                    </div>
                                    <div style="margin-bottom:10px;margin-left:40px;">
                                        <div class="info-middle1">姓名</div>
                                        <div class="pi-content display-i-b">
                                    	    <div id="name-content" class="info-middle2"></div>
                                        </div>
                                    </div>
                                    <div style="margin-bottom:10px;margin-left:40px;">
                                        <div class="info-middle1">学号</div>
                                        <div class="pi-content display-i-b">
                                        	<div id="id-content" class="info-middle2"></div>
                                        </div>
                                    </div>                       
                                </div>
                                <div class="col-md-6">
                                    <div style="margin-bottom:10px;margin-left:40px;">
                                        <div class="info-middle1">系别</div>
                                        <div class="pi-content  display-i-b">
                                    	    <div id="dept-content" class="info-middle2"></div>
                                        </div>
                                    </div>
                                    <div style="margin-bottom:10px;margin-left:40px;">
                                        <div class="info-middle1">班级</div>
                                        <div class="pi-content  display-i-b">
                                        	<div id="class-content" class="info-middle2"></div>
                                        </div>
                                    </div>
                                    <div style="margin-bottom:10px;margin-left:40px;">
                                        <div class="info-middle1">生日</div>
                                        <div class="pi-content display-i-b">
                                            <div id="birth-content" class="info-middle2"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                               
                                <script type="text/javascript">
                                    //将信息变为编辑模式
                                    var is_editing = false;
                                    function edit_personal_info() {
                                        if (is_editing) { return; }
                                        $("#personal-info-content .pi-content").each(function () { 
											$(this).html("<input type='text' class='info-middle2 info-middle2-input' name='" + $(this).find("div")[0].id + "' id='" + $(this).find("div")[0].id + "' value='" + $(this).find("div").html() + "'/>") });
										//$("#birth-content").attr("type", "date");
                                        is_editing = true;
                                        $("#pi-save").attr('href', '#pi-save');
                                        $("#pi-edit").removeAttr('href');
                                        $("#pi-edit").attr("class", "info-edit  info-edit-gray");
                                        $("#pi-save").attr("class", "info-save");

                                    }
                                    function save_personal_info() {
                                        if (!is_editing) { return; }
										$.post("/user/save_user_info", {user_name: $('#name-content').val(), student_id: $('#id-content').val(), department: $('#dept-content').val(), class: $('#class-content').val(), birthday: $('#birth-content').val() } ,function(result){
											result = eval("("+result+")");
											if(result.status != null)
											{
												if(result.status == true)
												{
													$("#personal-info-content .pi-content").each(function () { $(this).html("<div class='info-middle2' id='" + $(this).find("input")[0].id + "'>" + $(this).find("input").val() + "</div>") });
													is_editing = false;
													$("#pi-edit").attr('href', '#pi-edit');
													$("#pi-save").removeAttr('href');
													$("#pi-edit").attr("class", "info-edit");
													$("#pi-save").attr("class", "info-save info-save-gray");
													$('#save_succ').fadeIn(3000);
													setTimeout("$('#save_succ').fadeOut(3000)", 5000);
												}
												else
												{
													alert(result.failDesc);
												}
											}
											else
											{
												alert(result.failDesc);
												location.href = result.url;
											}
										});
                                    }
                                    		
                                    </script>
                                </div>
                                <div style="height:30px;">
                                    <div>
                                        <input name="search_result"  type="button" class="info-save info-save-gray text-click" style="margin-right:70px;"  id="pi-save" onclick="javascript:save_personal_info();" value="保存">
                                    </div>
                                    <div>
                                        <input name="search_result"  type="button" class="info-edit text-click" style="margin-right:10px;"    id="pi-edit" onclick="javascript:edit_personal_info();" value="编辑">
                                    </div>
                                    <div>
                                        <input name="search_result"  type="button" class="key-save text-click" style="margin-right:10px;"    id="k-edit" onclick="javascript:return uCPWShow();"value="修改密码">
                                    </div> 
                                    <div id="save_succ" style="display:none; float:right; margin-right:80px;"  >
                                        保存成功~~
                                    </div>
									<div id="change_succ" style="display:none; float:right; margin-right:80px;"  >
                                        修改成功~~
                                    </div>
                               </div>
                        </div>                        
                    </div>                 
                </div>
                              
                <!--我关注的-->
                <div class="margin-t-20 ">
                    <div id="zgzcontent-tabs" class="normaltab-grxx">
                            <div id="content-tools" class="pull-right size-w-120 margin-t-10">
                                 <a href="#wgzhd_shezhi" onclick="javascript:delete_edit();" id="wgzhd-edit">
                                      <div id="wgzhd_shezhi" class="xxs_shezhi pull-right" style="margin-right:0px;"></div>
                                 </a>                                      
                            </div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs underline-bold-dblue">
                                <li id="wgzhd-tab" class="active"><a style="color:#5da3bd" href="#woguanzhude-page" onclick="javascript:woguanzhd();"  data-toggle="tab">我关注的</a></li>
                                <li id="wsxx-tab" class=""><a style="color:#f1ae28" href="#wodexiaoxi-page" onclick="javascript:wodexx();"  data-toggle="tab">我的消息</a></li>
                            </ul>
                    </div>
                    <div id="grxx-content-pages" class="back-white maincontent">
                        <!-- Tab panes -->
                        <div class="tab-content">
                                <!-- 底侧栏1：我关注的 -->
                                <div class="tab-pane active" id="woguanzhude-page">
                                    <!--表头-->
                                    <div style="margin-bottom:15px;"class="row chaxun-titlefont" id="woguanzhude-head">
                                        <div class="col-md-2  padding-0">
                                            <div style="margin-left:20px;"class="padding-0">
                                                <div class="cxresult-frame" style="padding:0;">
                                                    <label for="checkbox-10" class="label_check wgzhd" style= "display:none" >
                                                        <input type="checkbox"  value="1" id="checkbox-10" name="sample-checkbox-10" /><p>&nbsp;</p>
			                                        </label>
                                                </div>
                                                <div style="margin-left:20px;"class="display-in-bl">开课院系</div>
                                            </div>
                                        </div>
                                        <div  class="col-md-1  padding-0">课程号</div>
                                        <div class="col-md-1  padding-0">课序号</div>
                                        <div class="col-md-2  padding-0"> 课程名</div>
                                        <div class="col-md-1  padding-0">学分</div>
                                        <div class="col-md-1  padding-0">主讲教师</div>
                                        <div class="col-md-2  padding-0">上课时间</div>
                                        <div class="col-md-2  padding-0">课程特色</div>
                                    </div>
                                    <!--表项-->
									<div id="woguanzhude-body">
									</div>
                                </div>
                                <!--底侧栏2：我的消息-->
                                <div class="tab-pane chaxun-titlefont" id="wodexiaoxi-page">
                                  
                                </div>
                                <div id="bb" style="display:none">
                                    <input name="search_result"  type="button" class="info-delete"  id="info-delete" value="删除">
                                </div>
                                <div id="cc" style="display:none;">
                                    <input name="search_result"  type="button" class="info-read"  id="info-read" value="标记为已读">
                                </div>
                               
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    var info_flag = false;
                    function delete_edit(comment_list_number) {
                        if (!info_flag) {
                            if ($("#bb")[0].style.display == "none") {
                                $("#bb").slideDown();
                                $(".wgzhd").attr("style", "display:block");
                            } else {
                                $("#bb").slideUp();
                                $(".wgzhd").attr("style", "display:none");
                            }
                        }
                        else {
                            if ($("#cc")[0].style.display == "none") {
                                $("#cc").slideDown();
                                $(".wdxx").attr("style", "display:block");
                            } else {
                                $("#cc").slideUp();
                                $(".wdxx").attr("style", "display:none");
                            }
                        }
                    }
                    function wodexx() {
						$.fn.init_my_message(function(){
							info_flag = true;
							if ($("#bb")[0].style.display == "none") { }
							else {
								$("#bb").attr("style", "display:none");
								$(".wgzhd").attr("style", "display:none");
							}
							$(".underline-bold-dblue").attr("style", "border-bottom:3px solid #f1ae28;");
							$("#wgzhd-tab").attr("style", "border-bottom:3px solid #f1ae28;");
							$("#wsxx-tab").attr("style", "border-bottom:3px solid #fff;");
						});
                    }
                    function woguanzhd() {
                        info_flag = false;
                        if ($("#cc")[0].style.display == "none") { }
                        else {
                            $("#cc").attr("style", "display:none");
                            $(".wdxx").attr("style", "display:none");
                        }
                        $(".underline-bold-dblue").attr("style", "border-bottom:3px solid #5da3bd;");
                        $("#wsxx-tab").attr("style", "border-bottom:3px solid #5da3bd;");
                        $("#wgzhd-tab").attr("style", "border-bottom:3px solid #fff;");
                    }                
                </script>
                                
            </div>
            <!--分页：既然有了上面这个加载按钮，这个分页真的在此处没有必要吧-->
             <!--   <div class="margin-t-20 size-h-50">
                    <div class="divpage-bar pull-right">
                        <a href="#page" class="text-white-link"><div class="margin-11 back-blue divpage-number pull-left">&lt;</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">1</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">2</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">3</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">4</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 margin-lr-3 back-grey divpage-number pull-left">10</div></a>
                        <a href="#page" class="text-white-link"><div class="margin-11 back-blue divpage-number pull-left">&gt;</div></a>
                        <div class="divpage-inputdiv pull-left "><input class="divpage-input" /></div>
                        <a href="#page" class="text-white-link"><div class="margin-11 back-blue divpage-number pull-left">Go</div></a>
                    </div>
                </div>-->
                
                <div class="margin-t-20 test">
                    Copyright © 2014 www.zuixueshu.com
                </div>
            
        </div>

        <!--修改密码表单-->
        <div id="CPWcontent" class="abs" style="display: none;">
            <div id="changepw-inner">
                <div id="cpw-head">
                    <div class="login-cancel" onclick="uCPWdisplay();"></div>
                </div>
                <form id="form_login" name="form-login1">
                    <div>
                        <div id="cpw-fragment-oldpw">
                            <input class="cpw-input text-click" id = "orig_password" type="password" />
                        </div>
                        <div id="cpw-fragment-newpw">
                            <input class="cpw-input text-click" id = "new_password" type="password" />
                        </div>
                        <div id="cpw-fragment-conpw">
                            <input class="cpw-input text-click" id = "new_password_repeat" type="password" />
                        </div>
                        <div class="cpw-fragment-blank-s"></div>
                        <div id="cpw-fragment-save"></div>
                    </div>
                </form>
                <div id="cpw-foot">
                </div>
            </div>
        </div>
        <!--登录表单-->
        <div id="logincontent" class="abs" style="display: none;">
            <div id="login-innner">
                <div id="login-head">
                    <div class="login-cancel" onclick="uLogindisplay();"></div>
                </div>
                <form id="form1" name="form-login1">
                    <div>
                        <div class="login-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="login-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="login-fragment-checkcode">
                            <input class="login-input-checkcode text-click" type="text"/>
                            <img class="login-checkcode-pic" src=""/>
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-blank-m autologin" >
                            <input type="checkbox" id="input_autologin"/>
                            下次自动登录
                        </div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-loginbtn" onclick="uLogindisplay();"></div>
                        <div class="login-fragment-blank-s"></div>
                        <div class="login-fragment-tosignup" onclick="uLogindisplay();uRegShow();"></div>
                    </div>
                </form>
                <div id="login-foot">
                </div>
            </div>
        </div>
        <!--注册表单-->
        <div id="signupcontent" class="abs" style="display: none;">
            <div id="signup-innner">
                <div id="signup-head">
                    <div class="login-cancel" onclick="uRegdisplay();"></div>
                </div>
                <form id="form_signup" name="form-signup1">
                    <div>
                        <div class="signup-fragment-email">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-idnum">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-name">
                            <input class="login-input text-click" type="text" />
                        </div>
                        <div class="signup-fragment-passwd">
                            <input class="login-input text-click" type="password" />
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-checkcode">
                        	<input class="login-input-checkcode text-click" type="text"/>
                            <img class="signup-checkcode-pic" src=""/>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-agreement" onclick="open_agreement();">
                        	<div id="signup-agree" class="signup-fragment-needagree" onclick="agreement_toggle();"></div>
                        </div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-signupbtn" onclick="uRegdisplay();"></div>
                        <div class="signup-fragment-blank-s"></div>
                        <div class="signup-fragment-tologin" onclick="uRegdisplay();uLoginShow();"></div>
                    </div>
                </form>
                <div id="signup-foot">
                </div>
            </div>
        </div>		
    </body>
    <script src="/js/index.js"></script>
    <script src="/js/raphael-min.js"></script>
    <script type="text/javascript" src="/js/checkbox.js"></script>
   	
</html>
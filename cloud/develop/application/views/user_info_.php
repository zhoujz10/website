<div>
	<div class="tabbable tabs-left" style="height:400px; width:90%; margin-left:5%">
		<ul class="nav nav-tabs" style="height:400px">
			<li class="active">
				<a href="#tab1" data-toggle="tab">个人信息</a>
			</li>
			<li>
				<a href="#tab2" data-toggle="tab">我的关注</a>
			</li>
			<li>
				<a href="#tab3" data-toggle="tab">账户安全</a>
			</li>
			<li>
				<a href="#tab4" data-toggle="tab">我的留言</a>
			</li>
		</ul>

		<div class="tab-content" style="height:400px">
			<div class="tab-pane fade in active" id="tab1">
				<form action = "/user/save" method="post">
					<div class="infoleft">

						<div class="infowidget">
							<label id = "mail" class="infolabel">邮箱</label>
							<input type="text" name="mailinput" style="margin-top:6px;width:140px" id="mailinput" value="<?php echo $mail; ?>" disabled/>
						</div>

						<div class="infowidget">
							<label id = "mail" class="infolabel">姓名</label>
							<input type="text" name="nameinput" style="margin-top:6px;width:140px" id="mailinput" value="<?php echo $name; ?>"/>
						</div>

						<div style="clear:both">
						</div>

						<div class="infowidget">
							<label id = "mail" class="infolabel">系别</label>
							<input type="text" name="departmentinput" style="margin-top:6px;width:140px" id="mailinput" value="<?php echo $department; ?>"/>
						</div>

						<div class="infowidget">
							<label id = "mail" class="infolabel">班级</label>
							<input type="text" name="classinput" style="margin-top:6px;width:140px" id="mailinput" value="<?php echo $class; ?>"/>
						</div>

						<div style="clear:both">
						</div>

						<div class="infowidget">
							<label id = "mail" class="infolabel">生日</label>
							<div style="margin-top:6px" data-date-format="dd-mm-yyyy" data-date="12-02-2012" id="dp3" class="input-append date" data-provide="datepicker">
								<input type="text" readonly="" value="12-02-2012" size="16" class="span2" />
								<span class="add-on">
									<i class="icon-calendar-3"></i>
								</span>
							</div>
						</div>

					</div>

					<div class="inforight">
						<ul class="thumbnails">
						<li class="span4">
						<img alt="160x120" data-src="holder.js/160x160" style="width: 160px; height: 160px;" src= "<?php echo '/userpic/pic' . $user_id . '.jpg'?>"/>
						</ul>
						<a class="btn btn-primary">浏览</a>
						<a data-toggle="modal" href="#savemyinfo" class="btn btn-primary" style="margin:50px">保存</a>

						<div aria-hidden="true" style="display: none;" id="savemyinfo" class="modal hide fade">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h3 id="myModalLabel">温馨提示</h3>
							</div>

							<div class="modal-body">
								<h4>小心操作</h4>
								<p>当前操作将遗弃过去的用户信息,将无法恢复过去的信息，是否继续操作？</p>
							</div>

							<div class="modal-footer">
								<button class="btn" data-dismiss="modal">取消</button>
								<input type = "submit" class="btn btn-primary" value="保存" />
							</div>
						</div>

					</div>
			 </form>

		</div>

		<div class="tab-pane fade" id="tab2">

			<div id="pivot" class="pivot slide">
				<div class="pivot-headers">
					<a href="#pivot" data-pivot-index="0" class="active" style = "font-size:18px;font-family:Arial;">
						<span class="icon-pencil" aria-hidden="true"></span>
					课程</a>
					<a href="#pivot" data-pivot-index="1" style = "font-size:18px;font-family:Arial;">
						<span class="icon-camera" aria-hidden="true"></span>
					社团</a>
				</div>

				<div class="pivot-items">

					<div id="pivot-item-1" class="pivot-item active">
						<table class="table table-condensed table-hover">
							<thead>
								<tr>
									<th>开课院系</th>
									<th>课程号</th>
									<th>课序号</th>
									<th>课程名</th>
									<th>学分</th>
									<th>主讲教师</th>
									<th>上课时间</th>
									<th>课程特色</th>
								</tr>
							</thead>

							<tbody>
								<?php
									echo "<tr>";

									$num = count($mycourse);

									for($i=0; $i<$num; $i++)
									{
										echo "<td>";
										echo $mycourse[$i]->department;
										echo "</td>";

										echo "<td>";
										echo $mycourse[$i]->course_id;
										echo "</td>";

										echo "<td>";
										echo $mycourse[$i]->sub_id;
										echo "</td>";

										echo "<td><a href = \"/course/id/";
										echo $mycourse[$i]->course_id;
										echo "/";
										echo $mycourse[$i]->sub_id;
										echo "\" style = \"color:black;\">";
										echo $mycourse[$i]->name;
										echo "</a></td>";

										echo "<td>";
										echo $mycourse[$i]->credit;
										echo "</td>";

										echo "<td>";
										echo $mycourse[$i]->teacher;
										echo "</td>";

										echo "<td>";
										echo $mycourse[$i]->time;
										echo "</td>";

										echo "<td>";
										echo $mycourse[$i]->tese;
										echo "</td>";

										echo "</tr>";
									}
								?>
							</tbody>
						</table>

					</div>

					<div id="pivot-item-2" class="pivot-item">
						<table class="table table-condensed table-hover">
							<thead>
								<tr>
									<th>社团类别</th>
									<th>社团名称</th>
									<th>社长</th>
									<th>会员数</th>
								</tr>
							</thead>

							<tbody>
								<?php
									echo "<tr>";

									$num = count($myclub);

									for($i=0; $i<$num; $i++)
									{
										echo "<td>";
										echo $myclub[$i]->leibie;
										echo "</td>";

										echo "<td><a href = \"/club/id/";
										echo $myclub[$i]->club_id;
										echo "\" style = \"color:black;\">";
										echo $myclub[$i]->name;
										echo "</a></td>";

										echo "<td>";
										echo $myclub[$i]->chairman;
										echo "</td>";

										echo "<td>";
										echo $myclub[$i]->number;
										echo "</td>";

										echo "</tr>";
									}
								?>
							</tbody>
						</table>

					</div>

				</div>
			</div>
		</div>

		<div class="tab-pane fade" id="tab3">

			<div class="infowidget">
				<label id = "oldpassword" class="passlabel">原密码</label>
				<input type="password" name="oldpasswordinput" style="margin-top:6px;width:200px" id="oldpasswordinput"/>
			</div>

			<div style="clear:both">
			</div>

			<div class="infowidget">
				<label id = "newpassword" class="passlabel">新密码</label>
				<input type="password" name="newpasswordinput" style="margin-top:6px;width:200px" id="newpasswordinput"/>
			</div>

			<div style="clear:both">
			</div>

			<a data-toggle="modal" href="#myModal" class="btn btn-primary" style="margin-top:30px;margin-left:30px;">保存</a>
			</div>

		
		<div class="tab-pane fade" id="tab4">
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>留言人</th>
						<th>留言内容</th>
						<th>留言时间</th>
					</tr>
				</thead>

				<tbody>
					<?php
						echo "<tr>";

						$num = count($message);

						for($i=0; $i<$num; $i++)
						{
							echo "<td>";
							echo $message[$i]->user_name;
							echo "</td>";

							echo "<td>";
							echo $message[$i]->content;
							echo "</td>";

							echo "<td>";
							echo $message[$i]->time;
							echo "</td>";

							echo "</tr>";
						}
					?>
				</tbody>
			</table>
		</div>

	 </div>
 </div>


</body>
</html>

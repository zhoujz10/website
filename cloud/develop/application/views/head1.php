<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<script type="text/javascript" src="/js/index.js"></script>

   <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
   <link href='http://fonts.googleapis.com/css?family=Reenie+Beanie' rel='stylesheet' type='text/css'> 
   <link rel="stylesheet" type="text/css" href="/css/userdefine/userinfo.css">
   <link rel="stylesheet" type="text/css" href="/css/bootmetro.css">
   <link rel="stylesheet" type="text/css" href="/css/bootmetro-responsive.css">
   <link rel="stylesheet" type="text/css" href="/css/bootmetro-icons.css">
   <link rel="stylesheet" type="text/css" href="/css/bootmetro-ui-light.css">
   <link rel="stylesheet" type="text/css" href="/css/datepicker.css">
   <link rel="stylesheet" type="text/css" href="/css/tablestyle.css">
   <!--  these two css are to use only for documentation 
   <link rel="stylesheet" type="text/css" href="/css/demo.css">-->
   <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
   <script src="/js/modernizr-2.6.2.min.js"></script>

   <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-3182578-6']);
      _gaq.push(['_trackPageview']);
      (function() {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
   </script>

   <script src="//code.jquery.com/jquery-1.10.0.min.js"></script>
   <script>window.jQuery || document.write("<script src='/js/jquery-1.10.0.min.js'>\x3C/script>")</script>

   <script type="text/javascript" src="/js/min/bootstrap.min.js"></script>
   <script type="text/javascript" src="/js/bootmetro-panorama.js"></script>
   <script type="text/javascript" src="/js/bootmetro-pivot.js"></script>
   <script type="text/javascript" src="/js/bootmetro-charms.js"></script>
   <script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>

   <script type="text/javascript" src="/js/jquery.mousewheel.min.js"></script>
   <script type="text/javascript" src="/js/jquery.touchSwipe.min.js"></script>

   <script type="text/javascript" src="/js/holder.js"></script>
   <!--<script type="text/javascript" src="./assets/js/perfect-scrollbar.with-mousewheel.min.js"></script>-->


   <script type="text/javascript">

      $('.panorama').panorama({
         //nicescroll: false,
         showscrollbuttons: true,
         keyboard: true,
         parallax: true
      });

//      $(".panorama").perfectScrollbar();

      $('#pivot').pivot();

   </script>







    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="opensource rich wysiwyg text editor jquery bootstrap execCommand html5" />
    <meta name="description" content="This tiny jQuery Bootstrap WYSIWYG plugin turns any DIV into a HTML5 rich text editor" />
    <link href="/css/external/google-code-prettify/prettify.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script src="/js/external/jquery.hotkeys.js"></script>
    <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
    <script src="/js/external/google-code-prettify/prettify.js"></script>
		<link href="/css/activity_submit.css" rel="stylesheet">
    <script src="/js/bootstrap-wysiwyg.js"></script>







	<title>最学术网站</title>
</head>

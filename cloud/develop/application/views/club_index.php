<div id = "club_index">
	<div id = "all_club" style = "position:absolute;left:100px;top:80px;width:855px;">
		<span id = "ac_text">所有社团</span>
		<div style="margin:0;padding:0; width:850px;height:1px;background-color:#303030;overflow:hidden;position:absolute;top:25px;"></div>
		<div class = "club_class" style = "position:absolute;top:30px;width:170px;">
			<span class = "club_class_text">人文类</span>
			<div style="margin:0;padding:0;width:155px;height:1px;background-color:#303030;overflow:hidden;position:absolute;top:22px;"></div>
			<div>
				<a class = "club_name" title = "清华大学青莲诗社" href = "/club/id/00000001" target="_Blank" style = "position:absolute;left:0px;top:25px;">清华大学青莲诗社</a>
				<a class = "club_name" title = "清华大学海峡两岸交流协会" href = "/club/id/00000002" target="_Blank" style = "position:absolute;left:0px;top:50px;">清华大学海峡两岸...</a>
				<a class = "club_name" title = "清华大学英语翻译协会" href = "/club/id/00000003" target="_Blank" style = "position:absolute;left:0px;top:75px;">清华大学英语翻译...</a>
				<a class = "club_name" title = "清华大学中原发展协会" href = "/club/id/00000004" target="_Blank" style = "position:absolute;left:0px;top:100px;">清华大学中原发展...</a>
				<a class = "club_name" title = "清华大学科幻协会" href = "/club/id/00000005" target="_Blank" style = "position:absolute;left:0px;top:125px;">清华大学科幻协会</a>
				<a class = "club_name" title = "清华大学记者团" href = "/club/id/00000006" target="_Blank" style = "position:absolute;left:0px;top:150px;">清华大学记者团</a>
				<a class = "club_name" title = "清华大学岭南文化交流协会" href = "/club/id/00000007" target="_Blank" style = "position:absolute;left:0px;top:175px;">清华大学岭南文化...</a>
				<a class = "club_name" title = "清华大学国际文化交流协会
" href = "/club/id/00000008" target="_Blank" style = "position:absolute;left:0px;top:200px;">清华大学国际文化...</a>
				<a class = "club_name" title = "清华大学荷风诗社" href = "/club/id/00000009" target="_Blank" style = "position:absolute;left:0px;top:225px;">清华大学荷风诗社</a>
				<a class = "club_name" title = "清华大学理财协会" href = "/club/id/00000010" target="_Blank" style = "position:absolute;left:0px;top:250px;">清华大学理财协会</a>
				<a class = "club_name" title = "清华大学湖湘文化协会" href = "/club/id/00000011" target="_Blank" style = "position:absolute;left:0px;top:275px;">清华大学湖湘文化...</a>
				<a class = "club_name" title = "清华大学推理协会" href = "/club/id/00000012" target="_Blank" style = "position:absolute;left:0px;top:300px;">清华大学推理协会</a>
				<a class = "club_name" title = "清华大学模拟联合国协会" href = "/club/id/00000013" target="_Blank" style = "position:absolute;left:0px;top:325px;">清华大学模拟联合...</a>
				<a class = "club_name" title = "清华大学茶文化协会" href = "/club/id/00000014" target="_Blank" style = "position:absolute;left:0px;top:350px;">清华大学茶文化协会</a>
			</div>
		</div>

		<div class = "club_class" style = "position:absolute;top:30px;left:170px;width:170px;">
			<span class = "club_class_text">体育类</span>
			<div style="margin:0;padding:0;width:155px;height:1px;background-color:#303030;overflow:hidden;position:absolute;top:22px;"></div>
		</div>

		<div class = "club_class" style = "position:absolute;top:30px;left:340px;width:170px;">
			<span class = "club_class_text">艺术类</span>
			<div style="margin:0;padding:0;width:155px;height:1px;background-color:#303030;overflow:hidden;position:absolute;top:22px;"></div>
		</div>

		<div class = "club_class" style = "position:absolute;top:30px;left:510px;width:170px;">
			<span class = "club_class_text">科创类</span>
			<div style="margin:0;padding:0;width:155px;height:1px;background-color:#303030;overflow:hidden;position:absolute;top:22px;"></div>
		</div>

		<div class = "club_class" style = "position:absolute;top:30px;left:680px;width:170px;">
			<span class = "club_class_text">公益类</span>
			<div style="margin:0;padding:0;width:155px;height:1px;background-color:#303030;overflow:hidden;position:absolute;top:22px;"></div>
		</div>
	</div>

	<div style = "position:absolute;left:1000px;top:150px;width:210px;height:320px;background-color:#EEE;">
		<a href = "/poster/view">
			<img src = "/css/poster.jpg" width = "150px;" style = "margin:30px 30px 30px 30px;"/>
		</a>
		<a class="btn btn-primary" href = "/poster/view" style = "position:absolute;left:70px;top:270px;">海报墙</a>
	</div>

	<div id = "my_club">
		<span id = "my_club_text">我关注的</span>
		<div style="margin:0;padding:0;width:1020px;height:1px;background-color:#303030;overflow:hidden;position:absolute;left:0px;top:28px;"></div>
		<div style = "position:absolute;top:30px;width:1020px;">
			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>社团类别</th>
						<th>社团名称</th>
						<th>社长</th>
						<th>会员数</th>
					</tr>
				</thead>

				<tbody>
					<?php
						echo "<tr>";

						$num = count($mycare);

						for($i=0; $i<$num; $i++)
						{
							echo "<td>";
							echo $mycare[$i]->leibie;
							echo "</td>";

							echo "<td><a href = \"/club/id/";
							echo $mycare[$i]->club_id;
							echo "\" style = \"color:black;\">";
							echo $mycare[$i]->name;
							echo "</a></td>";

							echo "<td>";
							echo $mycare[$i]->chairman;
							echo "</td>";

							echo "<td>";
							echo $mycare[$i]->number;
							echo "</td>";

							echo "</tr>";
						}
					?>
				</tbody>
			</table>
		</div>
	</div>

	<div id = "hot_club">
		<span id = "hot_club_text">热门社团</span>
		<div style="margin:0;padding:0;width:1020px;height:1px;background-color:#303030;overflow:hidden;position:absolute;left:0px;top:28px;"></div>
		<div style = "position:absolute;left:80px;top:145px;height:50px;"></div>
	</div>
</div>
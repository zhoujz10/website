<span style = "position:relative;float:left;margin-left:20px;margin-top:2px;font-size:16px;">活动类型</span>
<div style = "margin:0;padding-left:100px;border:0;position:relative;width:1050px;">
	<label class = "activity_class_check" id = "activity_class_checkall"><input name = "activity_class_check" type = "checkbox" />全部</label>
	<label class = "activity_class_check"><input name = "activity_class_check" type = "checkbox" />讲座</label>
	<label class = "activity_class_check"><input name = "activity_class_check" type = "checkbox" />宣讲会</label>
	<label class = "activity_class_check"><input name = "activity_class_check" type = "checkbox" />培训</label>
	<label class = "activity_class_check"><input name = "activity_class_check" type = "checkbox" />节目</label>
	<label class = "activity_class_check"><input name = "activity_class_check" type = "checkbox" />比赛</label>
	<label class = "activity_class_check"><input name = "activity_class_check" type = "checkbox" />招新</label>
	<label class = "activity_class_check"><input name = "activity_class_check" type = "checkbox" />公益</label>
	<label class = "activity_class_check"><input name = "activity_class_check" type = "checkbox" />出游</label>
	<label class = "activity_class_check"><input name = "activity_class_check" type = "checkbox" />其他</label>
</div>

<div style="clear:both"></div>

<span style = "position:relative;float:left;margin-left:20px;margin-top:2px;font-size:16px;">活动时间</span>
<div style = "margin:0;padding-left:100px;border:0;position:relative;width:1050px;">
	<label class = "activity_time_check" id = "activity_time_checkall"><input name = "activity_time_check" type = "checkbox" />全部</label>
	<label class = "activity_time_check"><input name = "activity_time_check" type = "checkbox" />一个月前</label>
	<label class = "activity_time_check"><input name = "activity_time_check" type = "checkbox" />一周前</label>
	<label class = "activity_time_check"><input name = "activity_time_check" type = "checkbox" />最近一周</label>
	<label class = "activity_time_check"><input name = "activity_time_check" type = "checkbox" />今天</label>
	<label class = "activity_time_check"><input name = "activity_time_check" type = "checkbox" />未来一周</label>
	<label class = "activity_time_check"><input name = "activity_time_check" type = "checkbox" />未来一个月</label>
</div>

<div style="clear:both"></div>

<span style = "position:relative;float:left;margin-left:20px;margin-top:2px;font-size:16px;">活动地点</span>
<div style = "margin:0;padding-left:100px;border:0;position:relative;width:1050px;">
	<label class = "activity_spot_check" id = "activity_spot_checkall"><input name = "activity_spot_check" type = "checkbox" />全部</label>
	<label class = "activity_spot_check"><input name = "activity_spot_check" type = "checkbox" />校内</label>
	<label class = "activity_spot_check"><input name = "activity_spot_check" type = "checkbox" />校外</label>
</div>


<a class = "btn" href = "/activity/submit">发表活动</a>
<div id="myFlipview" class="carousel flipview slide" style="float:left; margin-left:5%;width:700px">
	<div class="carousel-inner" >
		<div class="item active">
			<img src="/css/1.jpg">
			<div class="carousel-caption">
				<h4>First picture</h4>
				<p>This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.</p>
			</div>
		</div>

		<div class="item">
			<img src="/css/2.jpg">
			<div class="carousel-caption">
				<h4>Second picture</h4>
				<p>This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.</p>
			</div>
		</div>
		
		<div class="item">
			<img src="/css/3.jpg" >
			<div class="carousel-caption">
				<h4>Third picture</h4>
				<p>This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.This is a picture.</p>
			</div>
		</div>
	</div>

	<a class="carousel-control left" href="#myFlipview" data-slide="prev"></a>
	<a class="carousel-control right" href="#myFlipview" data-slide="next"></a>
</div>

<div class="hero-unit" style="float:left;width:300px;margin-left:20px;height:405px">
	<h1>Hello, world!</h1>
	<p>Description of the website.Description of the website.Description of the website.Description of the website.Description of the website.Description of the website.</p>
	<p><a class="btn btn-primary btn-large">Learn more</a></p>
</div>

<div>
	<div class="tab-content" style="height:400px;margin-left:80px;">
		<div class="tab-pane fade in active" id="tab1">
			<div class="infoleft">

				<div class="infowidget">
					<label id = "mail" class="infolabel">邮箱</label>
					<input type="text" name="mailinput" style="margin-top:6px;width:140px" id="mailinput" value="<?php echo $view_mail; ?>" disabled/>
				</div>

				<div class="infowidget">
					<label id = "mail" class="infolabel">姓名</label>
					<input type="text" name="nameinput" style="margin-top:6px;width:140px" id="mailinput" value="<?php echo $view_name; ?>" disabled/>
				</div>

				<div style="clear:both">
				</div>

				<div class="infowidget">
					<label id = "mail" class="infolabel">系别</label>
					<input type="text" name="departmentinput" style="margin-top:6px;width:140px" id="mailinput" value="<?php echo $view_department; ?>" disabled/>
				</div>

				<div class="infowidget">
					<label id = "mail" class="infolabel">班级</label>
					<input type="text" name="classinput" style="margin-top:6px;width:140px" id="mailinput" value="<?php echo $view_class; ?>" disabled/>
				</div>

				<div style="clear:both">
				</div>


				<div class="infowidget">
					<label id = "mail" class="infolabel">生日</label>
					<div style="margin-top:6px" data-date-format="dd-mm-yyyy" data-date="12-02-2012" id="dp3" class="input-append date" data-provide="datepicker">
						<input type="text" readonly="" value="12-02-2012" size="16" class="span2" />
						<span class="add-on">
							<i class="icon-calendar-3"></i>
						</span>
					</div>
				</div>
				
				<div class="infowidget" style = "position:absolute;left:95px;top:330px;">
					<a data-toggle="modal" href="#leavemessage" class="btn btn-primary">给他留言</a>
				</div>

			</div>

			<div class="inforight">
				<ul class="thumbnails">
				<li class="span4">
				<img alt="160x120" data-src="holder.js/160x160" style="width: 160px; height: 160px;" src= "<?php echo '/userpic/pic' . $view_id . '.jpg'?>"/>
				</ul>
				<span style = "font-size:18px;margin-left:70px;"><?php echo $view_name; ?></span>
			</div>
		</div>
	</div>

	<div aria-hidden="true" style="display:none;width:450px" id="leavemessage" class="modal hide fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		</div>

		<form action = "/user/leavemessage/<?php echo $user_id; ?>/<?php echo $view_id; ?>" method = "post">
			<div class="modal-body">
				<span style="float:left;margin-top:8px;">内容</span>
				<textarea name = "content_input" rows = "8" maxlength = "450" style="float:left;margin-left:10px;margin-top:6px;width:320px;"></textarea>
			</div>

			<div class="modal-footer">
				<button class="btn" data-dismiss="modal">取消</button>
				<input type = "submit" class="btn btn-primary" value="留言" />
			</div>
		</form>
	</div>

 </div>


</body>
</html>
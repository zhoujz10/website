<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Zl extends CI_Controller {
	function index() {
	}

	function care($course_id,$sub_id) {
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);
		
		$this->load->model("course_m");
		$j = $this->course_m->searchid($course_id,$sub_id);
		$name = $j[0]->name;

		$this->load->model("care_m");
		$d = $this->care_m->change_course_care($user_id,$course_id,$sub_id,$name);
		$r = array("r" => $d);

		$this->load->model("user_m");
		$result1 = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result1[0]->name, "mail" => $result1[0]->mail);

		$this->load->model("course_m");
		$c = $this->course_m->searchid($course_id,$sub_id);
		$result = array("result" => $c);

		$u = "Location:/course/material/" . $course_id . "/" . $sub_id;
		header($u);
	}

	function shangchuan($course_id,$sub_id) {
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);

		$this->load->model("zl_m");
		$key = $this->zl_m->count_num();
		$key = $key + 1;

		$config["upload_path"] = './zl/';
		$config['overwrite'] = true;
		$config["allowed_types"] = 'docx|doc|txt|pdf';
		$config['max_size'] = '10000';
		$config['file_name'] = "zl" . $key;
		$this->load->library("upload", $config);

		if ($this->upload->do_upload("fs")) {
			$info = array("key" => $key, "user_id" => $user_id, "course_id" => $course_id, "sub_id" => $sub_id, "name" => $result[0]->name, "title" => $_POST["title_input"], "description" => $_POST["content_input"], "click" => '0', "download" => '0', "time" => date('Y-m-d H:i:s',time()));
			$this->zl_m->insert($info);
			$u = "Location:/course/material/" . $course_id . "/" . $sub_id;
			header($u);
		}
		else {
			echo $this->upload->display_errors('<p>', '</p>');
		}
	}

	function read($key) {
		header("Content-type: application/msword");
		readfile("/zl/zl" . $key . ".docx");
	}


}


?>
<?php

class Upload_files extends CI_Controller {

	function index() {
		$this->load->view("user_info.php");
	}

    function upload_mypic() {
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$config["upload_path"] = './userpic/';
		$config['overwrite'] = true;
		$config["allowed_types"] = 'gif|jpg|png';
		$config['max_size'] = '100';
		$config['max_width'] = '1024';
		$config['max_height'] = '1024';
		$config['file_name'] = "pic" . $user_id;;
		$this->load->library("upload", $config);
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail);
		if ($this->upload->do_upload("fs")) {
			header("Location:/user_info/info");
		}
		else {
			echo $this->upload->display_errors('<p>', '</p>');
		}
	}

}

?>
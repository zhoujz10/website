<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Poster extends CI_Controller {
	function index() {
	}

	function view() {
		$this->load->model("user_m");
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$result = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail, "department" => $result[0]->department, "class" => $result[0]->class);

		$this->load->model("poster_m");
		$p = $this->poster_m->load();
		$poster = array("poster" => $p);

		$this->load->view('head1', $data);
		$this->load->view('banner2',$poster);
		$this->load->view('poster_index');
	}

	function upload() {
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);

		$this->load->model("poster_m");
		$id = $this->poster_m->count_num();
		$id = $id + 1;

		$config["upload_path"] = './poster/';
		$config['overwrite'] = true;
		$config["allowed_types"] = 'jpg';
		$config['max_size'] = '100000';
		$config['file_name'] = "poster" . $id;
		$this->load->library("upload", $config);

		if ($this->upload->do_upload("fs")) {
			$height = $this->upload->data()['image_height'];
			$width = $this->upload->data()['image_width'];
			$info = array("id" => $id, "user_id" => $user_id, "title" => $_POST["postertitle"], "description" => $_POST["posterdescription"], "height" => $height, "width" => $width, "time" => date('Y-m-d H:i:s',time()));
			$this->poster_m->insert($info);
			$u = "Location:/poster/view";
			header($u);
		}
		else {
			echo $this->upload->display_errors('<p>', '</p>');
		}
	}

	function id($id) {
		$this->load->model("user_m");
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$result = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail, "department" => $result[0]->department, "class" => $result[0]->class);

		$this->load->model("poster_m");
		$a = $this->poster_m->queryid($id);
		$p = array("p" => $a);

		$this->load->view('head1',$data);
		$this->load->view('banner2',$p);
		$this->load->view('poster_v');

	}

}


?>
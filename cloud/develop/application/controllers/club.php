<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Club extends CI_Controller {
	function index() {
		$this->load->model("user_m");
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$result = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail, "department" => $result[0]->department, "class" => $result[0]->class);

		$this->load->model("care_m");
		$d = $this->care_m->querymyclub($user_id);
		$mycare = array("mycare" => $d);

		$this->load->view('head1', $data);
		$this->load->view('banner2',$mycare);
		$this->load->view('club_index');
	}

    function id($club_id) {
		$this->load->model("user_m");
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$result = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail, "department" => $result[0]->department, "class" => $result[0]->class);

		$this->load->model("club_m");
		$c = $this->club_m->searchid($club_id);
		$result = array("result" => $c);

		$this->load->model("care_m");
		$d = $this->care_m->query_club_tag($user_id,$club_id);
		$r = array("r" => $d);

		$this->load->model("club_tz_m");
		$e = $this->club_tz_m->get_tz_list($club_id);
		$tz_list = array("tz_list" => $e);

		$this->load->view('head1.php',$r);
		$this->load->view('banner2.php', $data);
		$this->load->view('searcher',$tz_list);
		$this->load->view('club_v.php', $result);
	}

	function care($club_id) {
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);

		$this->load->model("club_m");
		$c = $this->club_m->searchid($club_id);
		$result = array("result" => $c);
		$name = $c[0]->name;

		$this->load->model("care_m");
		$d = $this->care_m->change_club_care($user_id,$club_id,$name);
		$r = array("r" => $d);

		$this->load->model("user_m");
		$result1 = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail, "department" => $result[0]->department, "class" => $result[0]->class);

		$u = "Location:/club/id/" . $club_id;
		header($u);
	}

	function fatie($club_id) {
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);

		$this->load->model("club_tz_m");
		$key = $this->club_tz_m->count_num();
		$id = $this->club_tz_m->querymaxid();
		$key = $key + 1;
		$id = $id + 1;
		$info = array("user_id" => $user_id, "club_id" => $club_id, "key" => $key, "id" => $id, "name" => $result[0]->name, "title" => $_POST["title_input"], "content" => $_POST["content_input"], "click" => '0', "reply" => '0', "tag" => '1', "time" => date('Y-m-d H:i:s',time()));
		$this->club_tz_m->insert($info);

		$u = "Location:/club/id/" . $club_id;
		header($u);
	}

	function showtz($club_id,$id) {
		$this->load->model("user_m");
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$result = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail, "department" => $result[0]->department, "class" => $result[0]->class);

		$this->load->model("club_m");
		$c = $this->club_m->searchid($club_id);
		$result = array("result" => $c);

		$this->load->model("care_m");
		$d = $this->care_m->query_club_tag($user_id,$club_id);
		$r = array("r" => $d);

		$this->load->model("club_tz_m");
		$e = $this->club_tz_m->querytz($id);
		$tz = array("tz" => $e);

		$this->load->view('head1', $data);
		$this->load->view('banner2',$tz);
		$this->load->view('searcher', $result);
		$this->load->view('club_tz_v', $r);
	}

	function huifu($club_id,$id) {
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);

		$this->load->model("club_tz_m");
		$key = $this->club_tz_m->count_num();
		$key = $key + 1;
		$info = array("user_id" => $user_id, "club_id" => $club_id, "key" => $key, "id" => $id, "name" => $result[0]->name,  "content" => $_POST["content_input"], "click" => '0', "reply" => '0', "tag" => '2', "time" => date('Y-m-d H:i:s',time()));
		$this->club_tz_m->insert($info);
		$this->club_tz_m->addreply($id);

		$u = "Location:/club/showtz/" . $club_id . "/" . $id;
		header($u);
	}

}


?>
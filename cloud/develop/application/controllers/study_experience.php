<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Study_experience extends CI_Controller {
	function id($study_experience_key) {
		$this->load->library('session');
		$this->load->model("operate_count_m");
		$ip_address = $this->session->userdata('ip_address');		
		$tag = $this->operate_count_m->ip_status($ip_address);
		if ($tag == 2)
		{
			show_error('You have too many operations! If you think a technical problem has encontered, please contact feedback@zuixueshu.com.',500,'ERROR');
			return;
		}
		if (!isset($this->session->userdata['user_id']))
		{
			header('Location:/');
			return;
		}
		$user_id = $this->session->userdata['user_id'];
		$tag = $this->operate_count_m->user_status($user_id);
		if ($tag == 2)
		{
			show_error('You have too many operations! If you think a technical problem has encontered, please contact feedback@zuixueshu.com.',500,'ERROR');
			return;
		}
		
		$this->load->model('user_m');
		$user_name = $this->session->userdata['user_name'];
		$this->load->model('study_experience_m');
		$course_key = $this->study_experience_m->get_course_key_by_study_experience_key($study_experience_key);
		if ($course_key == null)
		{
			header('Location:/');
			return;
		}

		//$this->load->model("care_m");
		//$tag = $this->care_m->query_course_tag($user_id, $course_key);
		$info = array('user_id' => $user_id, 'user_name' => $user_name, 'primary_key' => $course_key, 'study_experience_key' => $study_experience_key);
		$this->load->view('study_experience_v', $info);
		
	}
	
	function write($course_id, $sub_id) {
		$this->load->library('Operate_count');
		$this->load->model('operate_count_m');
		$this->load->library('session');
		$ip_address = $this->session->userdata['ip_address'];
		$tag = $this->operate_count_m->ip_status($ip_address);
		if ($tag == 2)
		{
			show_error('You have too many operations! If you think a technical problem has encontered, please contact feedback@zuixueshu.com.',500,'ERROR');
			return;
		}
		if (!isset($this->session->userdata['user_id']))
		{
			header('Location:/');
			return;
		}
		$user_id = $this->session->userdata['user_id'];
		$tag = $this->operate_count_m->user_status($user_id);
		if ($tag == 2)
		{
			show_error('You have too many operations! If you think a technical problem has encontered, please contact feedback@zuixueshu.com.',500,'ERROR');
			return;
		}	

		$this->load->model("course_m");
		$result = $this->course_m->search_primary_key_by_id($course_id,$sub_id);
		$course_name = $this->course_m->search_course_name_by_key($result[0]->primary_key)[0]->course_name;
		if(count($result) == 0)
		{
			header('Location:/');
			return;
		}
		$this->load->model("care_m");
		$tag = $this->care_m->query_course_tag($user_id,$result[0]->primary_key);
		$info = array("user_id" => $user_id, "user_name" => $this->session->userdata['user_name'], "primary_key" => $result[0]->primary_key, "course_name" => $course_name, "tag" => $tag);
		$this->load->view("write_study_experience_v", $info);
	}
	
	function get_rizhi_info() {
		$this->load->library('Operate_count');
		$this->load->model('operate_count_m');
		$this->load->library('session');
		$ip_address = $this->session->userdata['ip_address'];
		$tag = $this->operate_count_m->ip_status($ip_address);
		if ($tag == 2)
		{
			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		if (!isset($this->session->userdata['user_id']))
		{
			$rjson = array("failDesc" => "请先登录！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		$user_id = $this->session->userdata['user_id'];
		$tag = $this->operate_count_m->user_status($user_id);
		if ($tag == 2)
		{
			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		if(!array_key_exists("study_experience_key", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		$this->load->model('study_experience_m');
		$result = $this->study_experience_m->get_study_experience($_GET['study_experience_key']);
		if (count($result) == 0)
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$study_experience = $result[0];
		$study_experience_address = $study_experience->store_address;
		$content = file_get_contents($study_experience_address);
	
		$study_experience->content = $content;
		$rjson = array("status" => true, "study_experience" => $study_experience, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;

	}
	
	function submit_study_experience() {
		$this->load->library('Operate_count');
		$this->load->model('operate_count_m');
		$this->load->library('session');
		$ip_address = $this->session->userdata['ip_address'];
		$tag = $this->operate_count_m->ip_status($ip_address);
		if ($tag == 2)
		{
			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		if (!isset($this->session->userdata['user_id']))
		{
			$rjson = array("failDesc" => "请先登录！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		$user_id = $this->session->userdata['user_id'];
		$tag = $this->operate_count_m->user_status($user_id);
		if ($tag == 2)
		{
			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		if(!array_key_exists("course_primary_key", $_POST) || !array_key_exists("title", $_POST) || !array_key_exists("content", $_POST))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model('study_experience_m');
		$content_preview = $_POST['content'];

		$content_preview=preg_replace("/\s+/", " ", $content_preview); //过滤多余回车 
		$content_preview=preg_replace("/<[ ]+/si","<",$content_preview); //过滤<__("<"号后面带空格) 

		$content_preview=preg_replace("/<\!--.*?-->/si","",$content_preview); //注释 
		$content_preview=preg_replace("/<(\!.*?)>/si","",$content_preview); //过滤DOCTYPE 
		$content_preview=preg_replace("/<(\/?html.*?)>/si","",$content_preview); //过滤html标签 
		$content_preview=preg_replace("/<(\/?head.*?)>/si","",$content_preview); //过滤head标签 
		$content_preview=preg_replace("/<(\/?meta.*?)>/si","",$content_preview); //过滤meta标签 
		$content_preview=preg_replace("/<(\/?body.*?)>/si","",$content_preview); //过滤body标签 
		$content_preview=preg_replace("/<(\/?link.*?)>/si","",$content_preview); //过滤link标签 
		$content_preview=preg_replace("/<(\/?form.*?)>/si","",$content_preview); //过滤form标签 
		$content_preview=preg_replace("/cookie/si","COOKIE",$content_preview); //过滤COOKIE标签 

		$content_preview=preg_replace("/<(applet.*?)>(.*?)<(\/applet.*?)>/si","",$content_preview); //过滤applet标签 
		$content_preview=preg_replace("/<(\/?applet.*?)>/si","",$content_preview); //过滤applet标签 

		$content_preview=preg_replace("/<(style.*?)>(.*?)<(\/style.*?)>/si","",$content_preview); //过滤style标签 
		$content_preview=preg_replace("/<(\/?style.*?)>/si","",$content_preview); //过滤style标签 

		$content_preview=preg_replace("/<(title.*?)>(.*?)<(\/title.*?)>/si","",$content_preview); //过滤title标签 
		$content_preview=preg_replace("/<(\/?title.*?)>/si","",$content_preview); //过滤title标签 

		$content_preview=preg_replace("/<(object.*?)>(.*?)<(\/object.*?)>/si","",$content_preview); //过滤object标签 
		$content_preview=preg_replace("/<(\/?objec.*?)>/si","",$content_preview); //过滤object标签 

		$content_preview=preg_replace("/<(noframes.*?)>(.*?)<(\/noframes.*?)>/si","",$content_preview); //过滤noframes标签 
		$content_preview=preg_replace("/<(\/?noframes.*?)>/si","",$content_preview); //过滤noframes标签 

		$content_preview=preg_replace("/<(i?frame.*?)>(.*?)<(\/i?frame.*?)>/si","",$content_preview); //过滤frame标签 
		$content_preview=preg_replace("/<(\/?i?frame.*?)>/si","",$content_preview); //过滤frame标签 

		$content_preview=preg_replace("/<(script.*?)>(.*?)<(\/script.*?)>/si","",$content_preview); //过滤script标签 
		$content_preview=preg_replace("/<(\/?script.*?)>/si","",$content_preview); //过滤script标签 
		$content_preview=preg_replace("/javascript/si","Javascript",$content_preview); //过滤script标签 
		$content_preview=preg_replace("/vbscript/si","Vbscript",$content_preview); //过滤script标签 
		$content_preview=preg_replace("/on([a-z]+)\s*=/si","On\\1=",$content_preview); //过滤script标签 
		
		$content_preview=preg_replace("/<(.*?)>/si","",$content_preview); //过滤b标签
		$content_preview=preg_replace("/\s+/", " ", $content_preview); //过滤多余回车 
		$content_preview=preg_replace("/&nbsp;/si", " ", $content_preview); //过滤多余空格 
		/*$content_preview=preg_replace("/<(\/?b.*?)>/si","",$content_preview); //过滤b标签 
		
		$content_preview=preg_replace("/<(font.*?)>(.*?)<(\/font.*?)>/si","",$content_preview); //过滤font标签 
		$content_preview=preg_replace("/<(\/?font.*?)>/si","",$content_preview); //过滤font标签 
		
		$content_preview=preg_replace("/<(i.*?)>(.*?)<(\/i.*?)>/si","",$content_preview); //过滤i标签 
		$content_preview=preg_replace("/<(\/?i.*?)>/si","",$content_preview); //过滤i标签 
		
		$content_preview=preg_replace("/<(div.*?)>(.*?)<(\/div.*?)>/si","",$content_preview); //过滤div标签 
		$content_preview=preg_replace("/<(\/?div.*?)>/si","",$content_preview); //过滤div标签 
		
		$content_preview=preg_replace("/<(ol.*?)>(.*?)<(\/ol.*?)>/si","",$content_preview); //过滤ol标签 
		$content_preview=preg_replace("/<(\/?ol.*?)>/si","",$content_preview); //过滤ol标签 
		
		$content_preview=preg_replace("/<(li.*?)>(.*?)<(\/li.*?)>/si","",$content_preview); //过滤li标签 
		$content_preview=preg_replace("/<(\/?li.*?)>/si","",$content_preview); //过滤li标签 
		
		$content_preview=preg_replace("/<(u.*?)>(.*?)<(\/u.*?)>/si","",$content_preview); //过滤u标签 
		$content_preview=preg_replace("/<(\/?u.*?)>/si","",$content_preview); //过滤u标签 
		
		$content_preview=preg_replace("/<(strike.*?)>(.*?)<(\/strike.*?)>/si","",$content_preview); //过滤strike标签 
		$content_preview=preg_replace("/<(\/?strike.*?)>/si","",$content_preview); //过滤strike标签 

		$content_preview=preg_replace("/<(blockquote.*?)>(.*?)<(\/blockquote.*?)>/si","",$content_preview); //过滤blockquote标签 
		$content_preview=preg_replace("/<(\/?blockquote.*?)>/si","",$content_preview); //过滤blockquote标签 
		
		$content_preview=preg_replace("/<(a.*?)>(.*?)<(\/a.*?)>/si","",$content_preview); //过滤a标签 
		$content_preview=preg_replace("/<(\/?a.*?)>/si","",$content_preview); //过滤a标签 */
		
		if(strlen($content_preview) > 140)
			$content_preview = mb_substr($content_preview, 0, 140, 'utf-8');
		
		$tag = $this->study_experience_m->insert_study_experience($_POST['course_primary_key'],$user_id,$this->session->userdata['user_name'],$_POST['title'],$content_preview);
		if($tag == 0)
		{
			$rjson = array("status" => false, "failDesc" => "无满足查询条件的结果！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		else
		{
			$handle=fopen("./study_experience/" . $_POST['course_primary_key'] . "_" . $user_id . "_" . $tag . ".data","w");
			fprintf($handle,"%s",$_POST['content']);
			fclose($handle);
			
			$rjson = array("status" => true, "url" => "/study_experience/id/" . $tag);
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
	}
	
	function get_study_experience_reply() {
		$this->load->library('Operate_count');
		$this->load->model('operate_count_m');
		$this->load->library('session');
		$ip_address = $this->session->userdata['ip_address'];
		$tag = $this->operate_count_m->ip_status($ip_address);
		if ($tag == 2)
		{
			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		if (!isset($this->session->userdata['user_id']))
		{
			$rjson = array("failDesc" => "请先登录！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		$user_id = $this->session->userdata['user_id'];
		$tag = $this->operate_count_m->user_status($user_id);
		if ($tag == 2)
		{
			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		if(!array_key_exists("study_experience_key", $_GET))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model("study_experience_m");
		$result = $this->study_experience_m->query_study_experience_reply_list($_GET['study_experience_key']);
		$rjson = array("status" => true, "study_experience_reply_list" => $result, "url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
	
	function submit_study_experience_reply() {
		$this->load->library('Operate_count');
		$this->load->model('operate_count_m');
		$this->load->library('session');
		$ip_address = $this->session->userdata['ip_address'];
		$tag = $this->operate_count_m->ip_status($ip_address);
		if ($tag == 2)
		{
			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		if (!isset($this->session->userdata['user_id']))
		{
			$rjson = array("failDesc" => "请先登录！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		$user_id = $this->session->userdata['user_id'];
		$tag = $this->operate_count_m->user_status($user_id);
		if ($tag == 2)
		{
			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		if(!array_key_exists("study_experience_key", $_POST) || !array_key_exists("content", $_POST) || !array_key_exists("reply_pk", $_POST))
		{
			$rjson = array("status" => false, "failDesc" => "您可能在用robot访问我们的网站！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		if($_POST["content"] == null)
		{
			$rjson = array("status" => false, "failDesc" => "新鲜事内容不能为空！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		
		$this->load->model("study_experience_m");
		if($_POST['reply_pk'] == '0')
		{
			$insert_result = $this->study_experience_m->insert_new_rely($user_id,$this->session->userdata['user_name'],$_POST['study_experience_key'],$_POST['content']);
			if($insert_result['tag'] == 0)
			{
				$rjson = array("status" => false, "failDesc" => "该学习经验不存在！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
				return;
			}
		}
		else
		{
			$insert_result = $this->study_experience_m->insert_new_sub_rely($user_id,$this->session->userdata['user_name'],$_POST['study_experience_key'],$_POST['content'],$_POST['reply_pk']);
			if($insert_result['tag'] == 0)
			{
				$rjson = array("status" => false, "failDesc" => "该学习经验不存在！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
				return;
			}
		}
		
		$this->load->model("message_m");
		$this->load->model("course_m");
		if($_POST['reply_pk'] == '0')
		{
			$result = $this->study_experience_m->query_study_experience($_POST['study_experience_key']);
			$course_name = $this->course_m->search_course_by_key($result[0]->course_key)[0]->course_name;
			$this->message_m->insert_new_study_experience_reply_message($result[0]->course_key, $course_name, $user_id, $this->session->userdata['user_name'], $result[0]->primary_key, $result[0]->user_id, $result[0]->title);

		}
		else
		{
			$result_reply = $this->study_experience_m->query_study_experience_reply($_POST['reply_pk']);
			$result = $this->study_experience_m->query_study_experience($_POST['study_experience_key']);
			$course_name = $this->course_m->search_course_by_key($result[0]->course_key)[0]->course_name;
			$this->message_m->insert_new_study_experience_reply_message($result[0]->course_key, $course_name, $user_id, $this->session->userdata['user_name'], $result[0]->primary_key, $result_reply[0]->user_id, $result_reply[0]->content);
			if($result_reply[0]->user_id != $result[0]->user_id)
				$this->message_m->insert_new_study_experience_reply_message($result[0]->course_key, $course_name, $user_id, $this->session->userdata['user_name'], $result[0]->primary_key, $result[0]->user_id, $result[0]->title);
		}
		$rjson = array("status" => true, "reply_key" => $insert_result['reply_primary_key'], "time" => $insert_result['time'],"url" => "/");
		$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
		echo $rjson;
		return;
	}
}



?>


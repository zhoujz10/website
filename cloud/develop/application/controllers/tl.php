<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tl extends CI_Controller {
    function reply_orig($course_id, $sub_id){
        $this->load->library("session");
        $user_id = $this->session->userdata('user_id');
        $this->load->model("user_m");
        $this->load->model("tl_m");
        $result = $this->user_m->queryid($user_id);

        $id = $this->tl_m->query_max_id();
        $tl_id = $id + 1;

        $info = array("tl_id" => $tl_id, "user_id" => $user_id, "name" => $result[0]->name, "course_id" => $course_id, "sub_id" => $sub_id, "content" => $_POST['content'], "tl_anchor" => 0);
        $this->tl_m->insert_orig($info);

    }
    function reply_re($course_id, $sub_id){
        $this->load->library("session");
        $user_id = $this->session->userdata('user_id');
        $this->load->model("user_m");
        $this->load->model("tl_m");
        $result = $this->user_m->queryid($user_id);

        $id = $this->tl_m->query_re_max_id();
        $re_id = $id + 1;

        $info = array("re_id" => $re_id, "tl_id" => $_POST['tl_id'], "user_id" => $user_id, "name" => $result[0]->name, "course_id" => $course_id, "sub_id" => $sub_id, "content" => $_POST['content']);
        $this->tl_m->insert_re($info);

    }
}


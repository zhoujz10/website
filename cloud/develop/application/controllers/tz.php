<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tz extends CI_Controller {
	function index() {
	}

	function showtz($course_id,$sub_id,$id) {
		$this->load->library('Operate_count');
		$userdata = $this->operate_count->page_jump();
		if($userdata['tag'] == '0' || $userdata['tag'] == '2')
		{
			header("Location:/");
		}
		else
		{
			$this->load->model("course_m");
			$c = $this->course_m->searchid($course_id,$sub_id);
			$result = array("result" => $c);

			$this->load->model("care_m");
			$d = $this->care_m->query_course_tag($userdata['user_id'],$course_id,$sub_id);
			$r = array("r" => $d);

			$this->load->model("tz_m");
			$e = $this->tz_m->querytz($id);

			$num = count($e);
			$tag = 0;
			for($i = 0; $i < $num; $i++)
			{
				if($e[$i]->reply_id>$tag)
					$tag = $e[$i]->reply_id;
			}
			$f = array();
			for($i = 0; $i <= $tag; $i++)
			{
				for($j = 0; $j < $num; $j++)
				{
					if($e[$j]->reply_id == $i)
						$f[] = $e[$j];
				}
			}


			$tz = array("tz" => $f);

			$this->load->view('head1', $userdata);
			$this->load->view('banner2',$tz);
			$this->load->view('searcher', $result);
			$this->load->view('tz_v', $r);
		}
	}

	function care($course_id,$sub_id,$id) {
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$this->load->model("user_m");
		$result = $this->user_m->queryid($user_id);
		
		$this->load->model("course_m");
		$j = $this->course_m->searchid($course_id,$sub_id);
		$name = $j[0]->name;

		$this->load->model("care_m");
		$d = $this->care_m->change_course_care($user_id,$course_id,$sub_id,$name);
		$r = array("r" => $d);

		$this->load->model("user_m");
		$result1 = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail, "department" => $result[0]->department, "class" => $result[0]->class);
		$this->load->model("course_m");
		$c = $this->course_m->searchid($course_id,$sub_id);
		$result = array("result" => $c);

		$u = "Location:/tz/showtz/" . $course_id . "/" . $sub_id . "/" . $id;
		header($u);
	}

	function huifu($course_id, $sub_id,$id) {
		$this->load->library('Operate_count');
		$userdata = $this->operate_count->page_jump();
		if($userdata['tag'] == '0' || $userdata['tag'] == '2')
		{
			header("Location:/");
		}
		else
		{
			$this->load->model("tz_m");
			$key = $this->tz_m->count_num();
			$reply_id = $this->tz_m->querymaxreplyid($id);
			$key = $key + 1;
			$reply_id = $reply_id + 1;
			$info = array("user_id" => $userdata['user_id'], "course_id" => $course_id, "sub_id" => $sub_id, "key" => $key, "id" => $id, "reply_id" => $reply_id, "name" => $userdata['name'],  "content" => $_POST["content_huifu_input"], "click" => '0', "reply" => '0', "tag" => '2', "time" => date('Y-m-d H:i:s',time()));
			$this->tz_m->insert($info);
			$this->tz_m->addreply($id);

			$u = "Location:/tz/showtz/" . $course_id . "/" . $sub_id . "/" . $id;
			header($u);
		}
	}

	function sub_reply($course_id, $sub_id, $id) {
		$this->load->library('Operate_count');
		$userdata = $this->operate_count->page_jump();
		if($userdata['tag'] == '0' || $userdata['tag'] == '2')
		{
			header("Location:/");
		}
		else
		{
			$this->load->model("tz_m");
			$key = $this->tz_m->count_num();
			$key = $key + 1;
			$info = array("user_id" => $userdata['user_id'], "course_id" => $course_id, "sub_id" => $sub_id, "key" => $key, "id" => $id, "reply_id" => $_POST["reply_id"], "name" => $userdata['name'],  "content" => $_POST["content_huifu_input"], "click" => '0', "reply" => '0', "tag" => '3', "time" => date('Y-m-d H:i:s',time()));
			$this->tz_m->insert($info);
			$this->tz_m->addreply($id);

			$u = "Location:/tz/showtz/" . $course_id . "/" . $sub_id . "/" . $id;
			header($u);
		}
	}

}


?>

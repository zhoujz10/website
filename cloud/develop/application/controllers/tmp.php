<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tmp extends CI_Controller {
	function index() {
		$this->load->model("user_m");
		$result = $this->user_m->querymail($_POST["mailinput"]);
		if($result)
		{
			$user_id = $result[0]->user_id;
			if ($result[0]->password == $_POST["passwordinput"])
			{
				$userdata = $this->user_m->queryid($user_id);

				$this->load->library("Operate_count");
				$this->operate_count->login($user_id,'1');

				$this->load->library("session");
				$this->session->set_userdata($userdata[0]);

				header("Location:/");
			}
			else
			{
				$this->load->library('Operate_count');
				$this->operate_count->login($user_id,'0');
				echo "你密码输错了。";
			}
		}
		else
		{
			$this->load->library('Operate_count');
			$this->operate_count->login($result[0]->user_id,'2');
			echo "你不属于这里";
		}
	}
	function login() {
		if (array_key_exists("mail", $_POST) && array_key_exists("password", $_POST)){
			$this->load->library("session");
			if (array_key_exists("authCode", $_POST))
			{
				if ($this->session->userdata("auth_code") != $_POST["authCode"])
				{
					$rjson = array("status" => false, "test" => $this->session->userdata("auth_code"), "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "验证码不正确！", "url" => "/");
					$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
					echo $rjson;
					return;
				}
			}
			$this->load->model("user_m");
			$result = $this->user_m->query_user_by_mail($_POST["mail"]);
			if ($result)
			{
                $user_id = $result[0]->user_id;
                $user_name = $result[0]->user_name;
				if ($result[0]->password == $_POST["password"])
				{
					$userdata = $this->user_m->query_user_by_id($user_id);
					$rjson = array("status" => true, "user_id" => $user_id, "user_name" => $user_name, "url" => "/tmp/course");
					$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				    $userdata = $this->user_m->query_user_by_id($user_id);
				    $this->session->set_userdata($userdata[0]);

					echo $rjson;
				}
				else
				{
					$rjson = array("status" => false, "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "用户名和密码不匹配！", "url" => "/");
					$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
					echo $rjson;
				}
			}
			else
			{
				$rjson = array("status" => false, "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "用户名不存在！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
			}
		}
		else
		{
				$rjson = array("status" => false, "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "用户名或密码不能为空！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
		}
    }
    function register() {
        if (array_key_exists("mail", $_POST) && array_key_exists("studentId", $_POST) && array_key_exists("name", $_POST) && array_key_exists("password", $_POST))
        { 
            $this->load->library("session");
            if (array_key_exists("authCode", $_POST))
            {
                if ($this->session->userdata("auth_code") != $_POST["authCode"])
                {
                    $rjson = array("status" => false, "test" => $this->session->userdata("auth_code"), "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "验证码不正确！", "url" => "/");
                    $rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
                    echo $rjson;
                    return;
                }
            }
			$this->load->model("user_m");
            $result = $this->user_m->query_user_by_mail($_POST["mail"]);
            if ($result)
            {
                $rjson = array("status" => false, "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "邮箱已存在！", "url" => "/");
                $rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
                echo $rjson;
                return;
            }
            else
            {
                $user_id = $this->user_m->count_user() + 1;
                $info = array("user_id" => $user_id, "user_mail" => $_POST["mail"], "user_name" => $_POST["name"], "password" => $_POST["password"]);
                $this->user_m->insert($info);
                $this->session->set_userdata('user_id',$user_id);
                $data = array("user_id" => $user_id, "name" => $_POST["name"]);
                $user_name = $_POST["name"];
                $rjson = array("status" => true, "userId" => $user_id, "user_name" => $user_name, "url" => "/course");
                $rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
                echo $rjson;
            }
        }
        else
        {
                $rjson = array("status" => false, "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "有项目为空！", "url" => "/");
                $rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
                echo $rjson;
        }
    }

}


?>

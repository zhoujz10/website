<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {
	/*function index() {
		$this->load->model("user_m");
		$result = $this->user_m->querymail($_POST["mailinput"]);
		if($result)
		{
			$user_id = $result[0]->user_id;
			if ($result[0]->password == $_POST["passwordinput"])
			{
				$userdata = $this->user_m->queryid($user_id);

				$this->load->library("Operate_count");
				$this->operate_count->login($user_id,'1');

				$this->load->library("session");
				$this->session->set_userdata($userdata[0]);

				header("Location:/");
			}
			else
			{
				$this->load->library('Operate_count');
				$this->operate_count->login($user_id,'0');
				echo "你密码输错了。";
			}
		}
		else
		{
			$this->load->library('Operate_count');
			$this->operate_count->login($result[0]->user_id,'2');
			echo "你不属于这里";
		}
	}*/
	
	function index() {
		$this->load->library("session");
		$this->load->library("authcode");
		$ip_address = $this->session->userdata('ip_address');
		$this->load->model("operate_count_m");
		$this->load->library("Operate_count");
		$tag = $this->operate_count_m->ip_status($ip_address);

		if ($tag == 2)
		{
			$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
			return;
		}
		if (array_key_exists("mail", $_POST) && array_key_exists("password", $_POST)){
			$this->load->model("user_m");
			$result = $this->user_m->query_user_by_mail($_POST["mail"]);
			if (count($result) == 0)
			{
				$this->operate_count->login('0','2');
				$tag = $this->operate_count_m->ip_status($ip_address);
				if  ($tag ==2){
					$rjson = array("failDesc" => "操作次数过多！", "url" => "/");}
				else
					$rjson = array("status" => false, "authCode" => ($tag != 0), "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "用户名不存在！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
				return;
			}
			$user_id = $result[0]->user_id;
			$tag_user = $this->operate_count_m->user_status($user_id);
			$tag = ($tag_user > $tag)? $tag_user: $tag;
			if ($tag == 1)
			{
				if ((!array_key_exists("authCode", $_POST)) || $_POST["authCode"] == "")
				{
					$this->operate_count->login('0','2');
					$tag_user = $this->operate_count_m->user_status($user_id);
					$tag = $this->operate_count_m->ip_status($ip_address);
					$tag = ($tag_user > $tag)? $tag_user: $tag;
					if ($tag == 2){
						$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
					}
					else
						$rjson = array("status" => false, "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "验证码不能为空！", "url" => "/");
					$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
					echo $rjson;
					return;
				}
				if (!($this->authcode->check($_POST["authCode"])))
				{
					$this->operate_count->login('0','2');
					$tag_user = $this->operate_count_m->user_status($user_id);
					$tag = $this->operate_count_m->ip_status($ip_address);
					$tag = ($tag_user > $tag)? $tag_user: $tag;
					if ($tag == 2){
						$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
					}
					else
						$rjson = array("status" => false, "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "验证码不正确！", "url" => "/");	
					$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
					echo $rjson;
					return;
				}
			}
		
			$user_id = $result[0]->user_id;
			$user_name = $result[0]->user_name;
			if ($result[0]->password == $_POST["password"])
			{
				$userdata = $this->user_m->query_user_by_id($user_id);
				$rjson = array("status" => true, "user_id" => $user_id, "user_name" => $user_name, "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				$userdata = $this->user_m->query_user_by_id($user_id);
				$this->session->set_userdata(array("user_id" => $userdata[0]->user_id, "user_name" => $userdata[0]->user_name, "user_mail" => $userdata[0]->user_mail));
				echo $rjson;
				$this->operate_count->login($user_id,'1');
				return;
			}
			else
			{
				$this->operate_count->login($user_id,'0');
				$tag_user = $this->operate_count_m->user_status($user_id);
				$tag = $this->operate_count_m->ip_status($ip_address);
				$tag = ($tag_user > $tag)? $tag_user: $tag;
				if ($tag == 2)
				{
					$rjson = array("failDesc" => "操作次数过多！", "url" => "/");
				}
				else
					$rjson = array("status" => false, "authCode" => ($tag != 0), "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "用户名和密码不匹配！", "url" => "/");
				$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
				echo $rjson;
				return;
			}
			
			
		}
		else
		{
			$rjson = array("status" => false, "authCode" => true, "authSrc" => "/imgauthcode/show/" . rand(0,10000), "failDesc" => "用户名或密码不能为空！", "url" => "/");
			$rjson = json_encode($rjson, JSON_UNESCAPED_UNICODE);
			echo $rjson;
		}	
		

		
	}
	function logout()
	{
		$this->load->library("session");
		$this->session->unset_userdata("user_id");
	}
}


?>
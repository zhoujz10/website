<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Searcher extends CI_Controller {

    function index() {
	}

	function search() {
		$this->load->library('Operate_count');
		$userdata = $this->operate_count->page_jump();
		if($userdata['tag'] == '0' || $userdata['tag'] == '2')
		{
			header("Location:/");
		}
		else
		{
			$this->load->model("course_m");
			$c = $this->course_m->searchname($_POST['searchinput']);
			$result = array("result" => $c);
			$info = array("tag" => $_POST['searchinput']);
			$this->load->view('head1.php',$info);
			$this->load->view('banner2.php', $userdata);
			$this->load->view('searcher');
			$this->load->view('course_list.php', $result);
		}
	}


}


?>
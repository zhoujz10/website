<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Activity extends CI_Controller {
	function index() {
		$this->load->model("user_m");
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$result = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail, "department" => $result[0]->department, "class" => $result[0]->class);

		$this->load->view('head1', $data);
		$this->load->view('banner2');
		$this->load->view('activity_index');
	}

	function submit() {
		$this->load->model("user_m");
		$this->load->library("session");
		$user_id = $this->session->userdata('user_id');
		$result = $this->user_m->queryid($user_id);
		$data = array("user_id" => $user_id, "name" => $result[0]->name, "mail" => $result[0]->mail, "department" => $result[0]->department, "class" => $result[0]->class);

		$this->load->view('head1', $data);
		$this->load->view('banner2');
		$this->load->view('activity_submit');
	}


}


?>
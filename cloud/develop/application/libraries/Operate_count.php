<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Operate_count
{
	var $CI;

	public function __construct()
	{
		$this->CI = & get_instance();
		$this->CI->load->model("operate_count_m");
		$this->CI->load->library("session");
	}

	function page_jump()
	{
		$user_id = $this->CI->session->userdata("user_id");
		$result = 0;
		if($user_id != false)
		{
			$userdata = $this->CI->session->all_userdata();

			$result = $this->CI->operate_count_m->user_id_count($user_id);
		}
		$userdata['tag'] = $result;
		$userdata['user_id'] = $user_id;
		$userdata['name'] = $this->CI->session->userdata("name");
		return $userdata;
	}

	function session_test() {
		$user_id = $this->CI->session->userdata("user_id");
		if($user_id != false)
			return $user_id;
		else
			return 0;
	}

	function login($user_id,$tag) {
		$userdata = $this->CI->session->all_userdata();
		$this->CI->operate_count_m->login($user_id,$userdata,$tag);
	}
	
	function register($tag) {
		return;
	}


}


?>